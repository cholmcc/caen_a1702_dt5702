#include <feb/slow_control.hh>
#include <cstring>
#include <cassert>
#include <algorithm>

int main()
{
  feb::slow_control sc;
  // memset(sc._data,0,feb::slow_control::NBYTES);
  sc._data.reset();
  
  for (size_t i = 0; i < 16; i++) {
    sc.ch.time_thresholds  [i]    = i;
    sc.ch.charge_thresholds[i]    = i;
    sc.ch.time_thresholds  [16+i] = 15-i;
    sc.ch.charge_thresholds[16+i] = 15-i;
  }
  for (size_t i = 0; i < 32; i++) {
    sc.ch.disable_preamps[i] = true;
    sc.ch.high_gains     [i] = 42;
  }
  sc.hg.shaper_time[0]   = 7;
  sc.lg.shaper_time[0]   = 7;
  sc.tr.enable_or_out[0] = 1;
  
  const unsigned char ref_bytes[] = {
    0x10, 0x00, 0x00, 0x00, 0x00, 0x20, 
    0x80, 0x1a, 0x40, 0x0d, 0xa0, 0x06, 
    0x50, 0x03, 0xa8, 0x01, 0xd4, 0x00, 
    0x6a, 0x00, 0x35, 0x80, 0x1a, 0x40, 
    0x0d, 0xa0, 0x06, 0x50, 0x03, 0xa8, 
    0x01, 0xd4, 0x00, 0x6a, 0x00, 0x35, 
    0x80, 0x1a, 0x40, 0x0d, 0xa0, 0x06, 
    0x50, 0x03, 0xa8, 0x01, 0xd4, 0x00, 
    0x6a, 0x00, 0x35, 0x80, 0x1a, 0x40, 
    0x0d, 0xa0, 0x06, 0x50, 0x03, 0xa8, 
    0x01, 0xd4, 0x00, 0x6a, 0x00, 0x15, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0xe0, 0x1c, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x10, 0x32, 0x54, 
    0x76, 0x98, 0xba, 0xdc, 0xfe, 0xef, 
    0xcd, 0xab, 0x89, 0x67, 0x45, 0x23, 
    0x01, 0x10, 0x32, 0x54, 0x76, 0x98, 
    0xba, 0xdc, 0xfe, 0xef, 0xcd, 0xab, 
    0x89, 0x67, 0x45, 0x23, 0x01     };
  std::string ref_str = 
    "0000000100100011010001010110011110001001"
    "1010101111001101111011111111111011011100"
    "1011101010011000011101100101010000110010"
    "0001000000000001001000110100010101100111"
    "1000100110101011110011011110111111111110"
    "1101110010111010100110000111011001010100"
    "0011001000010000000000000000000000000000"
    "0000000000000000000000000000000000011100"
    "1110000000000000000000000000000000000000"
    "0000000000000000000000000000000000000000"
    "0000000000000000000000000000000000000000"
    "0000000000000000000000000000000000000000"
    "0000000000000000000000000000000000000000"
    "0000000000000000000000000000000000000000"
    "0000000000000000000000000000000000000000"
    "0000000000000000000101010000000001101010"
    "0000000011010100000000011010100000000011"
    "0101000000000110101000000000110101000000"
    "0001101010000000001101010000000001101010"
    "0000000011010100000000011010100000000011"
    "0101000000000110101000000000110101000000"
    "0001101010000000001101010000000001101010"
    "0000000011010100000000011010100000000011"
    "0101000000000110101000000000110101000000"
    "0001101010000000001101010000000001101010"
    "0000000011010100000000011010100000000011"
    "0101000000000110101000000000110101000000"
    "0001101010000000001000000000000000000000"
    "000000000000000000010000";
  std::reverse(ref_str.begin(), ref_str.end());
  std::bitset<feb::slow_control::nbits> ref_bits(ref_str);
  
  feb::dump_bits(ref_bits);
  feb::dump_bits(sc._data);

  assert(sc._data == ref_bits);
  unsigned char buf[143];
  sc.to_bytes(buf);

  feb::dump_bytes(ref_bytes, 143);
  feb::dump_bytes(buf,       143);

  for (size_t i = 0; i < 143; i++)
    assert(buf[i] == ref_bytes[i]);

  sc.from_bytes(ref_bytes);
  assert(ref_bits == sc._data);
  
  return 0;
}
// Local Variables:
//   compile-command: "g++ -I.. test_encode.cc -o test_encode"
// End:



  
