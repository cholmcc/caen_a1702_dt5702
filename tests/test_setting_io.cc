/** 
 * @file      test_settings_io.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Test
 */
#include <iostream>
#include <array>
#include <cstring>
#include <iterator>
#include <feb/slow_control.hh>
#include <fstream>

int main()
{
  feb::slow_control sc;
  std::cout << sizeof(sc) << std::endl;
  feb::dump_bytes(sc._data, 143);
  feb::dump_bits (sc._data, 143);
  std::cout << (void*)sc._data << "\t"
	    << (void*)sc.ch.biases._data << std::endl;
  
  {
    std::ofstream out("test.dat");
    out << sc << std::endl;
  }
  {
    feb::slow_control sc1;
    std::ifstream in("test.dat");
    in >> sc1;

    std::cout << sc1 << std::endl;
  }
  
  return 0;
}
// Local Variables:
//   compile-command: "g++ -I. test_setting_io.cc -o test_setting_io"
// End:

