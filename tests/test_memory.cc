/** 
 * @file      test_memory.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Test
 */
#include <feb/memory.hh>
#include <feb/utils.hh>
#include <array>


void test1() {
  struct test : feb::memory<8*4>
  {
    view<0,4> ch;

    test() : ch(_data) {
      ch[7] = 0xf;
      ch[6] = 0xe;
      ch[5] = 0xe;
      ch[4] = 0xb;
      ch[3] = 0xd;
      ch[2] = 0xa;
      ch[1] = 0xe;
      ch[0] = 0xd;
    }
    void load(std::istream&) override {}
    void save(std::ostream& o) const override {
      for (size_t i = 0; i < 8; i++) 
	o << feb::hex_out<1>(ch[i]);
    }
  };

  test t;

  feb::dump_bits(t._data);  
  std::cout << t << std::endl;

  for (size_t i = 0; i < 4; i++) {
    unsigned tmp  = t.ch[i+4];
    t.ch[i+4]     = unsigned(t.ch[i]);
    t.ch[i]       = tmp;
  }

  feb::dump_bits(t._data);  
  std::cout << t << std::endl;

  std::string s = t._data.to_string();
  std::cout << s << std::endl;
  std::reverse(s.begin(), s.end());
  std::cout << s << std::endl;
  
  test::reverse(t._data);
  feb::dump_bits(t._data);
  s = t._data.to_string();
  std::cout << s << std::endl;
  
}

int main()
{
  test1();
  
  return 0;
}
// Local Variables:
//    compile-command: "g++ -I.. test_memory.cc -o test_memory"
// End:

