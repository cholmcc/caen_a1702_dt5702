/** 
 * @file      test_memory.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Test
 */
#include <feb/memory.hh>
#include <array>


void test1() {
  struct test : feb::memory<8*4>
  {
    view<0,4> bytes;

    test() {
      bytes[7] = 0xd;
      bytes[6] = 0xe;
      bytes[5] = 0xa;
      bytes[4] = 0xd;
      bytes[3] = 0xb;
      bytes[2] = 0xe;
      bytes[1] = 0xe;
      bytes[0] = 0xf;
    }
  };

  test t;
  
  char buf[9];
  buf[8] = '\0';
  
  t.to_bytes(buf);
  std::cout << buf << std::endl;
}

int main()
{
  test1();
  
  return 0;
}
// Local Variables:
//    compile-command: "g++ -I.. test_memory.cc -o test_memory"
// End:

