/** 
 * @file      test_log.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Test
 */
#include <feb/log.hh>
#include <iostream>

void test(feb::logbuf& buf)
{
  auto old = std::clog.rdbuf(&buf);
  std::clog.exceptions(std::ios_base::badbit);

  std::clog << "D This is an debug message" << std::endl;
  std::clog << "I This is an info message" << std::endl;
  std::clog << "W This is an warning message" << std::endl;
  std::clog << "E This is an error message" << std::endl;
  std::clog << "This is an uncategorized message (treated as info)" 
	    << std::endl;
  try { 
    std::clog << "F This is fatal message which throws" << std::endl;
  }
  catch (std::exception& e) {
    std::cerr << e.what();
  }

  std::clog.rdbuf(old);
}

int
main()
{
  {
    feb::logbuf buf;
    buf.level = feb::logbuf::levels::INFO;

    test(buf);
  }

  {
    feb::logbuf buf;
    buf.level = feb::logbuf::levels::WARNING;

    test(buf);
  }
  {
    feb::color_logbuf buf;
    buf.level = feb::logbuf::levels::DEBUG;

    test(buf);
  }
  
  return 0;
}
// Local Variables:
//   compile-command: "g++ -I. test_log.cc -o test_log"
// End:
