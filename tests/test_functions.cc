#include <valarray>
#include <iostream>
#include <iomanip>
#include <iterator>

template <typename T, size_t Width>
struct out_v
{
  const T& _v;
  out_v(const T& v) : _v(v) {}
};

template <typename T,size_t Width>
std::ostream& operator<<(std::ostream& o, const out_v<T,Width>& v)
{
  return o << std::setw(Width) << v._v;
}
  
template <typename T, size_t Width=8>
std::ostream& operator<<(std::ostream& o, const std::valarray<T>& v)
{
  std::copy(std::begin(v), std::end(v),
	    std::ostream_iterator<out_v<T,Width>>(o, "\t"));
  return o;
}

#include "scripts/Functions.C"
#include <feb/functions.hh>

int main()
{
  std::valarray<double> i(10);
  std::iota(std::begin(i), std::end(i),0);
  auto                  p = feb::functions::poisson(i, 3);

  
  std::cout << i << std::endl;
  std::cout << p << std::endl;


  double norm        =   1;
  double gain        =   1;
  double pedestal    =   7;
  double noise       =   9.6;
  double avg_photons =   1.18;
  double xtra_width  =   0.3;
  double x_talk      =   0.5;
  double pp[]        = {norm,gain,pedestal,noise,avg_photons,xtra_width,x_talk,
    5};
  
  std::valarray<double> x(3);
  std::iota(std::begin(x), std::end(x),0);
  x *= 10;

  std::cout << "x=" << x << std::endl;

  std::valarray<double> fr1(x.size());
  for (size_t i = 0; i < fr1.size(); i++) fr1[i] = mppc0(&(x[i]),pp);

  std::valarray<double> fr2(x.size());
  for (size_t i = 0; i < fr2.size(); i++) fr2[i] = mppc1(&(x[i]),pp);
  
  std::cout << "fr1=" << fr1 << std::endl;
  std::cout << "fr2=" << fr2 << std::endl;

  auto f1 = feb::functions::response(x,
				     norm,
				     gain,
				     pedestal,
				     noise,
				     avg_photons,
				     xtra_width,
				     x_talk);

  std::cout << "f1=" << f1 << std::endl;

  auto f2 = feb::functions::response2(x,
				      norm,
				      gain,
				      pedestal,
				      noise,
				      avg_photons,
				      xtra_width,
				      x_talk);

  std::cout << "f2=" << f2 << std::endl;
  
  
  
  
  return 0;
}

			  
// Local Variables:
//  compile-command: "g++ -I.. -g `root-config --cflags` test_functions.cc `root-config --libs` -o test_functions  &&  ./test_functions"
// End:
