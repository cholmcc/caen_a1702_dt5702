/** 
 * @file      root_daq.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     standalone DAQ writting to TTree
 */
#include <feb/host.hh>
#include <feb/dummy.hh>
#include <feb/root/DAQ.hh>
#include <iomanip>
#include <cassert>
#include <feb/log.hh>

void usage(const std::string& prog,std::ostream& o=std::cout)
{
  o << "Usage: " << prog << " [OPTIONS]\n\n"
    << "Options:\n\n"
    << "  -h              This help\n"
    << "  -i INTERFACE    Network interface to use (eth0)\n"
    << "  -c DIRECTORY    Directory for configuration files\n"
    << "  -d DIRECTORY    Directory for data files\n"
    << "  -v LEVEL        Verbosity\n"
    << "  -m MAX_CLIENTS  Maximum number of clients to register\n"
    << "  -n MAX_EVENTS   Maximum number of events\n"
    << "  -t MAX_SECONDS  Maximum number of seconds\n"
    << "  -r RUN_NUMBER   Run number\n\n"
    << "INTERFACE is the network interface, use \"\" for emulation\n"
    << "If FILE is '-' write to standard output\n"
    << "LEVEL is one of 0:errors,1:warnings,2:information,3:debug\n"
    << "DIRECTORY for configurations should contain "
    << "'client_<addr>.{sc,pb,fl}' files\n"
    << "DIRECTORY for data must exist"
    << std::endl;
}
  
int main(int argc, char** argv)
{
  std::string  confdir = "config";
  std::string  datadir = "data";
  std::string  inter   = "eth0";
  int          loglvl  = 1;
  int          maxcl   = 1;
  unsigned int maxev   = 0;
  unsigned int maxsec  = 0;
  unsigned int runno   = 0;
  
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 'i': inter   = argv[++i]; break;
      case 'c': confdir = argv[++i]; break;
      case 'd': datadir = argv[++i]; break;
      case 'v': loglvl  = std::stoi(argv[++i]); break;
      case 'm': maxcl   = std::stoi(argv[++i]); break;
      case 'n': maxev   = std::stoi(argv[++i]); break;
      case 't': maxsec  = std::stoi(argv[++i]); break;
      case 'r': runno   = std::stoi(argv[++i]); break;
      default:
	std::clog << "F: Unknown option " << argv[i] << std::endl;
	return 1;
      }
    }
    else {
      std::clog << "F: Unknown argument: " << argv[i] << std::endl;
      return 1;
    }
  }

  feb::logbuf::instance()->set_level(loglvl);
  
  feb::base_host* h = 0;
  if (inter.empty()) h = new feb::dummy(inter);
  else               h = new feb::host(inter);

  h->scan(maxcl);

  for (auto& k : h->_clients) {
    if (k.first == feb::broadcast) continue;

    feb::client& cl = k.second;
    
    cl.load_settings(confdir);
    cl.configure();
    cl.sync();
  }

  feb::root::DAQ daq(*h,runno,datadir);
  daq.install_handler();
  daq.loop(maxev,maxsec);
  
  return 0;
}
// Local Variables:
//   compile-command: "g++ -g -I. `root-config --cflags` daq.cc `root-config --libs` -o daq"
// End:
