#!/bin/bash
#  
# file      first.cc
# date      Sun 26 Sep 2021
# author    Christian Holm Christensen <cholm@nbi.dk>
# copyright 2021 Christian Holm Christensen
# license   GPL-v3
# brief     Example  
# 

root=$ROOTSYS/bin/root.exe
if ! test -f $root ; then
    root=`command -v root.exe`
fi

inter=eth0
conf="config"
data="data"
verb=1
deb=
reb=
max=254
sudo="sudo -E"

usage()
{
    cat <<EOF
Usage: $0 [OPTIONS] 

Options:
	-h		This help
	-i INTERFACE	Network interface 
	-c DIRECTORY	Configurations directory 
	-d DIRECTORY	Directory to put data into 
	-v VERBOSITY    Logging level 
	-m MAX          Maximum number of clients
	-g              Build with debug 
	-r              Rebuild 	

INTERFACE: 
- Typically "eth0"	  

VERBOSITY: 
- 0 Only show errors
- 1 Also show warnings 
- 2 Also show information 
- 3 Also show debugging  

DIRECTORY: 
- Directory containting configuration files 

MAX:
- Maximum number of FEBs to look for 

"-g" means build GUI with debugging support 

"-r" means a force rebuild 
EOF
}

while test $# -gt 0 ; do
    case $1 in 
	-h|--help) usage ; exit 0 ;;
	-i)        inter=$2; shift ;;
	-c)        conf=$2  ; shift ;;
	-d)        data=$2  ; shift ;;
	-v)        verb=$2 ; shift ;;
	-g)        deb="g" ;;
	-r)        reb="+" ;;
	-m)        max=$2  ; shift ;;
	*)
	    echo "Unknown option: $1" > /devstderr
	    exit 1
	    ;;
    esac
    shift
done

if test "x$inter" = "x" ; then
    sudo=""
fi

arg="feb/gui/GUI.C+${reb}${deb}(\"${inter}\",\"${conf}\",\"${data}\",${verb},${max})"
echo "Running ${sudo} ${root} ${arg}"
    
${sudo} ${root} -l "$arg"

