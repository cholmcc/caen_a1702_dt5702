# Software for CAEN A1702/DT5702

The CAEN A1702/DT5702 board (FEB) is for reading out signals from up to 32
silicon photomultipliers (SiPM). 

## Introduction 

This project contains software for accessing and control up to 254
FEBs connected to a GNU/Linux computer via ethernet.  The software
consists of two parts: 

- Low-level interface to the FEBs via ethernet 
- High-level applications to do common tasks 

## Low-level interface 

Most of the code in the sub-directory [`feb`](feb) constitutes the low
level interface.  

### `feb::base_host`

This class defines the host (GNU/Linux) interface to the FEBs.
Through this interface the user can `scan` for clients on the network
and send commands to the FEBs.  The base class
[`feb::base_host`](feb/base_host.hh) does not implement the
interface.  Two concrete implementations are defined 

- [`feb::host`](feb/host.hh) implements the communication via ethernet
  to the FEBs.  As this is typically a privileged operation, one needs
  to be super user (`sudo`) when using objects of this kind.  The name
  of the interface (e.g., `eth0`) needs to be passed to this class
  upon object construction. 
  
- [`feb::dummy`](feb/dummy.hh) is an emulation implementation.  That
  is, it does not communicate with actual hardware but emulates the
  behaviour in software.  This is selected by passing an empty string
  for the interface name.
  
### `feb::command`

Commands (read/write/action) are sent via objects of classes
inheriting from [`feb::command`](feb/command.hh).   The `feb::command`
objects encode 

- The destination MAC address `00:60:37:12:34:`_XX_ of the FEB
  addressed.  Here, _XX_ is the value set on the dip-switches on the
  board itself.  The special address with _XX_ equal `FF` is the
  special broadcast address. 
- The source MAC address (the MAC address of the used interface) 
- The protocol (always `0x0108` - or `0x0801` in network byte
  ordering). 
- The command family (a single byte).  The families are 
  - `0x00` Read/write registers on the on-board network switch
  - `0x01` General commands 
  - `0x02` Slow-control register read/write 
  - `0x03` Event read-out 
  - `0x04` Probe-point settings read/write 
  - `0x05` Firmware read/write (PROM and FPGA)
  - `0x06` Flexible (trigger) logic read/write 
- The sub-command (a single byte) of the family 
- A possible parameter for the command (2 bytes)
- Payload for the command.  On write commands, this is typically
  register values to write.  For read commands, this will be filled
  with the read-back register values on return. 
  
The following table summarises the commands available.  They are all
encoded as classes in the name space
[`feb::commands`](feb/commands.hh). 

| **Command**            | **Description**                          |    |
|------------------------|------------------------------------------|----|
| `read_switch`          | Read switch register                     |    |
| `write_switch`         | Write switch register                    |	 |
| `read_switch_bulk`     | Read all switch register                 |	 |
| `write_switch_bulk`    | Write all switch register                | *	 |
| `set_host`             | Set host MAC on FEBs and possible timing | *	 |
| `acquire`              | Start/stop/reset trigger handling        | *	 |
| `turn_on_bias`         | Turn on reverse bias voltage             | *	 |
| `turn_off_bias`        | Turn on reverse bias voltage             | *	 |
| `get_event_rate`       | Read current event rate                  | ** |
| `read_slow_control`    | Read slow control registers              |	 |
| `write_slow_control`   | Write slow control registers             |	 |
| `read_events`          | Read events from FEB                     | ** |
| `read_probe`           | Read probe-point settings                |	 |
| `write_probe`          | Write probe-point settings               |	 |
| `read_flexible_logic`  | Read trigger logic registers             |	 |
| `write_flexible_logic` | Write trigger logic registers            |    |

`*` commands are essentially proper commands and initiates some
action. 

`**` commands deals with getting events and information back to the
user (host). 

The rest of the commands read or write registers in the FEBs. 

### `feb::client`

Objects of the [`feb::client`](feb/client.hh) provides 

- A simpler interface for sending commands to the FEBs 
- A reflection of the hardware slow-control registers

Objects of this class can load or save configurations to a file for
later use.  They also allow to push these settings to the FEBs or read
them pack into memory.  Each object of the this class contains
information on 

- [`feb::slow_control`](feb/slow_control.hh) 
- [`feb::probe`](feb/probe.hh) 
- [`feb::flexible_logic`](feb/flexible_logic.hh) 

### `feb::slow_control`

This structure contains a lot of different setting of the slow-control
part of the FEBs.  These are typically global flags, but some settings
are defined per-channel (0 to 31). 

### `feb::probe`

This structure contains the different probe-point settings. 

### `feb::flexible_logic`

This structure `feb::flexible_logic` - contains the different
probe-point settings.

## Higher-level interface 

The project provides a number of high-level interfaces. 

### Data acquistion 

The class [`feb::daq`](feb/daq.hh) defines an abstract data acquistion
(DAQ) application.   This class does not define any specific output
format, but goes through the necessary steps to do DAQ from the FEBs.
This relies on the interface [`feb::base_host`](`feb/base_host.hh`) to
send commands to the FEBs. 

An "event" in the DAQ is defined as a single trigger in a FEB.  That
means that a single event from contains data from a _single_ FEB (as
opposed to all connected FEBs). 

Two concrete implementations are provided 

- [`feb::raw_daq`](feb/raw_daq.hh) which writes out events to standard
  output (or some other stream) as tab-separated values.  Note, this
  output quickly becomes big and unwieldy.
  
  Each line has the fields
  
  | **Field**   | **Type**         | **Description**                       |
  |-------------|------------------|---------------------------------------|
  | _feb_       | `unsigned char`  | Client address as set in dip-switches |
  | _T0 sync_   | `bool`           | This is a `T0` sync event             |
  | _T1 sync_   | `bool`           | This is a `T1` sync event             |
  | _T0 over_   | `bool`           | Overflow of `T0` counter              |
  | _T0 over_   | `bool`           | Overflow of `T1` counter              |
  | _overwrite_ | `unsigned short` | Number of overwritten events          |
  | _lost_      | `unsigned short` | Number of lost events                 |
  | _T0_        | `unsigned int`   | Nano-seconds since last `T0` sync.    |
  | _T1_        | `unsigned int`   | Nano-seconds since last `T1` sync.    |
  | _ADC_       | `unsigned short` | 32 signals                            |

- [`feb::root::DAQ`](feb/root/DAQ.hh) defines a
  [ROOT](https://root.cern.ch) based DAQ.  The events are written to a
  [`TTree`](https://root.cern.ch/doc/master/classTTree.html) in a 
  [`TFile`](https://root.cern.ch/doc/master/classTFile.html).  The
  `TTree` has a single branch with the format 
  
  | **Field**     | **Type**         | **Description**                       |
  |---------------|------------------|---------------------------------------|
  | `t0`          | `unsigned int`   | Nano-seconds since last `T0` sync.    |
  | `t1`          | `unsigned int`   | Nano-seconds since last `T1` sync.    |
  | `overwritten` | `unsigned short` | Number of overwritten events          |
  | `lost`        | `unsigned short` | Number of lost events                 |
  | `adc`         | `unsigned short` | 32 signals                            |
  | `feb`         | `unsigned char`  | Client address as set in dip-switches |
  | `t0 sync`     | `bool`           | This is a `T0` sync event             |
  | `t1 sync`     | `bool`           | This is a `T1` sync event             |
  | `t0 overflow` | `bool`           | Overflow of `T0` counter              |
  | `t1 overflow` | `bool`           | Overflow of `T1` counter              |

The data can be read in with code _a la_ 

```c++
#include <TFile.h>
#include <TTree.h>
#include <feb/root/Event.hh>

void ReadData() 
{
  TFile* file = TFile::Open("run_000000.root","READ");
  TTree* tree = static_cast<TTree*>(file->Get("T"));
  Event  event;
  tree->SetBranchAddress("event",&event);
  
  for (Int_t ev = 0; ev < tree->GetEntries(); ev++) {
    // Access the members of event to get information 
    // E.g., 
    //
    // event->t0      is the T0 counter
    // event->adc[15] is the 16th channels signal 
  }
}
```

### ROOT based DAQ 

Classes in the directory [`feb/root`](feb/root) are classes for
defining data acquisition which write the result to
[ROOT](https://root.cern.ch) `TTree`s.  Provided is also the
possibility to monitor the data via the class
[`feb::root::Monitor`](feb/root/Monitor.hh). 

This is also integrated into the ROOT based graphical user interface.


### ROOT based GUI 

The classes in [`feb::gui`](feb/gui) provides a graphical user
interface which encompasses a most of the functionality of the
project: 

- Reading and writing registers and settings on the FEBs. 
- Loading and saving configurations 
- Data acquisition - including monitoring 

## Applications 

These are built by executing **Make**: 

    make 
	
### `first`

[This](first.cc) is a simple application that reads all settings from
the FEBs and writes them to disk.

### `raw_daq`

[A](raw_daq.cc) standard-alone DAQ which write data to standard
output.  It accepts a number of arguments (use the `-h` option to see
a short description).  An example of use

    sudo ./daq -i eth0 -c config > data/events.dat 
	
This will acquire for ever.  Press `Ctrl-C` to stop.  One can limit
the number of events (`-n`) or time running (`-t`) by passing
appropriate options. 

The flow of the application is 

- Connect to the specified network interface and search for FEBs 
- Apply configurations read form files in the specified
  configuration directory (defaults to `config`) to the FEBs 
- Turn on bias voltages on all connected FEBs
- Reset all event buffers on all connected FEBs 
- Enable trigger handling on the FEBs 
- Loop until time-out, enough events have been acquired, or
  indefinitely 
  - For each FEB, retrieve the current events 
    - For each event retrieved, write out the data 
	

### `root_daq`

[A](root_dac.cc) stand-alone DAQ which writes data to a ROOT `TTree`
(as described above).  It accepts a number of arguments (use the
option `-h` to see a short description).  This application will try to
choose the next run number in a sequence based on the files present in
the directory where it is executed.

For example, if the sub-directory `data` has the files 

    run_000000.root 
	...
	run_000010.root 
	
then run number `11` will be chosen, and the command 

    sudo ./root_daq -i eth0 -c config -d data -t 60 
	
will run for approximately 1 minute (= 60 seconds) and write to the
output file `data/run_000011.root`.  If no time-out (`-t`) or number
of events are specified, then the application will run indefinitely
and can be stopped by an interrupt (`Ctrl-C`). 

The general flow of the application is the same as for `raw_daq`
described above. 

### `gui`

[This](gui.cc) is a stand-alone graphical user interface using the
services described above, including data acquisition and monitoring.

 Use the option `-h` to see available
options.   For example 

    sudo ./gui -i eth0 -c config -d data 
	
will connect to the FEBs on the interface `eth0`, read configuration
files from the directory `config` and write data files to the
directory `data`.  Note that these settings can later be changed in
the user interface. 
	
### `gui.sh`

[This](gui.sh) is an alternative to the above application which executes the
graphical user interface via the ROOT interactive interpreter. 

Use the option `-h` to see available options.  For example

    sudo ./gui.sh -i eth0 -c config -d data 
	
will start the GUI and read initial settings from the sub-directory
`config`. Data will be written to `data`. 

### Other applications 

Applications for specific purposes can easily be implemented using the
application programming interface defined by the classes in
[`feb`](feb/), [`feb/root`](feb/root/), and possibly
[`feb/gui`](feb/gui). 

## TODO 

- Check encoding of `flexible_logic` - seems like it should be in
  reverse order 
- Firmware read/write 

## Original code 

A modified version of the  [original code](orig/) is included.  More
details on the modification can be found there. 
