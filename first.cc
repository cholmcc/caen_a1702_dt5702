/** 
 * @file      first.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Example  
 */
#include <feb/host.hh>
#include <feb/dummy.hh>
#include <feb/log.hh>
#include <iomanip>
#include <cassert>

void usage(const std::string& prog,std::ostream& o=std::cout)
{
  o << "Usage: " << prog << " [OPTIONS]\n\n"
    << "Options:\n\n"
    << "  -h              This help\n"
    << "  -i INTERFACE    Network interface to use (eth0)\n"
    << "  -c DIRECTORY    Directory for configuration files\n"
    << "  -v LEVEL        Verbosity\n"
    << "  -m MAX_CLIENTS  Maximum number of clients to register\n"
    << std::endl;
}
  
int main(int argc, char** argv)
{
  std::string confdir = "";
  std::string inter   = "eth0";
  int         loglvl  = 5;
  int         maxcl   = 1;
  
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 'i': inter   = argv[++i]; break;
      case 'c': confdir = argv[++i]; break;
      case 'v': loglvl  = std::stoi(argv[++i]); break;
      case 'm': maxcl   = std::stoi(argv[++i]); break;
      default:
	std::clog << "F: Unknown option " << argv[i] << std::endl;
	return 1;
      }
    }
    else {
      std::clog << "F: Unknown argument: " << argv[i] << std::endl;
      return 1;
    }
  }

  feb::logbuf::instance()->set_level(loglvl);
  
  feb::base_host* h = 0;
  if (inter.empty()) h = new feb::dummy(inter);
  else               h = new feb::host(inter);

  h->scan(maxcl);

  for (auto& k : h->_clients) {
    std::cout << std::setfill('0') << std::hex
	      << "0x" << std::setw(2) << unsigned(k.first)
	      << std::setfill(' ') << std::dec << std::endl;

    if (k.first == feb::broadcast) continue;
    k.second.sync();
    std::clog << "D: " << k.second._slow_control << std::endl;
  }

  return 0;
}
// Local Variables:
//   compile-command: "g++ -g -Wall -ansi -pedantic -I. first.cc -o first"
// End:
