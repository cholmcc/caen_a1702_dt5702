/** 
 * @file      gui.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     GUI application
 */
#include <feb/gui/Main.hh>
#include <feb/dummy.hh>
#include <feb/host.hh>
#include <TApplication.h>

void usage(const std::string& prog,std::ostream& o=std::cout)
{
  o << "Usage: " << prog << " [OPTIONS]\n\n"
    << "Options:\n\n"
    << "  -h              This help\n"
    << "  -i INTERFACE    Network interface to use (eth0)\n"
    << "  -c DIRECTORY    Directory for configuration files\n"
    << "  -d DIRECTORY    Directory for data files\n"
    << "  -v LEVEL        Verbosity\n"
    << "  -m MAX_CLIENTS  Maximum number of clients to register\n"
    << "INTERFACE is the network interface, use \"\" for emulation\n"
    << "If FILE is '-' write to standard output\n"
    << "LEVEL is one of 0:errors,1:warnings,2:information,3:debug\n"
    << "Config DIRECTORY should contain 'client_<addr>.{sc,pb,fl}' files\n"
    << "Data output DIRECTORY must exist\n"
    << std::endl;
}

#include "feb/gui/GUI.C"

int
main(int argc, char** argv) {
  std::string  confdir = "config";
  std::string  datadir = "data";
  std::string  inter   = "eth0";
  int          loglvl  = 1;
  int          maxcl   = 1;

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 'i': inter   = argv[++i]; break;
      case 'c': confdir = argv[++i]; break;
      case 'o': datadir = argv[++i]; break;
      case 'v': loglvl  = std::stoi(argv[++i]); break;
      case 'm': maxcl   = std::stoi(argv[++i]); break;
      default:
	std::clog << "F: Unknown option " << argv[i] << std::endl;
	return 1;
      }
    }
    else {
      std::clog << "F: Unknown argument: " << argv[i] << std::endl;
      return 1;
    }
  }

  TApplication app("GUI",0,0);
  
  GUI(inter, confdir, datadir, loglvl, maxcl);

  app.Run();

  return 0;
}
