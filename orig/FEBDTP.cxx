#include <cstdlib>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/ether.h>
#include "FEBDTP.hxx"
#include <iostream>
#include <iomanip>


ClassImp(FEBDTP)
#if 0
;
#endif

bool FEBDTP::SetTimeout(unsigned int microseconds)
{
  if (Verbose)
    printf("Setting timeout to %u microseconds\n",microseconds);
  timeval tv = { 0, microseconds };
  if (setsockopt(sockfd_r, SOL_SOCKET, SO_RCVTIMEO,
		 (char *)&tv,sizeof(struct timeval))== -1){
    perror("SO_SETTIMEOUT");
    return false;
  }
  return true;
}

int FEBDTP::GetIndex()
{
  struct ifreq if_idx;
  /* Get the index of the interface to send on */
  memset(&if_idx, 0, sizeof(struct ifreq));
  strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
  if (ioctl(sockfd_w, SIOCGIFINDEX, &if_idx) < 0) {
    perror("SIOCGIFINDEX");
    return -1;
  }
  index = if_idx.ifr_ifindex;
  if (Verbose)
    printf("Got index %d\n",index);
  return index;
}

bool FEBDTP::GetMAC()
{
  struct ifreq if_mac;
  memset(&if_mac, 0, sizeof(struct ifreq));
  strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
  if (ioctl(sockfd_w, SIOCGIFHWADDR, &if_mac) < 0) {
    perror("SIOCGIFHWADDR");
    return false;
  }

  // Copy to local variale 
  memcpy(&srcmac,((uint8_t *)&if_mac.ifr_hwaddr.sa_data),6);
  if (Verbose) {
    for (int i = 0; i < 6; i++) printf("%02x ",srcmac[i]);
    printf("\n");
  }
  
  return true;
}  
  
bool FEBDTP::Init(const char * iface) 
{
  Verbose = 1;
  tv.tv_sec  = 0;  /* 0 Secs Timeout */
  tv.tv_usec = 500000;  // 500ms

  int sockfd;

  sprintf(ifName,"%s",iface);

  // Initialize the packet structyre 
  Init_FEBDTP_pkt();		// 
  
  /* Open RAW socket to send on */
  if ((sockfd_w = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
    perror("sender: socket");
    return false;
  }
  if (Verbose) printf("Got write socket %d\n", sockfd_w);
  
  /* Open PF_PACKET socket, listening for EtherType ETHER_TYPE */
  if ((sockfd_r = socket(PF_PACKET, SOCK_RAW, gpkt.iptype)) == -1) {
    perror("listener: socket");	
    return false;
  }
  if (Verbose) printf("Got read socket %d\n", sockfd_r);

  // Set socket options
  if (!SetTimeout(500000)) return false;
  
  /* Bind listener to device */
  if (setsockopt(sockfd_r, SOL_SOCKET, SO_BINDTODEVICE,
		 ifName, IFNAMSIZ-1) == -1) {
    perror("SO_BINDTODEVICE");
    return false;
  }
  if (Verbose) printf("Read socket %d bound to device %s",sockfd_r,ifName);
  
  /* Get the index of the interface to send on */
  GetIndex();
  
  /* Get the MAC address of the interface to send on */
  GetMAC();

  // Re-Initialize the packet structyre 
  Init_FEBDTP_pkt();
  
  return true;
}

void FEBDTP::Init_FEBDTP_pkt(FEBDTP_PKT_t *pkt, UChar_t* src, UChar_t* dst)
{
  // Set source and destination mac addresses 
  memcpy(&(pkt->src_mac),src,6);
  memcpy(&(pkt->dst_mac),dst,6);
  
  pkt->iptype=0x0108; // IP type 0x0801
}

int FEBDTP::Send_pkt(FEBDTP_PKT_t *pkt, int len, int timeout_us)
{
  struct ifreq if_idx;
  struct ifreq if_mac;

  int      numbytes;
  int      retnumbytes;
  int      numpacks=0;
  UChar_t  thisdstmac[6];

  // Store destination destination 
  memcpy(thisdstmac,pkt->dst_mac,6);

  struct sockaddr_ll socket_address;
  int  jj;
  char str[32];

  /* Get the index of the interface to send on */
  // int idx = GetIndex();
  // if (idx < 0) return 0;
  
  /* Get the MAC address of the interface to send on */
  // GetMAC();

  // set receive timeout
  if (!SetTimeout(timeout_us)) return 0;

  /* Index of the network device */
  socket_address.sll_ifindex = index;
  socket_address.sll_halen   = ETH_ALEN;
  socket_address.sll_addr[0] = pkt->dst_mac[0];
  socket_address.sll_addr[1] = pkt->dst_mac[1];
  socket_address.sll_addr[2] = pkt->dst_mac[2];
  socket_address.sll_addr[3] = pkt->dst_mac[3];
  socket_address.sll_addr[4] = pkt->dst_mac[4];
  socket_address.sll_addr[5] = pkt->dst_mac[5];
  CMD_stoa(gpkt.CMD,str);

  if (Verbose>0) {
    printf(">> (%4d)", len);
    Print_pkt(*pkt,len);
  }

  /* Send packet */
  if (sendto(sockfd_w, (char*)pkt, len, 0,
	     (struct sockaddr*)&socket_address,
	     sizeof(struct sockaddr_ll)) < 0) {
    if(Verbose>0)
      printf("***  Send CMD=%s failed!\n",str);
    return 0;
  }
  numbytes=0;

  // Recieve the reply 
  while(numbytes>=0) {
    //  memset(gpkt.Data,0,MAXPAYLOAD);
    // Get from read socket 
    numbytes = recvfrom(sockfd_r, &gpkt, MAXPACKLEN, 0, NULL, NULL);

    if(numbytes<0 && numpacks==0) {
      if(Verbose>0)
	printf("No reply within %ld us!\n",timeout_us);
      return 0;
    }

    if (numbytes<0) return retnumbytes;

    // Check if NOT from our FEB - hmm, perhaps this should be earlier? 
    if(gpkt.src_mac[5] != thisdstmac[5] && thisdstmac[5] !=0xff) {
      if(Verbose>0)
	printf("****************  Packet received from wrong FEB: "
	       "0x%02x (must be 0x%02x) Skipping..\n",
	       gpkt.src_mac[5], thisdstmac[5]);
      continue;
    }
    
    if (gpkt.CMD==FEB_DATA_CDR || gpkt.CMD==FEB_DATA_FW) {
      //received event data buffer
      // Print_gpkt_evts(numbytes);
      if(fPacketHandler)
	//Do something with received data
	fPacketHandler(numbytes); 
    }
    //else  
    if (Verbose>0) {
      printf("<< (%4d) ", numbytes);
      Print_gpkt(numbytes);
    }

    //else return retnumbytes;
    retnumbytes=numbytes;
    numpacks++;
  }
  return retnumbytes;
}

bool FEBDTP::SendCMD(UChar_t* mac, UShort_t cmd, UShort_t reg, UChar_t* buf)
{
  int   numbytes = 1;

  // set short timeout and Flush input buffer 
  tv.tv_usec=10;

  if (!SetTimeout(10)) return false;

  // Read from socket - flush input buffer
  if (Verbose)
    printf("Flushing input buffers\n");
  while (numbytes>0) 
    numbytes = recvfrom(sockfd_r, &gpkt, MAXPACKLEN, 0, NULL, NULL);

  // Initialize our packet 
  Init_FEBDTP_pkt();

  // Set destination, command (2 bytes), and register (2 bytes)
  memcpy(&gpkt.dst_mac,mac,6);        
  memcpy(&gpkt.CMD,&cmd,2);
  memcpy(&gpkt.REG,&reg,2);

  // Default timeout 50 ms
  int tout= 50000;
  // Default packet length 
  int packlen = 64;

  // Change package and timeout based on which command 
  switch (cmd) {
  case FEB_GET_RATE :
  case FEB_GEN_INIT : 
  case FEB_GEN_HVON : 
  case FEB_GEN_HVOF : 
    tout=1000; //timeout 1ms
    break;
  case FEB_SET_RECV :
    // Copy source mac to the data buffer
    memcpy(&gpkt.Data,buf,6); 
    gpkt.REG=VCXO;
    tout=500; //timeout 0.5ms
    break;
  case FEB_WR_SR : 
    // Copy register value to the data buffer 
    memcpy(&gpkt.Data,buf,1); 
    break;
  case FEB_WR_SRFF : 
  case FEB_WR_SCR  : 
  case FEB_WR_PMR  : 
  case FEB_WR_FIL  : 
  case FEB_WR_CDR : 
    // Copy 256 register values to the data buffer
    memcpy(&gpkt.Data,buf,256);
    for (size_t i = 0; i < 256; i++) {
      if (i % 8 == 0)
	std::cout << "\n"
		  << std::dec << std::setfill(' ')
		  << std::setw(4) << i << ' '
		  << std::hex << std::setfill('0');
      for (size_t j = 0; j < 8; j++) 
	std::cout << std::setw(1) << ((unsigned(buf[i]) & (1 << j)) >> j);
      std::cout << ' ';
    }
    std::cout << std::endl;
    packlen=256+18; 
    break;
  case FEB_RD_CDR : 
    tout=5000; //timeout 150ms
    break;
  case FEB_RD_FW : 
  case FEB_WR_FW :
    // Copy address and numblocks
    memcpy(&gpkt.Data,buf,5); 
    break;
  case FEB_DATA_FW :
    // Copy 1kB  data buffer
    memcpy(&gpkt.Data,buf,1024); 
    packlen=1024+18;
    tout=5000; //timeout 5ms
    break;
  }

  // Send the package 
  int recvlen = Send_pkt(&gpkt, packlen,tout);

  // If no reply or invalid 
  if(recvlen<=0) return false;
  
  // analyse received packet in gpkt
  bool retval = false; //default - error
  switch (gpkt.CMD) {
  case FEB_OK : 
    retval = true;
    break;
  case FEB_OK_SR : 
  case FEB_OK_SCR : 
  case FEB_OK_PMR : 
  case FEB_OK_FIL : 
  case FEB_OK_FW :
    // Copy register value(s) from the data buffer 
    memcpy(buf,&gpkt.Data,(recvlen>256)?256:1); 
    retval = true;
    break;
  case FEB_DATA_CDR : 
  case FEB_DATA_FW :
    // Copy register value(s) from the data buffer 
    // memcpy(buf,&gpkt.Data,(recvlen>MAXPAYLOAD)?MAXPAYLOAD:recvlen); 
    retval=true;
    break;
  case FEB_ERR_SR : 
  case FEB_ERR_SCR : 
  case FEB_ERR_PMR : 
  case FEB_ERR_FIL : 
  case FEB_ERR_CDR : 
  case FEB_ERR_FW : 
    printf("Error code in reply.\n");
    break;
  case FEB_EOF_CDR : 
  case FEB_EOF_FW : 
    retval = false;
    break;
  default : 
    printf("Unrecognized code RECEIVED FROM CLIENT! recvlen=%d\n",recvlen);
    Print_gpkt(recvlen);
  }
   
  return retval;
}


void FEBDTP::CMD_stoa(UShort_t cmd, char* str)
{
  switch ( cmd ) {
  case FEB_RD_SR :	sprintf(str,"FEB_RD_SR");	break;
  case FEB_WR_SR :	sprintf(str,"FEB_WR_SR");	break;
  case FEB_RD_SRFF :	sprintf(str,"FEB_RD_SRFF");	break;
  case FEB_WR_SRFF :	sprintf(str,"FEB_WR_SRFF");	break;
  case FEB_OK_SR :	sprintf(str,"FEB_OK_SR");	break;
  case FEB_ERR_SR :	sprintf(str,"FEB_ERR_SR");	break;
  case FEB_SET_RECV :	sprintf(str,"FEB_SET_RECV");	break;
  case FEB_GEN_INIT :	sprintf(str,"FEB_GEN_INIT");	break;
  case FEB_GET_RATE :	sprintf(str,"FEB_GET_RATE");	break;
  case FEB_GEN_HVON :	sprintf(str,"FEB_GEN_HVON");	break;
  case FEB_GEN_HVOF :	sprintf(str,"FEB_GEN_HVOF");	break;
  case FEB_OK :		sprintf(str,"FEB_OK");		break;
  case FEB_ERR :	sprintf(str,"FEB_ERR");		break;
  case FEB_RD_SCR :	sprintf(str,"FEB_RD_SCR");	break;
  case FEB_WR_SCR  :	sprintf(str,"FEB_WR_SCR");	break;
  case FEB_OK_SCR :	sprintf(str,"FEB_OK_SCR");	break;
  case FEB_ERR_SCR :	sprintf(str,"FEB_ERR_SCR");	break;
  case FEB_RD_PMR :	sprintf(str,"FEB_RD_PMR");	break;
  case FEB_WR_PMR  :	sprintf(str,"FEB_WR_PMR");	break;
  case FEB_OK_PMR :	sprintf(str,"FEB_OK_PMR");	break;
  case FEB_ERR_PMR :	sprintf(str,"FEB_ERR_PMR");	break;
  case FEB_RD_FIL :	sprintf(str,"FEB_RD_FIL");	break;
  case FEB_WR_FIL  :	sprintf(str,"FEB_WR_FIL");	break;
  case FEB_OK_FIL :	sprintf(str,"FEB_OK_FIL");	break;
  case FEB_ERR_FIL :	sprintf(str,"FEB_ERR_FIL");	break;
  case FEB_RD_CDR :	sprintf(str,"FEB_RD_CDR");	break;
  case FEB_WR_CDR :	sprintf(str,"FEB_WR_CDR");	break;
  case FEB_DATA_CDR :	sprintf(str,"FEB_DATA_CDR");	break;
  case FEB_ERR_CDR :	sprintf(str,"FEB_ERR_CDR");	break;
  case FEB_EOF_CDR :	sprintf(str,"FEB_EOF_CDR");	break;
  case FEB_RD_FW :	sprintf(str,"FEB_RD_FW");	break;
  case FEB_WR_FW :	sprintf(str,"FEB_WR_FW");	break;
  case FEB_DATA_FW :	sprintf(str,"FEB_DATA_FW");	break;
  case FEB_ERR_FW :	sprintf(str,"FEB_ERR_FW");	break;
  case FEB_EOF_FW :	sprintf(str,"FEB_EOF_FW");	break;
  case FEB_OK_FW :	sprintf(str,"FEB_OK_FW");	break;

  default : 
    sprintf(str,"Unrecognized code");
  }
}

void FEBDTP::Print_pkt(FEBDTP_PKT_t& pkt,int truncat)
{
  char str[32];
  printf("(%02x %02x %02x %02x %02x %02x) => ",
	 pkt.src_mac[0],pkt.src_mac[1],pkt.src_mac[2],
	 pkt.src_mac[3],pkt.src_mac[4],pkt.src_mac[5]);
  printf("(%02x %02x %02x %02x %02x %02x) ",
	 pkt.dst_mac[0],pkt.dst_mac[1],pkt.dst_mac[2],
	 pkt.dst_mac[3],pkt.dst_mac[4],pkt.dst_mac[5]);
  printf("IP: %04x ",pkt.iptype);
  CMD_stoa(pkt.CMD,str);
  printf("CMD: %04x (%s)",pkt.CMD,str);
  printf(" REG: %04x",pkt.REG);
  int n = truncat - 18 > 0 ? truncat - 18 : 0;
  for(int jj = 0; jj < truncat-18; jj++) {
    if (jj % 6 == 0) printf("\n");
    printf("%02x ",pkt.Data[jj]);
  }
  printf("\n");
}

void FEBDTP::Print_gpkt_evts(int truncat)
{
  /*typedef struct {
    UChar_t flags; //flags defining event type, 1=T0 reset, 2=T1 reset or 4=scintillator trigger
    UInt_t T0;
    UInt_t T1;
    UShort_t adc[32]; //adc data on 32 channels
    } Event_t;
  */

  int jj;
  int kk;
  UInt_t T0,T1;
  UShort_t adc;
  char str[32];
  printf("(%02x %02x %02x %02x %02x %02x) => ",
	 gpkt.src_mac[0],gpkt.src_mac[1],gpkt.src_mac[2],
	 gpkt.src_mac[3],gpkt.src_mac[4],gpkt.src_mac[5]);
  printf("(%02x %02x %02x %02x %02x %02x) ",
	 gpkt.dst_mac[0],gpkt.dst_mac[1],gpkt.dst_mac[2],
	 gpkt.dst_mac[3],gpkt.dst_mac[4],gpkt.dst_mac[5]);
  printf("IP: %04x",gpkt.iptype);
  CMD_stoa(gpkt.CMD,str);
  printf("CMD: %04x (%s)",gpkt.CMD,str);
  printf(" Remaining events: %d\n",gpkt.REG);
  printf("Events: \n");
  jj=0;
  while(jj<truncat-18)
    {
      //	printf("Flags: %02x ",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
        
      printf("Flags: 0x%08x ",*(UInt_t*)(&gpkt.Data[jj]));jj=jj+4;
      T0=*(UInt_t*)(&gpkt.Data[jj]); jj=jj+4; 
      //	printf("T0: %02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      T1=*(UInt_t*)(&gpkt.Data[jj]); jj=jj+4; 
      printf("T0=%u ns, T1=%u ns,  ",T0,T1);

      //	printf(" T1: %02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;
      //	printf("%02x",gpkt.Data[jj]); jj++;

      printf(" ADC[32]:\n"); 
      //     for(kk=0; kk<32; kk++){ printf("%02x ",gpkt.Data[jj]);jj++; } //if(jj>=(truncat-18)) return;}
      //      printf("\n");
      //      for(kk=0; kk<32; kk++){ printf("%02x ",gpkt.Data[jj]);jj++; } //if(jj>=(truncat-18)) return;}
      for(kk=0; kk<32; kk++)
	{
	  adc=*(UShort_t*)(&gpkt.Data[jj]); jj++; jj++;  
	  printf("%04u ",adc);		
	} //if(jj>=(truncat-18)) return;}
      printf("\n");
    }
  printf("\n");        
}

int FEBDTP::ScanClients(int max) // Scan MAC addresses of FEBs within reach
{
  int num=0;
  for(num=0;num<=max;num++) {
    memcpy(macs[nclients],dstmac,6);

    macs[nclients][5]=num;

    if(SendCMD(macs[nclients],FEB_SET_RECV,0,srcmac)>0)  { 
      for(int i=0;i<5;i++)
	printf("%02x:",macs[nclients][i]);

      printf("%02x ",macs[nclients][5]);
      printf("%s\n",gpkt.Data);  nclients++;} // client reply received
  }
  printf("\n%d clients found.\n",nclients);

  return nclients; 
}

UChar_t SwapBits( UChar_t bt)
{
  int i=0;
  UChar_t retval=0;
  retval |= (bt & 0x01) << 7;
  retval |= (bt & 0x02) << 5;
  retval |= (bt & 0x04) << 3;
  retval |= (bt & 0x08) << 1;
  retval |= (bt & 0x10) >> 1;
  retval |= (bt & 0x20) >> 3;
  retval |= (bt & 0x40) >> 5;
  retval |= (bt & 0x80) >> 7;
  return retval;
}

// write CITIROC SC bitstream from the buffer, buf[MAXPACKLEN], to
// LabView setup file
void FEBDTP::WriteLVBitStream(const char * fname, UChar_t* buf, Bool_t rev) 
{
  int i=0;
  FILE *file = fopen(fname, "w");
  //   fprintf(file,"Normal:\n");  
  if(!rev)
    for(i=0;i<143;i++)
      {
	fprintf(file,"%02x",buf[i]);
      }
  //   fprintf(file,"\nReverse:\n");  
  else for(i=142;i>=0;i--)
	 {
	   fprintf(file,"%02x",SwapBits(buf[i]));
	 }
  fprintf(file,"\n");  
  fclose(file);

}

void FEBDTP::ReadLVBitStream(const char * fname, UChar_t* buf, Bool_t rev) 
{
  int i=0;
  UChar_t bt;
  char line[1024];
  unsigned int ibt;
  FILE *file = fopen(fname, "r");
  fgets(line, sizeof(line), file); //one line only
  if(!rev)
    for(i=0;i<143;i++)     //Read normal ordered file
      {
	sscanf(&line[i*2],"%02x",&ibt);
	buf[i]=ibt;
      }
  else 
    for(i=0;i<143;i++)     //Read reverse ordered file
      {
	sscanf(&line[i*2],"%02x",&ibt);
	bt=ibt;
	buf[142-i]=SwapBits(bt);
      }

  fclose(file);

}
//
// EOF
//


