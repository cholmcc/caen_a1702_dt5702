#ifdef __MAKECINT__

//#pragma link off all typedef;
#pragma link off all globals;
#pragma link off all functions;
#pragma link off all classes;

#pragma link C++ class BASE_FEBDTP+;
#pragma link C++ class DUMMY_FEBDTP+;
#pragma link C++ class FEBDTP+;

#endif /* __MAKECINT__ */
