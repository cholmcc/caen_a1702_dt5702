#ifndef BASE_FEBDTP_HXX
#define BASE_FEBDTP_HXX
#include <TObject.h>
#include "febdrv.h"
#include <cstdio>
#include <cstring>


class BASE_FEBDTP : public TObject
{
public:
  FEBDTP_PKT_t   gpkt;
  int            nclients=0;
  UChar_t        macs[256][6]; //list of detected clients
  int            Verbose=0;
  unsigned short VCXO=500; 
  UChar_t        srcmac[6]={0,0,0,0,0,0};
  UChar_t        dstmac[6]={0x00,0x60,0x37,0x12,0x34,0x00}; 

  // Packet handler function 
  void (*fPacketHandler)(int)=0;

  // default constructor
  BASE_FEBDTP(){}; 
  //main constructor
  BASE_FEBDTP(const char * iface)
  {
    for (size_t i = 0; i < 6; i++) {
      gpkt.src_mac[i] = srcmac[i];
      gpkt.dst_mac[i] = dstmac[i];
    }
    gpkt.iptype = 0x0108; // Little endian
    gpkt.REG    = 0x0000; 
    gpkt.CMD    = 0x0000; 
    memset(gpkt.Data,0,MAXPAYLOAD);
	
  };
  //destructor
  virtual ~BASE_FEBDTP(){}

  // Set timeout 
  virtual bool SetTimeout(unsigned int microseconds) { return true; }

  // Send a command 
  virtual bool SendCMD(UChar_t* mac,
		       UShort_t cmd,
		       UShort_t reg,
		       UChar_t* buf) = 0;

  // Scan MAC addresses of FEBs within reach
  virtual int ScanClients(int max=4) = 0;

  // read CITIROC SC bitstream into the buffer
  int ReadBitStream(const char * fname, UChar_t* buf) {
    FILE* file = fopen(fname, "r");
    if (file==0) return 0;

    char line[128];
    char bits[128];
    char comment[128];
    char bit; 
    int  ptr, byteptr;
    int  bitlen=0;
    char ascii[MAXPACKLEN];
    while (fgets(line, sizeof(line), file)) {
      bit=1;
      ptr=0;
      byteptr=0;

      // ASCII(0x27)= '
      while(bit!=0x27 && bit!=0 && ptr<sizeof(line) && bitlen<MAXPACKLEN) {
	bit=line[ptr];
	ptr++;
	
	if(bit==0x20 || bit==0x27) continue; //ignore spaces and apostrophe

	if (Verbose) printf("%c",bit);

	ascii[bitlen]=bit;
	bitlen++;
      }
    }

    printf("\nFEBDTP::ReadBitStream: %d bits read from file %s.\n",bitlen,fname);
    fclose(file);
  
    memset(buf,0,MAXPACKLEN); //reset buffer
    // Now encode ASCII bitstream into binary
    for(ptr=bitlen-1;ptr>=0;ptr--) {
      byteptr=(bitlen-ptr-1)/8;
      if(ascii[ptr]=='1')
	buf[byteptr] |= (1 << (7 - ptr % 8));

      if (Verbose)
	printf("%4d -> byte # %4d shift %2d\n",
	       ptr, byteptr, (7 - ptr % 8));
      // if((ptr%8)==0)
      //    printf("bitpos=%d buf[%d]=%02x\n",ptr,byteptr,buf[byteptr]);
    }
    
    return bitlen;
  }
    
  // write CITIROC SC bitstream into file
  int WriteBitStream(const char * fname, UChar_t* buf, int bitlen) {
    FILE *  file = fopen(fname, "w");
    UChar_t byte;
    int ib=0;
    char ascii[MAXPACKLEN];
    for(int i=bitlen/8-1;i>=0;i--) {
      byte=buf[i];

      for(int j=0;j<8;j++) {
	if(byte & 0x80) ascii[ib]='1'; else ascii[ib]='0';
	byte = byte<<1;
	ib++;
      }
    }
    for(int i=0;i<bitlen;i++) fprintf(file,"%c",ascii[i]); 
    fprintf(file,"\n"); 

    fclose(file);

    return bitlen;
  }
  // Write bit stream 
  int WriteBitStreamAnnotated(const char* fname,
			      UChar_t*    buf,
			      int         bitlen) 
  { 
    FILE*   file = fopen(fname, "w");
    UChar_t byte;
    int     ib = 0;
    char    ascii[MAXPACKLEN];
    
    for(int i=bitlen/8-1;i>=0;i--) {
      byte=buf[i];
      for(int j=0;j<8;j++) {
	if(byte & 0x80)
	  ascii[ib]='1';
	else
	  ascii[ib]='0';

	byte = byte<<1;
	ib++;
      }
    }

    //for(int i=0;i<bitlen;i++) fprintf(file,"%c",ascii[i]);
    ib=0;
    for(int Ch=0; Ch<32;Ch++) {
      fprintf(file,"%c%c%c%c \' Ch%d 4-bit DAC_t ([0..3])\'\n",
	      ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3],Ch);
      ib+=4;
    }
    
    for(int Ch=0; Ch<32;Ch++) {
      fprintf(file,"%c%c%c%c \' Ch%d 4-bit DAC ([0..3])\'\n",
	      ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3],Ch);
      ib+=4;
    }
    fprintf(file,"%c \'Enable discriminator\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable trigger discriminator power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Select latched (RS : 1) or direct output (trigger : 0)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Discriminator Two\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable trigger discriminator power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' EN_4b_dac\'\n",ascii[ib++]);
    fprintf(file,"%c \'PP: 4b_dac\'\n",ascii[ib++]);
    fprintf(file,"%c \' EN_4b_dac_t\'\n",ascii[ib++]);
    fprintf(file,"%c \'PP: 4b_dac_t\'\n",ascii[ib++]);
    for(int Ch=0; Ch<32;Ch++) fprintf(file,"%c",ascii[ib++]);
    fprintf(file," \' Allows to Mask Discriminator (channel 0 to 31) [active low]\'\n");
    fprintf(file,"%c \' Disable High Gain Track & Hold power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable High Gain Track & Hold\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable Low Gain Track & Hold power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Low Gain Track & Hold\'\n",ascii[ib++]);
    fprintf(file,"%c \' SCA bias ( 1 = weak bias, 0 = high bias 5MHz ReadOut Speed)\'\n",ascii[ib++]);
    fprintf(file,"%c \'PP: HG Pdet\'\n",ascii[ib++]);
    fprintf(file,"%c \' EN_HG_Pdet\'\n",ascii[ib++]);
    fprintf(file,"%c \'PP: LG Pdet\'\n",ascii[ib++]);
    fprintf(file,"%c \' EN_LG_Pdet\'\n",ascii[ib++]);
    fprintf(file,"%c \' Sel SCA or PeakD HG\'\n",ascii[ib++]);
    fprintf(file,"%c \' Sel SCA or PeakD LG\'\n",ascii[ib++]);
    fprintf(file,"%c \' Bypass Peak Sensing Cell\'\n",ascii[ib++]);
    fprintf(file,"%c \' Sel Trig Ext PSC\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable fast shaper follower power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable fast shaper\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable fast shaper power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable low gain slow shaper power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Low Gain Slow Shaper\'\n",ascii[ib++]);
    
    fprintf(file,"%c%c%c \' Low gain shaper time constant commands (0…2)  [active low] 100\'\n",ascii[ib],ascii[ib+1],ascii[ib+2]);
    ib+=3;
    
    fprintf(file,"%c \' Disable high gain slow shaper power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable high gain Slow Shaper\'\n",ascii[ib++]);
    
    fprintf(file,"%c%c%c \' High gain shaper time constant commands (0…2)  [active low] 100\'\n",ascii[ib],ascii[ib+1],ascii[ib+2]);
    ib+=3;
    
    fprintf(file,"%c \' Low Gain PreAmp bias ( 1 = weak bias, 0 = normal bias)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable High Gain preamp power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable High Gain preamp\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable Low Gain preamp power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Low Gain preamp\'\n",ascii[ib++]);
    fprintf(file,"%c \' Select LG PA to send to Fast Shaper\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable 32 input 8-bit DACs\'\n",ascii[ib++]);
    fprintf(file,"%c \' 8-bit input DAC Voltage Reference (1 = external 4,5V , 0 = internal 2,5V)\'\n",ascii[ib++]);
    
    for(int Ch=0; Ch<32;Ch++) {
      fprintf(file,"%c%c%c%c",ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3]); ib+=4;
      fprintf(file,"%c%c%c%c",ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3]); ib+=4;
      fprintf(file," %c \' Input 8-bit DAC Data channel %d – (DAC7…DAC0 + DAC ON), higher-higher bias\'\n",ascii[ib++],Ch);
    }
    
    for(int Ch=0; Ch<32;Ch++) {
      fprintf(file,"%c%c%c",ascii[ib],ascii[ib+1],ascii[ib+2]); ib+=3;
      fprintf(file,"%c%c%c ",ascii[ib],ascii[ib+1],ascii[ib+2]); ib+=3;
      fprintf(file,"%c%c%c",ascii[ib],ascii[ib+1],ascii[ib+2]); ib+=3;
      fprintf(file,"%c%c%c ",ascii[ib],ascii[ib+1],ascii[ib+2]); ib+=3;
      fprintf(file,"%c%c%c ",ascii[ib],ascii[ib+1],ascii[ib+2]); ib+=3;
      fprintf(file,"\' Ch%d   PreAmp config (HG gain[5..0], LG gain [5..0], CtestHG, CtestLG, PA disabled)\'\n",Ch);
    }


    fprintf(file,"%c \' Disable Temperature Sensor power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Temperature Sensor\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable BandGap power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable BandGap\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable DAC1\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable DAC1 power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable DAC2\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable DAC2 power pulsing mode (force ON)\'\n",ascii[ib++]);
    
    fprintf(file,"%c%c ",ascii[ib],ascii[ib+1]); ib+=2;
    fprintf(file,"%c%c%c%c ",ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3]); ib+=4;
    fprintf(file,"%c%c%c%c \' 10-bit DAC1 (MSB-LSB): 00 1100 0000 for 0.5 p.e.\'\n",ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3]); ib+=4;
    
    fprintf(file,"%c%c ",ascii[ib],ascii[ib+1]); ib+=2;
    fprintf(file,"%c%c%c%c ",ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3]); ib+=4;
    fprintf(file,"%c%c%c%c \' 10-bit DAC2 (MSB-LSB): 00 1100 0000 for 0.5 p.e.\'\n",ascii[ib],ascii[ib+1],ascii[ib+2],ascii[ib+3]); ib+=4;
    
    
    fprintf(file,"%c \' Enable High Gain OTA'  -- start byte 2\n",ascii[ib++]);
    fprintf(file,"%c \' Disable High Gain OTA power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Low Gain OTA\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable Low Gain OTA power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Probe OTA\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable Probe OTA power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Otaq test bit\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Val_Evt receiver\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable Val_Evt receiver power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable Raz_Chn receiver\'\n",ascii[ib++]);
    fprintf(file,"%c \' Disable Raz Chn receiver power pulsing mode (force ON)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable digital multiplexed output (hit mux out)\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable digital OR32 output\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable digital OR32 Open Collector output\'\n",ascii[ib++]);
    fprintf(file,"%c \' Trigger Polarity\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable digital OR32_T Open Collector output\'\n",ascii[ib++]);
    fprintf(file,"%c \' Enable 32 channels triggers outputs\'\n",ascii[ib++]);
  // for(int i=ib;i<bitlen;i++) fprintf(file,"%c",ascii[i]);
    fprintf(file,"\n"); 
    fclose(file);
    return bitlen;
  }
    
  // Set call-back 
  void setPacketHandler( void (*fhandler)(int)=0) { fPacketHandler=fhandler; }
  
  // Print list of discovered mac addresses 
  void PrintMacTable() {
    printf("Clients table:\n");
    for(int feb=0; feb<nclients;feb++)  {
      for(int i=0;i<5;i++)
	printf("%02x:",macs[feb][i]);
      printf("%02x ",macs[feb][5]);
      printf(" -  client %d\n",feb);
    }
  }
    
  ClassDef(BASE_FEBDTP, 0); // FEBDTP
};

#endif 
