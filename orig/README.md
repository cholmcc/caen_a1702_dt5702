# Original Software for CAEN A1702/DT5702

From [here](https://www.caen.it/download/?filter=DT5702) (requires log-in)

This is (almost) the original software.  However, I did make some
changes to it:

- Formatted the code - just to make it easier to read
- Fixed some minor issues with calling member functions
- Refactored the [`FEBDTP`](FEBDTP.hxx) class into with the base class
  [`BASE_FEBDTP`](BASE_FEBDTP.hxx).  This is to allow us to define an
  emulation interface in [`DUMMY_FEBDTP`](DUMMY_FEBDTP.hxx). 
  
  The emulation class [`DUMMY_FEBDTP`](DUMMY_FEBDTP.hxx) responds more
  or less as real hardware would do, but does not provide more
  advanced emulations as for example [`feb::dummy`](../feb/dummy.hh)
  does. 
  
  The main purpose of the emulation is to be able to reverse engineer
  the code. 
  

