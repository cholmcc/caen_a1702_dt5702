#ifndef DUMMY_FEBDTP_HXX
#define DUMMY_FEBDTP_HXX
#include "BASE_FEBDTP.hxx"
#include <sys/time.h>
#include <net/if.h>
#include <iostream>

class DUMMY_FEBDTP : public BASE_FEBDTP
{
public:
  // default constructor
  DUMMY_FEBDTP() : BASE_FEBDTP() {}; 

  //main constructor
  DUMMY_FEBDTP(const char * iface) : BASE_FEBDTP() {};


  std::string CMDstr(unsigned char family, unsigned char id) {
    std::string fam = "";
    std::string sub = "";
    
    // Common IDs
    switch (id) {
    case 0x0:  sub = "OK";   break;
    case 0x1:  sub = "RD";   break;
    case 0x2:  sub = "WR";   break;
    case 0x3:  sub = "EOF";  break;
    case 0xFF: sub = "ERR";  break;
    }
    
    switch (family) {
    case 0x0:
      switch (id) {
      case 0x3:  sub = "RD"; fam = "_SRFF"; break;
      case 0x4:  sub = "WR"; fam = "_SRFF"; break;
      default:               fam = "_SR";   break;
      }
      break;
    case 0x1:
      switch (id) {
      case 0x1: sub = "SET_RECV"; break;
      case 0x2: sub = "GEN_INIT"; break;
      case 0x3: sub = "GEN_HVON"; break;
      case 0x4: sub = "GEN_HVOF"; break;
      case 0x5: sub = "GET_RATE"; break;
      }
      break;
    case 0x2: fam = "_SCR"; break;
    case 0x3:
      fam = "_CDR";
      switch (id) {
      case 0x0: sub = "DATA"; break;
      }
      break;
    case 0x4: fam = "_PMR"; break;
    case 0x5:
      fam = "_FW";
      switch (id) {
      case 0x4: sub = "DATA"; break;
      }
      break;
    case 0x6: fam = "_FIL"; break;
    default:
      fprintf(stderr, "Unknown command familiy 0x%02x\n", family);
      return "";
    }

    return "FEB_" + sub +fam;
  }

  void PrintCMD(UChar_t*  src,
		UChar_t*  dst,
		UChar_t   family,
		UChar_t   id,
		UShort_t  reg,
		UChar_t*  buf,
		UInt_t    len)  {
    printf("(%02x %02x %02x %02x %02x %02x) => ",
	   src[0],src[1],src[2], src[3],src[4],src[5]);
    printf("(%02x %02x %02x %02x %02x %02x) ",
	   dst[0],dst[1],dst[2], dst[3],dst[4],dst[5]);
    printf("CMD: %02x%02x (%s)",family,id,CMDstr(family,id).c_str());
    printf(" REG: %04x",reg);
    for(int i = 0; i < len; i++)  {
      if (i % 6 == 0) printf("\n %4d ", i);
      printf("%02x ",buf[i]);
    }
    printf("\n");
  }
  bool GetBit(UChar_t *buffer, UShort_t bitlen, UShort_t bit_index)
  {
    UChar_t byte;
    UChar_t mask;
    byte = buffer[(bitlen-1-bit_index)/8];
    mask = 1 << (7-bit_index%8);
    byte = byte & mask;
    if(byte!=0)
      return true;
    
    return false; 
  }
  void PrintPayload(UChar_t* buf, UInt_t len) {
    printf("%9s","");
    for (int i = 0; i < 10; i++) printf(" %-4d",i*4);

    for (size_t bit = 0; bit < 8*len; bit++) {
      if (bit % 40 == 0) printf("\n%4lu %4lu", bit/8, bit);
      if (bit % 4 == 0) printf(" ");
      printf("%d", GetBit(buf, 8*len, bit));
    }
    printf("\n");
  }
  
  // Send a command 
  bool SendCMD(UChar_t* mac,
	       UShort_t cmd,
	       UShort_t reg,
	       UChar_t* buf) override
  {
    unsigned char family = (cmd >> 8);
    unsigned char id     = (cmd & 0xFF);
    std::string   nam    = CMDstr(family, id);

    std::cout << "Got command " << nam << " " << reg << std::endl;
    UInt_t len = 0;
    bool   bits = false;
    
    switch (family) {
    case 0x0: // Switch regisers 
      switch (id) {
      case 0x2: len = 1; break;
      case 0x4: len = 256; break;
      }
      break;
    case 0x1: // General commands 
      switch (id) {
      case 0x1: len = 6; break;
      }
      break;
    case 0x2: // Slow control 
      switch (id) {
      case 0x2: len = 143; bits = true; break;
      }
      break;
    case 0x3: // Data 
      break;
    case 0x4: // probe memory 
      switch (id) {
      case 0x2: len = 28; bits = true; break;
      }
      break;
    case 0x5: // FW
      switch (id) {
      case 0x1: len = 5; break; // Read at address (2 bytes) and length
      case 0x2: len = 5; break; // Write at address and length
      case 0x4: len = 1024; break; // Data to write
      }
      break;
    case 0x6: // FLT
      switch (id) {
      case 0x2: len = 9; bits = true; break;
      }
      break;
    }
    PrintCMD(srcmac,mac,family,id,reg,buf,len);
    if (bits) PrintPayload(buf,len);
    
    return true;
  }

  // Scan MAC addresses of FEBs within reach
  int ScanClients(int max=4) override {
    int num  = 0;
    nclients = 0;
    for(num=0;num<=max;num++) {
      memcpy(macs[nclients],dstmac,6);
      macs[nclients][5]=num;

      if (num == 0 || num == 1 || num == 16)
	nclients++;
    }
    return nclients;
  }

    
  ClassDef(DUMMY_FEBDTP, 0); // FEBDTP
};

#endif 
