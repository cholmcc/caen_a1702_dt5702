#ifndef FEBDTP_HXX
#define FEBDTP_HXX
#include "BASE_FEBDTP.hxx"
#include <sys/time.h>
#include <net/if.h>

class FEBDTP : public BASE_FEBDTP
{
public:
  UChar_t        brcmac[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

  char           ifName[IFNAMSIZ];
  int            sockfd_w=-1; 
  int            sockfd_r=-1;
  int            index=-1;
  struct timeval tv;

  // Packet handler function 
  void (*fPacketHandler)(int)=0;

  // default constructor
  FEBDTP() : BASE_FEBDTP() {}; 

  //main constructor
  FEBDTP(const char * iface) : BASE_FEBDTP() {Init(iface); };

  //destructor
  ~FEBDTP(){ close(sockfd_w); close(sockfd_r);}

  // Initialize - opens socket 
  bool Init(const char * iface); 

  // Set timeout 
  bool SetTimeout(unsigned int microseconds);

  // Get index of source interface 
  int GetIndex();

  // Get source MAC address
  bool GetMAC();
  
  // Initialize FEBDTP to send from src to dst 
  void Init_FEBDTP_pkt(FEBDTP_PKT_t *pkt, UChar_t* src, UChar_t* dst);

  // Initialize FEBDTP to send from stored src to stored dest 
  void Init_FEBDTP_pkt(FEBDTP_PKT_t *pkt) {
    Init_FEBDTP_pkt(pkt,srcmac,dstmac);
  }
  
  // Initialize global FEBDTP to stored source and destination 
  void Init_FEBDTP_pkt() { Init_FEBDTP_pkt(&gpkt); }

  // Send packet
  int Send_pkt(FEBDTP_PKT_t *pkt, int len, int timeout_us=50000);

  // Send a command 
  bool SendCMD(UChar_t* mac, UShort_t cmd, UShort_t reg, UChar_t* buf);

  // Print packet 
  void Print_pkt(FEBDTP_PKT_t& pkt,int truncat=0);
  void Print_gpkt(int truncat=0) { Print_pkt(gpkt,truncat); }

  // Print package of events 
  void Print_gpkt_evts(int truncat=MAXPACKLEN);

  //convert CMD code to name string
  void CMD_stoa(UShort_t cmd, char* str);

  // Scan MAC addresses of FEBs within reach
  int ScanClients(int max=4);

  // write CITIROC SC bitstream from the buffer, buf[MAXPACKLEN], to
  // LabView setup file
  void WriteLVBitStream(const char * fname, UChar_t* buf, Bool_t rev=false);

  // write CITIROC SC bitstream from the buffer, buf[MAXPACKLEN], to
   // LabView setup file
  void ReadLVBitStream(const char * fname, UChar_t* buf, Bool_t rev=false);
    
  ClassDef(FEBDTP, 0); // FEBDTP
};

#endif 
