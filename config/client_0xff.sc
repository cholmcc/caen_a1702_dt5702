# Time	Charge	On	Bias	Enable	High	Low	Test	Test	!Preamp
0	0	0	99	1	51	51	0	0	1	# Channel 0
0	0	0	211	1	51	51	0	0	1	# Channel 1
0	0	0	61	1	51	51	0	0	1	# Channel 2
0	0	0	83	1	51	51	0	0	1	# Channel 3
0	0	0	3	1	51	51	0	0	1	# Channel 4
0	0	0	211	1	51	51	0	0	1	# Channel 5
0	0	0	253	1	51	51	0	0	1	# Channel 6
0	0	0	83	1	51	51	0	0	1	# Channel 7
0	0	0	211	1	51	51	0	0	1	# Channel 8
0	0	0	211	1	51	51	0	0	1	# Channel 9
0	0	0	131	1	51	51	0	0	1	# Channel 10
0	0	0	163	1	51	51	0	0	1	# Channel 11
0	0	0	253	1	51	51	0	0	1	# Channel 12
0	0	0	147	1	51	51	0	0	1	# Channel 13
0	0	0	131	1	51	51	0	0	1	# Channel 14
0	0	0	179	1	51	51	0	0	1	# Channel 15
0	0	0	51	1	51	51	0	0	1	# Channel 16
0	0	0	125	1	51	51	0	0	1	# Channel 17
0	0	0	99	1	51	51	0	0	1	# Channel 18
0	0	0	195	1	51	51	0	0	1	# Channel 19
0	0	0	253	1	51	51	0	0	1	# Channel 20
0	0	0	131	1	51	51	0	0	1	# Channel 21
0	0	0	163	1	51	51	0	0	1	# Channel 22
0	0	0	3	1	51	51	0	0	1	# Channel 23
0	0	0	67	1	51	51	0	0	1	# Channel 24
0	0	0	157	1	51	51	0	0	1	# Channel 25
0	0	0	221	1	51	51	0	0	1	# Channel 26
0	0	0	3	1	51	51	0	0	1	# Channel 27
0	0	0	253	1	51	51	0	0	1	# Channel 28
0	0	0	195	1	51	51	0	0	1	# Channel 29
0	0	0	61	1	51	51	0	0	1	# Channel 30
0	0	0	99	1	51	51	0	0	1	# Channel 31
# High gain
1	# Disable track'n'hold pulser
1	# Enable track'n'hold
0	# PP peak detect
0	# Enable peak detect
1	# SCA or peak
1	# Disable shaper pulser
1	# Enable shaper
3	# Shaper time
1	# Disable preamp pulser
1	# Enable preamp
1	# Enable OTA
1	# Disable OTA pulser
# Low gain
1	# Disable track'n'hold pulser
1	# Enable track'n'hold
0	# PP peak detect
0	# Enable peak detect
1	# sca_or_peak
1	# Disable shaper pulser
1	# Enable shaper
3	# Shaper time
0	# Preamp bias mode
1	# Disable preamp pulser
1	# Enable preamp
0	# Send output
1	# Enable OTA
1	# Disable OTA pulser
# Thresholds
1	# Enable charge threshold/channel
1	# PP charge threshold/channel
1	# Enable time threshold/channel
1	# PP time threshold/channel
1	# Enable threshold 1
1	# Disable threshold 1 pulser
1	# Enable threshold 2
1	# Disable threshold 2 pulser
634	# Threshold 1 value
380	# Threshold 2 value
# Bias
1	# Enable bias voltage
1	# Bias refernce (0: internal, 1: external)
# Trigger
1	# Enable disc.
1	# Disable disc. pulser
0	# Latched (1: RS, 0: trig)
1	# Enable disc. 2
1	# Disable disc. 2 pulster
1	# By-pass peak sense cell
0	# External trigger
0	# Enable validate recv.
0	# Disable validate pulser
0	# Digital multiplex output
1	# Enable OR-32 output
0	# Enable OR-32 open coll.
0	# Polarity of trigger
0	# Enable OR-32 time
1	# Enable trigger out
# Misc
1	# 1: weak, 0: high 5MHz R/O
1	# Disable f/s follow pulser
1	# Enable f/s
1	# Disable f/s pulser
1	# Disable temperature pulser
1	# Enable temperature sensor
1	# Disable band-gap pulser
1	# Enable band-gap
1	# Enable probe OTA
1	# Disable probe OTA pulser
0	# OTAQ test 
0	# Enable RAZ recv.
0	# Disable RAZ rec. pulser
# EOF
