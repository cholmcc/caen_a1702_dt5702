#
#
#
PACKAGE		:= caen_a1702_dt5702
VERSION		:=
DISTNAME	:= $(PACKAGE)

CXX		:= g++
CPPFLAGS	:= -I.
CXXFLAGS	:= -O2 -fPIC -g -Wall -ansi -pedantic -std=c++11
LD		:= g++
LDFLAGS		:=
LIBS		:= 
SO		:= g++
SOFLAGS		:= -shared

ROOT_LIBDIR	:= $(shell root-config --libdir)
ROOT_CFLAGS	:= $(shell root-config --cflags)
ROOT_LIBS	:= $(filter -l%, $(shell root-config --libs)) \
		   -lROOTNTuple
ROOT_GLIBS	:= $(filter -l%, $(shell root-config --glibs)) \
		   -lROOTNTuple
ROOT_LDFLAGS	:= -Wl,-rpath,$(ROOT_LIBDIR) \
		   -Wl,-rpath-link,$(ROOT_LIBDIR) \
		   -Wl,-no-as-needed \
	           $(filter-out -l%, $(shell root-config --libs)) 
ROOT_CLING	:= rootcling

HEADERS		:= feb/base_host.hh		\
		   feb/client.hh		\
		   feb/command.hh		\
		   feb/commands.hh		\
		   feb/daq.hh			\
		   feb/dummy.hh			\
		   feb/event.hh			\
		   feb/flexible_logic.hh	\
		   feb/generator.hh		\
		   feb/host.hh			\
		   feb/log.hh			\
		   feb/memory.hh		\
		   feb/packet.hh		\
		   feb/probe.hh			\
		   feb/raw_daq.hh		\
		   feb/slow_control.hh		\
		   feb/utils.hh

RHEADERS	:= feb/root/DAQ.hh		\
		   feb/root/Event.hh		\
		   feb/root/Monitor.hh

GHEADERS	:= feb/gui/Bias.hh		\
		   feb/gui/Board.hh		\
		   feb/gui/Channel.hh		\
		   feb/gui/Channels.hh		\
		   feb/gui/Client.hh		\
		   feb/gui/DAQ.hh		\
		   feb/gui/Gains.hh		\
		   feb/gui/Log.hh		\
		   feb/gui/Main.hh		\
		   feb/gui/Misc.hh		\
		   feb/gui/Thresholds.hh	\
		   feb/gui/Trigger.hh		\
		   feb/gui/Utils.hh		\
		   feb/gui/GUI.C

SOURCES		:= first.cc			\
		   root_daq.cc			\
		   raw_daq.cc			\
		   gui.cc

MISC		:= README.md			\
		   config/client_0xff.sc	\
		   config/client_0xff.pb	\
		   config/client_0xff.fl	\
		   data/.keep			\
		   misc/manual.pdf		\
		   misc/FEB_rev3_FLX7_M4.bin	\
		   Makefile			\
		   gui.sh


TARGETS		:= $(SOURCES:%.cc=%)
TESTS		:= tests/test_setting_io.cc 	\
		   tests/test_log.cc 		\
		   tests/test_bits_view.cc	\
		   tests/test_encode.cc

TEST_TARGETS	:= $(TESTS:%.cc=%)

DISTFILES	:= $(HEADERS)			\
		   $(RHEADERS)			\
		   $(GHEADERS)			\
	           $(SOURCES)			\
		   $(TESTS)			\
		   $(MISC)


%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $<

%:%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

all:	$(TARGETS)
tests:	$(TEST_TARGETS)


first:			first.o
first.o:		first.cc $(HEADERS)

root_daq:		root_daq.o 
root_daq:		LDFLAGS:=$(LDFLAGS) $(ROOT_LDFLAGS)
root_daq:		LIBS:=$(LIBS) $(ROOT_LIBS)
root_daq.o:		root_daq.cc $(HEADERS) $(RHEADERS)
root_daq.o:		CXXFLAGS:=$(CXXFLAGS) $(ROOT_CFLAGS)

raw_daq:		raw_daq.o
raw_daq.o:		raw_daq.cc $(HEADERS)

dict.cc:		feb/gui/GUI.C # $(GHEADERS)
	$(ROOT_CLING) -f $@ -c $(CPPFLAGS) $^

gui:			gui.o dict.o 
gui:			LDFLAGS:=$(LDFLAGS) $(ROOT_LDFLAGS)
gui:			LIBS:=$(LIBS) $(ROOT_GLIBS)
gui.o:			root_daq.cc $(HEADERS) $(RHEADERS) $(GHEADERS)
gui.o:			CXXFLAGS:=$(CXXFLAGS) $(ROOT_CFLAGS)
dict.o:			dict.cc $(HEADERS) $(RHEADERS) $(GHEADERS)
dict.o:			CXXFLAGS:=$(CXXFLAGS) $(ROOT_CFLAGS)
dict.o:			CPPFLAGS:=$(CPPFLAGS) -DDAQ_INSTANCE=1 -DLOG_INSTANCE=1

tests/test_setting_io:	tests/test_setting_io.o
tests/test_setting_io.o:tests/test_setting_io.cc $(HEADERS)
tests/test_encode:	tests/test_encode.o
tests/test_encode.o:	tests/test_encode.cc   $(HEADERS)


doc:	misc/doxyconfig misc/layout.xml $(HEADERS) $(RHEADERS) $(GHEADERS) \
	$(SOURCES) $(TESTS)
	doxygen $< 

clean:
	rm -f $(TARGETS) feb/libfeb.so
	rm -f $(TEST_TARGETS)
	rm -f *.o feb/*.o tests/*.o *dict.* 
	rm -f feb/*.c feb/gui/*_C.* feb/gui/*.pcm
	rm -f *linkdef.h *_mainC *_map.in *_map.out daq
	rm -rf $(DISTNAME).tar.gz $(DISTNAME)
	rm -rf html 

realclean: clean
	rm -f *.sc *.fl *pb *.root data/*.root 

distdir:
	mkdir -p $(DISTNAME)

dist:distdir
	$(foreach f, $(DISTFILES), \
           mkdir -p $(DISTNAME)/$(dir $(f)); \
	   cp -a $(f) $(DISTNAME)/$(f);)
	tar -czvf $(DISTNAME).tar.gz $(DISTNAME)
	rm -rf $(DISTNAME)

distcheck:dist
	tar -xzvf $(DISTNAME).tar.gz
	$(MAKE) -C $(DISTNAME)
	rm -rf $(DISTNAME)


#
# EOF
#
