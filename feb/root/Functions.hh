#ifndef FEB_ROOT_FUNCTIONS
#define FEB_ROOT_FUNCTIONS

namespace feb
{
  namespace root
  {
    /** 
     * Functions used when fitting to signals 
     *
     */
    template <size_t MAX_PHOTONS=10>
    struct Functions
    {
      constexpr static size_t max_photons = 10;
      static double poisson_pdf(size_t n, double mean) {
	if (mean <= 1e-9) return 0;
	if (n    == 0)    return std::exp(-mean);
	
      }
      double mppc0(double x,
		   double normalisation,
		   double gain,
		   double pedestal,
		   double noise,
		   double avg_photons,
		   double xtra_width,
		   double x_talk)
      {
	std::valarray<double> i(MAX_PHOTONS);
	std::iota(std::begin(peakloc),std::end(peakloc));

	std::valarray<double> peakloc = i*gain + pedestal;
	
	std::valarray<double> intg(MAX_PHOTONS);
	if (avg_photos <= 1e-9) intg = 0;
	else {
	  intg    = std::exp(i * std::log(avg_photons) - avg_photons
			     - (i+1).apply(lgamma));
	  intg[0] = std::exp(-avg_photons);
	  intg[std::slice(1,MAX_PHOTONS-1,1)] *=
	    x_talk * intg[std::slice(0,MAX_PHOTONS-1,1)];
	}
	// Fold Poisson with Normal distribution 
	std::valarray<double> sigma = std::sqrt(i * xtra_width + noise * noise);
	double ret = (intg / (sqrt(2*M_PI) * sigma)
		      * std::exp(-std::pow((x - peakloc)/sigma,2)/2)).sum();

	return normalisation * ret;
      }
	  
		   
