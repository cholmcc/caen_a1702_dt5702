/** 
 * @file      feb/root/Event.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Event structure in ROOT output TTree
 */
#ifndef FEB_ROOT_EVENT_HH
#define FEB_ROOT_EVENT_HH
#include <feb/event.hh>
#include <Rtypes.h>

namespace feb
{
  namespace root
  {
    struct Event {
      // Decreasing size of types (not member size)
      UInt_t         t0;
      UInt_t         t1;
      UShort_t       overwritten;
      UShort_t       lost;
      UShort_t       adc[32];
      UChar_t        feb;
      Bool_t         t0_sync;
      Bool_t         t1_sync;
      Bool_t         t0_overflow;
      Bool_t         t1_overflow;

      void Set(unsigned char mac5,feb::event&   ev) {
	feb         = mac5;
	t0_sync     = ev.t0_sync;
	t1_sync     = ev.t1_sync;
	t0_overflow = ev.t0_overflow;
	t1_overflow = ev.t1_overflow;
	lost        = ev.lost;
	overwritten = ev.overwritten;
	t0          = ev.t0;
	t1          = ev.t1;
	for (size_t i = 0; i < 32; i++) adc[i]    = ev.adc[i];
      }
      const char* Spec() const {
	return 
	  "t0/i:t1:overwritten/s:lost:adc[32]/s:feb/b"
	  ":t0_sync/O:t1_sync:t0_overflow:t1_overflow";
      }
    };
  }
}
#endif
