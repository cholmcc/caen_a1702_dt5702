/** 
 * @file      feb/root/Monitor.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Monitor DAQ with ROOT
 */
#ifndef FEB_ROOT_MONITOR_HH
#define FEB_ROOT_MONITOR_HH
#include <TH1.h>
#include <TCanvas.h>
#include <feb/root/Event.hh>

namespace feb
{
  namespace root
  {
    struct Monitor
    {
      unsigned char fFEB;
      TH1*          fAdc[32];

      Monitor(std::pair<unsigned char,unsigned short> dat)
	: Monitor(dat.first, dat.second)
      {}
      /** 
       * Constructor 
       *
       * @param mac5 Last byte of MAC address 
       * @param max Maximum ADC value to register.  The ADC is 12bit
       * deep, so the absolute maximum is `0xFFF=4095`
       */
      Monitor(unsigned char mac5=0, unsigned short max=0xFFF)
	: fFEB(mac5),
	  fAdc{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
      {
	max = std::max(int(max),0xFFF);
	for (size_t i = 0; i < 32; i++) {
	  fAdc[i] = new TH1D(Form("feb_0x%02x_ch%02zu", mac5, i),
			     Form("0x%02x/%zu",mac5, i),
			     820, 0, max);
	  fAdc[i]->SetDirectory(0);
	  fAdc[i]->SetXTitle("ADC");
	  fAdc[i]->SetYTitle("Counts");
	  // Printf("CTOR 0x%02x %2zu: %p", fFEB, i, fAdc[i]);
	}
      }
      /** Destructor */
      ~Monitor() {
	for (size_t i = 0; i < 32; i++) {
	  // Printf("DTOR 0x%02x %2zu: %p", fFEB, i, fAdc[i]);
	  if (fAdc[i]) { delete fAdc[i]; fAdc[i] = 0; }
	}
      }
      /** Deleted copy constructor */
      Monitor(const Monitor&) = delete;
      /** Move copy constructor */
      Monitor(Monitor&& o) : fFEB(o.fFEB)
      {
	for (size_t i = 0; i < 32; i++) {
	  fAdc[i] = o.fAdc[i];
	  o.fAdc[i] = 0;
	}
      }
      /** Fill in observations */
      void Fill(const Event& ev) {
	// std::clog << "D: Filling in observations" << std::endl;
	for (size_t i = 0; i < 32; i++)
	  fAdc[i]->Fill(ev.adc[i]);
      }

      /** Draw the observations */
      void Draw(TCanvas& c, Int_t ch) {
	// std::clog << "D: Drawing monitor" << std::endl;
	if (ch >= 0) {
	  // std::clog << "D: Single channel " << ch << std::endl;
	  if (c.GetPad(32)) {
	    c.Clear();
	    c.SetLeftMargin(0.10);
	    c.SetBottomMargin(0.10);
	    c.SetTopMargin(0.02);
	    c.SetRightMargin(0.02);
	  }

	  c.SetLogy();
	  c.cd();
	  
	  fAdc[ch]->Draw();
	}
	else {
	  // std::clog << "D: All channels " << c.GetPad(31) << std::endl;
	  if (!c.GetPad(32)) {
	    // std::clog << "D: Dividing canvas " << std::endl;
	    c.Clear();
	    c.SetLeftMargin(0.10);
	    c.SetBottomMargin(0.10);
	    c.SetTopMargin(0.02);
	    c.SetRightMargin(0.02);
	    c.Divide(6,6,0,0);
	    for (size_t i = 0; i < 36; i++) {
	      auto p = c.GetPad(i+1);
	      // p->SetTopMargin(0.02);
	      // p->SetRightMargin(0.02);
	      p->SetLeftMargin(0.10);
	      p->SetRightMargin(0.01);
	      p->SetLogy();
	    }
	  }

	  for (size_t i = 0; i < 32; i++) {
	    c.cd(i+1);
	    fAdc[i]->Draw();
	  }
	}
	c.Modified();
	c.Update();
	// c.cd();
      }
    };
  }
}
#endif

	  
	  
