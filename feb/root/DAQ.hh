/** 
 * @file      feb/root/DAQ.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     DAQ using ROOT output in TTree
 */
#ifndef FEB_ROOT_DAQ_HH
#define FEB_ROOT_DAQ_HH
#include <TTree.h>
#include <TFile.h>
#include <TSystem.h>
#include <feb/daq.hh>
#include <feb/root/Event.hh>

namespace feb
{
  namespace root
  {

    struct DAQ : feb::daq
    {
      TTree*          fTree = 0;
      Event           fEvent;
      unsigned long   fRunNo;
      std::string     fDataDir;
	
      DAQ(feb::base_host& host,
	  unsigned long runno=0,
	  const std::string& datadir="")
	: feb::daq(host),
	  fRunNo(runno),
	  fDataDir(datadir)
      {
      }
      /** Filename */
      std::string FileName(unsigned long runNo) {
	std::string fn;
	if (!fDataDir.empty()) fn = fDataDir + "/";
	fn += Form("run_%06lu.root", runNo);
	return fn;
      }
      /** Write tree to disk */
      void Write()
      {
	if (!fTree) return;

	fTree->Write();
	if (fTree->GetCurrentFile()) {
	  std::clog << "I: Wrote tree to "
		    << fTree->GetCurrentFile()->GetName() << std::endl;
	  fTree->GetCurrentFile()->Close();
	}
	
	fTree = 0;
	fRunNo++;
      }
	
      /** 
       * Configure all cards and make sure high-voltage is on 
       */
      bool init() override {
	if (!feb::daq::init()) return false;

	// Possibly write the tree 
	Write();	
	
	// Open target file
	if (fRunNo <= 0) {
	  // Take next available run number 
	  unsigned long runNo = 0;
	  while (runNo < 999'999) {
	    if (gSystem->AccessPathName(FileName(runNo).c_str())) break;
	    runNo++;
	  }
	  if (runNo >= 999'999)
	    std::clog << "F: Ran out of run numbers" << std::endl;

	  fRunNo = runNo;
	}
	
	TFile::Open(FileName(fRunNo).c_str(),"RECREATE");
	fTree = new TTree("T","T");

	fTree->Branch("event",&fEvent,fEvent.Spec());

	return true;
      }

      /** 
       * Take a single event from a specific FEB. Can be overridden to
       * do monitoring for example.
       */
      bool process(feb::client& cl) override 
      {
	fEvent.Set(cl._mac5,_event);

	if (fTree) fTree->Fill();
		  
	// std::clog << "D: Read event # " << _nev << " from client "
	// 	  << feb::hex_out<2>("0x",cl._mac5) << " "
	// 	  << _event.t0 << "\t" << fEvent.t0 << std::endl;

	return true;
      }
      /** 
       * Starts the DAQ 
       */
      bool start() override
      {
	bool ret = feb::daq::start();
	if (ret) 
	  std::clog << "I: Starting run " << fRunNo << std::endl;
	return ret;
      }
      /** 
       * Stops the DAQ 
       */
      bool stop() override
      {
	if (!_running) return true;
	
	std::clog << "I: Stopping run " << fRunNo << std::endl;
	if (!feb::daq::stop()) return false;

	Write();

	return true;
      }
    };
  }
}
#endif

      
