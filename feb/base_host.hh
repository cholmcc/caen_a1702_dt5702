/** 
 * @file      feb/base_host.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Base class for hosts
 */
#ifndef FEB_BASE_HOST_HH
#define FEB_BASE_HOST_HH
#include <feb/command.hh>
#include <map>
#include <string>


namespace feb
{
  // forward declaration 
  struct client;

  /** 
   * Base class for host class 
   */
  struct base_host
  {
    /** Map of clients */
    using client_map = std::map<unsigned char,client>;
    
    /** Name of interface (e.g., "eth1") */
    std::string _interface;
    /** Souce MAC address */
    mac_address _source;
    /** Vector of connetec clients */
    client_map _clients;
    

    /** Constructor */
    base_host(const std::string& inter)
      : _interface(inter),
	_source{0,0,0,0,0,0},
	_clients()
    {
    }
    virtual ~base_host() {}
    /** 
     * Send a command to the specified board 
     *
     * @param mac5 Last byte of MAC address 
     * @param cmd  Command to send 
     *
     * @return true on success 
     */
    virtual bool send_command(const char mac5,
			      command_base& cmd) = 0;
    /** 
     * Scan for clients on the network interface 
     */
    virtual bool scan(unsigned short max=254) = 0;
  };
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:



    
