/** 
 * @file      feb/command.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Command base class 
 */
#ifndef FEB_COMMAND_HH
#define FEB_COMMAND_HH
#include <feb/packet.hh>

namespace feb
{
  //==================================================================
  /** A command for the front-end */
  struct command_base : public packet 
  {
    /** Get timeout in microseconds */
    virtual unsigned short timeout() const = 0;
     /** Get size of payload in bytes */
    virtual unsigned short size() const = 0;
    /** Set the length of the reply */
    virtual bool length(int) { return true; }
    /** Verify return */
    virtual bool verify() const {
      return id == 0x00;
    }
  protected:
    /** Constructor with parameter value */
    command_base(unsigned short value=0)
      : packet()
    {
      packet::value = value;
    }
  };
  //------------------------------------------------------------------
  /** 
   * A command with a specific payload size 
   */
  template <unsigned char  FAMILY,
	    unsigned char  ID,
	    unsigned short PAYLOAD_SIZE,
	    unsigned short TIMEOUT=50000>
  struct command : command_base
  {
    /** Get the payload size (in bytes) */
    unsigned short size() const override { return PAYLOAD_SIZE; }
    /** Get timeout in microseconds */
    unsigned short timeout() const override { return TIMEOUT; }
  protected:
    /** Constructor */
    command(unsigned short value=0) 
      : command_base(value)
    {
      packet::family = FAMILY;
      packet::id     = ID;
    }
  };
}
#endif

// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
