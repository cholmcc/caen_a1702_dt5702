/** 
 * @file      feb/probe.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Probe point configuration
 */
#ifndef FEB_MEMORY_HH
#define FEB_MEMORY_HH
#include <algorithm>
#include <bitset>
#include <iostream>

namespace feb
{
  //==================================================================
  /** 
   * A view of a bit set that allows access as if values and as arrays 
   *
   * @param OFF    Starting off set (bit number)
   * @param TOTAL  Total number of bits in bit set 
   * @param WIDTH  Number of bits in words to extract 
   * @param T      Type to extract as (must larger than WIDTH)
   * @param STRIDE Stride of array access in bits
   *
   */ 
  template <size_t OFF,
	    size_t TOTAL,
	    size_t WIDTH=1,
	    typename T=unsigned short,
	    size_t STRIDE=WIDTH>
  struct bit_view {
    /** the value type */
    using value_t=T;
    /** The bitset type */
    using bitset=std::bitset<TOTAL>;
    /** The size of the type to decode as */
    constexpr static size_t  WORD_SIZE  = sizeof(value_t)*8;
    /** Full mask of the type to decode as */
    constexpr static value_t FULL       = ~(value_t(0));
    /** Mask of words to extract */
    constexpr static value_t MASK       = FULL >> (WORD_SIZE-WIDTH);
    /** Reference to the bits */
    bitset& _bits;

    /** Trampoline to get/set data */
    struct manip {
      /** Reference to parent */
      bit_view& _v;
      /** Index to extract at */
      size_t     _i;
      /** Constructor */
      manip(bit_view& v, size_t i) : _v(v), _i(i) {}
      /** Move contructor */
      manip(manip&& m) : _v(m._v), _i(m._i) {}
      /** Assign from a value */
      value_t operator=(value_t v) { return _v.set(_i,v); }
      /** Retrieve value */
      operator value_t() const { return _v.get(_i); }
    };
    /** Trampoline to get/set data */
    struct const_manip {
      /** Reference to parent */
      const bit_view& _v;
      /** Index to extract at */
      size_t           _i;
      /** Constructor */
      const_manip(const bit_view& v, size_t i) : _v(v), _i(i) {}
      /** Extract value */
      operator value_t() const { return _v.get(_i); }
    };

    /** Constructor from a bit set */
    bit_view(bitset& s) : _bits(s) {}
    /** Set a value in the bits */
    value_t set(size_t i, const value_t& v) {
      // Take byte ordering into account
      for (size_t j = 0; j < WIDTH; j++)
	_bits.set(OFF+i*STRIDE+(WIDTH-1-j), (v >> j) & 0x1);
      return v & MASK;
    }
    /** Get the value of bits */
    value_t get(size_t i) const {
      value_t v = 0;
      for (size_t j = 0; j < WIDTH; j++)
	v |= (_bits[OFF+i*STRIDE+(WIDTH-1-j)] << j);
      return v & MASK;
    }
    /** Access operator */
    manip operator[](size_t i) { return manip(*this,i); }
    /** Access operator - constant version (short circuit to get?) */
    const_manip operator[](size_t i) const { return const_manip(*this,i); }
  };

  //==================================================================
  /** A memory with a certain size in bits */
  template <size_t N, bool REVERSE=false>
  struct memory
  {
    /** The number of bits */
    constexpr static size_t nbits = N;
    /** The number of bytes */
    constexpr static size_t nbytes = N / 8;
    /** The data type */
    using data_type=std::bitset<nbits>;
    /** Reference to the data type */
    using data_type_ref=data_type&;
    /** the data */
    data_type _data;

    /** A template to make it easier to make views */
    template <size_t   OFF,
	      size_t   WIDTH,
	      typename T=unsigned short,
	      size_t   STRIDE=WIDTH>
    using view=bit_view<OFF,N,WIDTH,T,STRIDE>;
    
    /** Constructor */
    memory() {}

    /** Helper function to reverse order of bits */
    static void reverse(data_type& bits) {
      for (size_t i = 0; i < N/2; i++) {
	// std::cout << i << " <-> " << N-i << std::endl;
	bool tmp    = bits[i];
	bits[i]     = bits[N-i-1];
	bits[N-i-1] = tmp;
      }
    }
    /** Helper function to convert from bits to bytes */
    static void to_bytes(const data_type& bits, unsigned char* o) {
      for (size_t i = 0; i < nbytes; i++) {
	size_t k = nbytes - i - 1;
	o[k] = 0;
	for (size_t j = 0; j < 8; j++) {
	  size_t l = 8 - j - 1;
	  o[k] |= (bits[i*8+j] << l);
	}
      }
    }
    /** Helper function to convert bytes to bits */
    static void from_bytes(const unsigned char* s, data_type& bits) {
      for (size_t i = 0; i < nbytes; i++) {
	size_t k = nbytes - i - 1;
	for (size_t j = 0; j < 8; j++) {
	  size_t l = 8 - j - 1;
	  bits[i * 8 + j] = (s[k] >> l) & 0x1;
	}
      }
    }
    /** Encode memory into bytes */
    virtual void to_bytes(unsigned char* o) const {
      if (REVERSE) {
	data_type cpy = _data;
	reverse(cpy);
	to_bytes(cpy,o);
	return;
      }
      to_bytes(_data,o);
    }
    /** Decode memory from bytes */
    void from_bytes(const unsigned char* s) {
      from_bytes(s,_data);
      if (REVERSE)
	reverse(_data);
    }
    /** Save content to output stream */
    virtual void save(std::ostream& o) const = 0;
    /** Load content from input stream */
    virtual void load(std::istream& i) = 0;
  };
  //==================================================================
  template <size_t N,bool R>
  std::ostream& operator<<(std::ostream& o, const memory<N,R>& m) {
    m.save(o);
    return o;
  }
  //==================================================================
  template <size_t N,bool R>
  std::istream& operator>>(std::istream& i, memory<N,R>& m) {
    m.load(i);
    return i;
  }
  //==================================================================
  /** 
   * Swallow the rest of an input line 
   *
   * @param i Input stream 
   *
   * @return i 
   */
  inline std::istream& swallow(std::istream& i) {
    std::string s;
    std::getline(i,s);
    return i;
  }

  //------------------------------------------------------------------
  /** 
   * Read in one value from input stream 
   *
   * This is here to simplify reading in single bits 
   *
   * @param i Input stream 
   *
   * @return Read unsigned value (bit, byte, ...)
   */
  inline unsigned get_one(std::istream& i) {
    unsigned r;
    i >> r;
    return r;
  }
  //------------------------------------------------------------------
  /** 
   * Read in one value from input stream and eat the rest of the line
   *
   * This is here to simplify reading in single bits 
   *
   * @param i Input stream 
   *
   * @return Read unsigned value (bit, byte, ...)
   */
  inline unsigned get_one_eat(std::istream& i) {
    unsigned r = get_one(i);
    swallow(i);
    return r;
  }
  
}
#endif
//
// EOF
//

