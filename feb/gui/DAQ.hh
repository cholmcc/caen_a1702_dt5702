/** 
 * @file      feb/gui/DAQ.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     DAQ widget 
 */
#ifndef FEB_GUI_DAQ_HH
#define FEB_GUI_DAQ_HH
#include <feb/root/DAQ.hh>
#include <feb/root/Monitor.hh>
#include <feb/gui/Board.hh>
#include <TGButton.h>
#include <TGFrame.h>
#include <TSystem.h>
#include <TRootEmbeddedCanvas.h>
#include <TStyle.h>
#include <map>

namespace feb
{
  namespace gui
  {
    struct DAQ : public TGVerticalFrame, public feb::root::DAQ
    {
      using Base=feb::root::DAQ;
      using MonitorMap=std::map<unsigned char,feb::root::Monitor>;

      TGGroupFrame        fTop;
      Board<DAQ>          fSelect;
      TGNumberEntry       fChannel;
      TGNumberEntry       fVCXO;

      TGGroupFrame        fMiddle;
      TGCheckButton       fMonitor;
      TGNumberEntry       fMaxADC;
      TGTextButton        fClear;

      TGGroupFrame        fLimits;
      TGNumberEntry       fNev;
      TGNumberEntry       fSec;
      TGTextButton        fStart;
      TGNumberEntryField  fRate;
      TGNumberEntryField  fRunNumber;

      TRootEmbeddedCanvas fEmCanvas;
      TCanvas&            fCanvas; 
      MonitorMap          fMonitors;
      
      Pixel_t       fRed;
      Pixel_t       fGreen;
      
      DAQ(feb::base_host& host,
	  const std::string& datadir,
	  TGCompositeFrame& parent)
	: TGVerticalFrame(&parent),
	  Base(host,0,datadir),

	  fTop(this,"Select board/channel to monitor", kHorizontalFrame),
	  fSelect(*this, host, fTop),
	  fChannel(&fTop, -1, 2, 0,
		   TGNumberFormat::kNESInteger,
		   TGNumberFormat::kNEAAnyNumber,
		   TGNumberFormat::kNELLimitMinMax, -1, 0x1F),
	  fVCXO(&fTop, 0, 2, 0,
		TGNumberFormat::kNESInteger,
		TGNumberFormat::kNEANonNegative,
		TGNumberFormat::kNELLimitMinMax, 0, 1000),

	  fMiddle(this,"Monitoring", kHorizontalFrame),
	  fMonitor(&fMiddle, "Monitor"),
	  fMaxADC(&fMiddle, 0xFFF, 6, 0,
		  TGNumberFormat::kNESInteger,
		  TGNumberFormat::kNEANonNegative,
		  TGNumberFormat::kNELLimitMinMax, 0, 0xFFF),
	  fClear(&fMiddle, "Clear histograms"),
	  
	  fLimits(this, "Run control", kHorizontalFrame),
	  fNev(&fLimits, 0, 10, 0, 
	       TGNumberFormat::kNESInteger,
	       TGNumberFormat::kNEANonNegative,
	       TGNumberFormat::kNELLimitMin, 0),
	  fSec(&fLimits, 0, 10, 0, 
	       TGNumberFormat::kNESInteger,
	       TGNumberFormat::kNEANonNegative,
	       TGNumberFormat::kNELLimitMin, 0),
	  fStart(&fLimits, "Start DAQ"),
	  fRate(&fLimits, 0, 10,
		TGNumberFormat::kNESReal,
		TGNumberFormat::kNEANonNegative,
		TGNumberFormat::kNELNoLimits),
	  fRunNumber(&fLimits, -1, 10,
		     TGNumberFormat::kNESInteger,
		     TGNumberFormat::kNEAAnyNumber,
		     TGNumberFormat::kNELLimitMin, 0),
	  fEmCanvas("canvas", this, 600, 600),
	  fCanvas(*fEmCanvas.GetCanvas())
      {
	gClient->GetColorByName("pink",fRed);
	gClient->GetColorByName("lightgreen",fGreen);

	TGLayoutHints* lhx  = new TGLayoutHints(kLHintsExpandX,3,3,3,3);
	TGLayoutHints* lhxy = new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,
						3,3,3,3);

	fSelect.GetTextEntry()->SetEnabled(false);
	fChannel.GetNumberEntry()->SetEnabled(false);
	fChannel.GetNumberEntry()->SetToolTipText("Channel to monitor "
						  "(<0 all channels)");
	fChannel.Connect("ValueSet(Long_t)","feb::gui::DAQ",this,
			 "HandleChannel()");
	fVCXO.GetNumberEntry()->SetToolTipText("Adjust timing frequency "
					       "(0 disables)");
	// fSelect.SetEnabled(false);
				      
	AddFrame(&fTop, lhx);

	fTop.AddFrame(new TGLabel(&fTop, "Select board"),     lhx);
	fTop.AddFrame(&fSelect,                               lhxy);
	fTop.AddFrame(new TGLabel(&fTop, "Select channel"),   lhx);
	fTop.AddFrame(&fChannel,                              lhx);
	fTop.AddFrame(new TGLabel(&fTop, "VCXO adjust freq."),lhx);
	fTop.AddFrame(&fVCXO,                                 lhx);

	fMonitor.Connect("Clicked()", "feb::gui::DAQ", this, "HandleMonitor()");
	fMonitor.SetToolTipText("Enable monitoring");
	fClear.Connect("Clicked()", "feb::gui::DAQ", this, "HandleClear()");
	fClear.SetToolTipText("Clear histograms");
	fMaxADC.GetNumberEntry()->SetToolTipText("Maximum ADC value to show");
	fMaxADC.GetNumberEntry()->SetEnabled(false);
	
	AddFrame(&fMiddle, lhx);
	fMiddle.AddFrame(&fMonitor,                           lhx);
	fMiddle.AddFrame(new TGLabel(&fMiddle,"Max ADC"),     lhx);
	fMiddle.AddFrame(&fMaxADC,                            lhx);
	fMiddle.AddFrame(&fClear,                             lhx);

	fStart.AllowStayDown(true);
	fStart.SetToolTipText("Start/stop run");
	fStart.Connect("Clicked()", "feb::gui::DAQ", this, "HandleStartStop()");
	fNev.GetNumberEntry()->SetToolTipText("Maximum number of events");
	fSec.GetNumberEntry()->SetToolTipText("Maximum duration (in seconds)");
	fRate.SetToolTipText("Current event rate");
	fRunNumber.SetToolTipText("Last run started");
	
	fStart.SetBackgroundColor(fRed);
	fRate.SetEnabled(false);
	fRunNumber.SetEnabled(false);
	
	AddFrame(&fLimits, lhx);
	fLimits.AddFrame(new TGLabel(&fLimits, "Max events"),     lhx);
	fLimits.AddFrame(&fNev,                                   lhx);
	fLimits.AddFrame(new TGLabel(&fLimits, "Max seconds"),    lhx);
	fLimits.AddFrame(&fSec,                                   lhx);
	fLimits.AddFrame(&fStart,                                 lhx);
	fLimits.AddFrame(new TGLabel(&fLimits, "Event rate (Hz)"),lhx);
	fLimits.AddFrame(&fRate,                                  lhx);
	fLimits.AddFrame(new TGLabel(&fLimits, "Run number"),     lhx);
	fLimits.AddFrame(&fRunNumber,                             lhx);
	
	fCanvas.SetTopMargin(0.02);
	fCanvas.SetRightMargin(0.02);
	
	AddFrame(&fEmCanvas, lhxy);
      }
      /** Update available clients */
      void Update() {
	fSelect.Update();
	fMonitors.clear();

	for (auto& kv : _host._clients)
	  fMonitors.emplace(kv.first, std::make_pair(kv.first,
						     fMaxADC.GetIntNumber()));
      }
      /** Draw the monitored histogram(s) */
      void DrawMonitor() {
	if (_running) return; // Just wait for update
	if (!fMonitor.IsOn()) return; // No monitoring

	auto& cl = fSelect.GetFEB();
	auto m = fMonitors.find(cl._mac5);
	if (m != fMonitors.end()) 
	  m->second.Draw(fCanvas, fChannel.GetIntNumber());
      }
      /** Handle change of channel */
      void HandleChannel() { DrawMonitor(); }
      /** Handle changle of board */
      void HandleBoard(feb::client&) { DrawMonitor(); }
      /** 
       * Handle enable/disable monitor 
       */
      void HandleMonitor() {
	// fSelect.SetEnabled(fMonitor.IsDown());
	fSelect.GetTextEntry()->SetEnabled(fMonitor.IsDown());
	fChannel.GetNumberEntry()->SetEnabled(fMonitor.IsDown());
	fMaxADC.GetNumberEntry()->SetEnabled(fMonitor.IsDown());
      }
      /** Handle clear histograms */
      void HandleClear() {
	for (auto& m : fMonitors) {
	  for (size_t i = 0; i < 32; i++) m.second.fAdc[i]->Reset();
	}
	fCanvas.Modified();
	fCanvas.Update();
      }
      /** Handle start/stop button */
      void HandleStartStop()
      {
	bool on = !fStart.IsDown(); // We get target state

	Pixel_t col = fGreen;
	if (on) {
	  Base::stop();
	  col = fRed;
	}
	fStart.SetText(!on ? "Stop DAQ" : "Start DAQ");
	fStart.SetBackgroundColor(col);
	fMaxADC.GetNumberEntry()->SetEnabled(on and
					     fMonitor.IsDown());
	
	if (on) return;

	loop(fNev.GetIntNumber(), fSec.GetIntNumber());
      }
      /** 
       * Starts the DAQ 
       */
      bool start() override
      {
	bool ret = feb::root::DAQ::start();
	fRunNumber.SetIntNumber(fRunNo);
	return ret;
      }
      /** Take events from client, possibly adjust VCX0  */
      bool take(feb::client& cl, unsigned short) override {
	return Base::take(cl, fVCXO.GetIntNumber());
      }
      /** Process an event a client */
      bool process(feb::client& cl) override {
	if (!Base::process(cl)) return false;

	if (!fMonitor.IsDown()) return true;

	unsigned char mac5 = cl._mac5;
	auto m = fMonitors.find(mac5);
	if (m == fMonitors.end()) return true;
	m->second.Fill(fEvent);

	// Not monitoring this board 
	auto& mcl = fSelect.GetFEB();
	if (mcl._mac5 != cl._mac5) return true;

	// std::clog << "D: Monitoring is on for "
	// 	     << feb::hex_out<2>("0x",cl._mac5) << std::endl;
		
	m->second.Draw(fCanvas, fChannel.GetIntNumber());

	return true;
      }
      /** 
       * After reading events from client - Updates event rate
      */
      bool post() override {
	// Handle events from ROOT
	// std::cout << "Post" << std::endl;
	if (!_running) {
	  fStart.SetDown(false);
	  fStart.SetText("Start DAQ");
	  fStart.SetBackgroundColor(fRed);
	}

	if (fMonitor.IsDown()) {
	  if (fSelect.HasFEB()) {
	    auto& cl = fSelect.GetFEB();
	    fRate.SetNumber(cl.trigger_rate());
	    // std::clog << "D: Event rate on "
	    // 	      << feb::hex_out<2>("0x",cl._mac5)
	    // 	      << " is " << fRate.GetNumber() << " Hz"
	    // 	      << std::endl;
	  }
	}
	
	gSystem->ProcessEvents();
	return true;
      }
      /** Stop the run */
      bool stop() override
      {
	bool ret = Base::stop();
	fStart.SetDown(false);
	fStart.SetText("Start DAQ");
	fStart.SetBackgroundColor(fRed);
	fMaxADC.GetNumberEntry()->SetEnabled(fMonitor.IsDown());
	
	return ret;
      }
    };
  }
}
#endif
// EOF
