/** 
 * @file      feb/gui/Thresholds.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to control threshold settings
 */
#ifndef FEB_GUI_THRESHOLDS_HH
#define FEB_GUI_THRESHOLDS_HH
#include <feb/slow_control.hh>
#include <TGNumberEntry.h>
#include <feb/gui/Utils.hh>

namespace feb
{
  namespace gui
  {
    struct Thresholds : public TGGroupFrame
    {
      TGGroupFrame fGroup1;
      GridFrame<>  fPerChannel;
      Headers      fChHeaders;
      EnablePulser fCharge;
      EnablePulser fTime;

      TGGroupFrame                    fGroup2;
      GridFrame<>                     fGlobal;
      HeadersXtra                     fGlHeaders;
      EnablePulserXtra<TGNumberEntry> fThreshold1;
      EnablePulserXtra<TGNumberEntry> fThreshold2;
      
      Thresholds(TGCompositeFrame& parent)
	: TGGroupFrame(&parent, "Thresholds", kVerticalFrame),
	  fGroup1       (this, "Per channel"),
	  fPerChannel   (fGroup1,3,2),
	  fChHeaders    (fPerChannel, "Enable","Pulser",fPerChannel.fXHint),
	  fCharge       (fPerChannel, "Charge",     fPerChannel.fXHint),
	  fTime         (fPerChannel, "Time",       fPerChannel.fXHint),
	  fGroup2       (this, "Global"),
	  fGlobal       (fGroup2,3,3),
	  fGlHeaders    (fGlobal, "Enable", "Pulser", "", fGlobal.fXHint),
	  fThreshold1   (fGlobal,"Threshold 1",           fGlobal.fXYHint,
			 0, 4, 0,
			 TGNumberFormat::kNESInteger,
			 TGNumberFormat::kNEANonNegative,
			 TGNumberFormat::kNELLimitMinMax, 0,0x3FF),
	  fThreshold2   (fGlobal,"Threshold 2",           fGlobal.fXYHint,
			 0, 4, 0,
			 TGNumberFormat::kNESInteger,
			 TGNumberFormat::kNEANonNegative,
			 TGNumberFormat::kNELLimitMinMax, 0,0x3FF)
      {
	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandY,2,2,2,2);
	parent.AddFrame(this,lh);
	AddFrame(&fGroup1);
	AddFrame(&fGroup2);

	fThreshold1.fEnable.Connect("Clicked()","feb::gui::Thresholds",
				    this, "Handle1()");
	fThreshold2.fEnable.Connect("Clicked()","feb::gui::Thresholds",
				    this, "Handle2()");
      }
      void Handle1() {
	bool on = fThreshold1.fEnable.IsDown();
	fThreshold1.fXtra.GetNumberEntry()->SetEnabled(on);
      }
      void Handle2() {
	bool on = fThreshold2.fEnable.IsDown();
	fThreshold2.fXtra.GetNumberEntry()->SetEnabled(on);
      }
	
      void Take(const feb::slow_control::thresholds& th) {
	fCharge    .Take(th.enable_charge[0],th.pp_charge       [0]);
	fTime      .Take(th.enable_time  [0],th.pp_time         [0]);
	fThreshold1.Take(th.enable_1     [0],th.disable_1_pulser[0]);
	fThreshold2.Take(th.enable_2     [0],th.disable_2_pulser[0]);
	fThreshold1.fXtra.SetIntNumber(th.value_1[0]);
	fThreshold2.fXtra.SetIntNumber(th.value_2[0]);
      }
	
      void Give(feb::slow_control::thresholds& th) {
	fCharge    .Give(th.enable_charge[0],th.pp_charge       [0]);
	fTime      .Give(th.enable_time  [0],th.pp_time         [0]);
	fThreshold1.Give(th.enable_1     [0],th.disable_1_pulser[0]);
	fThreshold2.Give(th.enable_2     [0],th.disable_2_pulser[0]);
	th.value_1[0] = fThreshold1.fXtra.GetIntNumber();
	th.value_2[0] = fThreshold2.fXtra.GetIntNumber();
      }
    };
  }
}
#endif
//
// EOF
//

	
	
