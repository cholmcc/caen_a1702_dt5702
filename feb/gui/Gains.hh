/** 
 * @file      feb/gui/Gains.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to control gains 
 */
#ifndef FEB_GUI_GAINS_HH
#define FEB_GUI_GAINS_HH
#include <feb/slow_control.hh>
#include <feb/gui/Board.hh>
#include <feb/gui/Utils.hh>
#include <TGNumberEntry.h>

namespace feb
{
  namespace gui
  {
    struct Gains : public TGGroupFrame
    {
      //--------------------------------------------------------------
      template <typename Cfg, typename PreampXtra=TGLabel>
      struct One : public TGGroupFrame {
	TGLayoutHints                   fSelHints;
	Labelled<TGRadioButton>         fSelect;
	GridFrame<TGHorizontalFrame>    fTop;
	HeadersXtra                     fHeaders;
	EnablePulserXtra<TGLabel>       fTrack;
	EnablePulserXtra<TGComboBox>    fPeak;
	EnablePulserXtra<TGNumberEntry> fShaper;
	EnablePulserXtra<PreampXtra>    fPreamp;
	EnablePulserXtra<TGLabel>       fOta;
	
	One(TGCompositeFrame& parent, const char* name)
	  : TGGroupFrame(&parent,name,kHorizontalFrame),
	    fSelHints(kLHintsCenterY,2,5,5,5),
	    fSelect(*this, "", fSelHints),
	    fTop(*this, 6, 3),
	    fHeaders(fTop,"Enable", "Pulser", "", fTop.fXHint),
	    fTrack  (fTop,"Track'n'hold",   fTop.fXHint),
	    fPeak   (fTop,"Peak finder",    fTop.fXYHint),
	    fShaper (fTop,"Shaper",         fTop.fXHint, 0, 1, 0,
		    TGNumberFormat::kNESInteger,
		    TGNumberFormat::kNEANonNegative,
		    TGNumberFormat::kNELLimitMinMax, 0, 0x3),
	    fPreamp(fTop, "Preamp",         fTop.fXYHint),
	    fOta   (fTop, "OTA",            fTop.fXHint)
	{
	  TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandX|
						kLHintsExpandY,2,2,2,2);
	  parent.AddFrame(this, lh); 
	  fPeak.fXtra.RemoveAll();
	  fPeak.fXtra.AddEntry("Peak", 0);
	  fPeak.fXtra.AddEntry("Track'n'hold",  1);
	  fPeak.fXtra.Select(0,false);
	  fPeak.fXtra.SetWidth(60);
	  fPeak.fXtra.SetHeight(20);
	  fPeak.fInverted = false;
	}
	void HandleChoice(bool on) {
	  // Enable/disable can clear flags - so we don't do that
	  // fTrack       .Enable(on);
	  // fPeak        .Enable(on);
	  // fShaper      .Enable(on);
	  fShaper.fXtra.GetNumberEntry()->SetEnabled(on);
	  if (fPeak.fXtra.GetTextEntry())
	    fPeak.fXtra.GetTextEntry()->SetEnabled(on);
	  // fPreamp      .Enable(on);
	  // fOta         .Enable(on);
	}
	virtual void Take(const Cfg& cfg) {
	  fTrack .Take(cfg.enable_track         [0],
		       cfg.disable_track_pulser [0]);
	  fPeak  .Take(cfg.enable_pdet          [0],
		       cfg.pp_pdet              [0]);
	  fShaper.Take(cfg.enable_shaper        [0],
		       cfg.disable_shaper_pulser[0]);
	  fPreamp.Take(cfg.enable_preamp        [0],
		       cfg.disable_preamp_pulser[0]);
	  fOta   .Take(cfg.enable_ota            [0],
		       cfg.disable_ota_pulser    [0]);
	  fPeak  .fXtra.Select( cfg.sca_or_peak     [0]);
	  fShaper.fXtra.SetIntNumber(cfg.shaper_time[0]);	  
	}
	virtual void Give(Cfg& cfg) {
	  fTrack .Give(cfg.enable_track         [0],
		       cfg.disable_track_pulser [0]);
	  fPeak  .Give(cfg.enable_pdet          [0],
		       cfg.pp_pdet              [0]);
	  fShaper.Give(cfg.enable_shaper        [0],
		       cfg.disable_shaper_pulser[0]);
	  fPreamp.Give(cfg.enable_preamp        [0],
		       cfg.disable_preamp_pulser[0]);
	  fOta   .Give(cfg.enable_ota           [0],
		       cfg.disable_ota_pulser   [0]);
	  cfg.sca_or_peak [0] = fPeak.fXtra.GetSelected();
	  cfg.shaper_time [0] = fShaper.fXtra.GetIntNumber( );
	}
      };
      //--------------------------------------------------------------
      using High=One<feb::slow_control::high_gain>;

      //--------------------------------------------------------------
      struct Low : public One<feb::slow_control::low_gain,TGComboBox>
      {
	Low(TGCompositeFrame& parent, const char* name)
	  : One(parent, name)
	{
	  fPreamp.fXtra.RemoveAll();
	  fPreamp.fXtra.AddEntry("Normal bias",0);
	  fPreamp.fXtra.AddEntry("Weak bias",  1);
	  fPreamp.fXtra.Select(0,false);
	  fPreamp.fXtra.SetWidth(80);
	  fPreamp.fXtra.SetHeight(20);
	}
	void Take(const feb::slow_control::low_gain& cfg) override
	{
	  One::Take(cfg);
	  fPreamp.fXtra   .Select (cfg.preamp_bias[0]);
	  fSelect.fContent.SetDown(cfg.preamp_send[0]);
	}
	void Give(feb::slow_control::low_gain& cfg) override
	{
	  One::Give(cfg);
	  cfg.preamp_bias[0] = fPreamp.fXtra   .GetSelected();
	  cfg.preamp_send[0] = fSelect.fContent.IsDown();
	}
      };
      
      
      //--------------------------------------------------------------
      High          fHigh;
      Low           fLow;
      
      Gains(TGCompositeFrame& parent) 
	: TGGroupFrame(&parent, "Gains", kHorizontalFrame),
	  fHigh(*this, "High"),
	  fLow (*this, "Low")
      {
	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandX,2,2,2,2);
	parent.AddFrame(this, lh);

	fHigh.fSelect.fContent.SetDown();
	fLow.fSelect.fContent.Connect("Clicked()","feb::gui::Gains",this,
				      "HandleLow()");
	fHigh.fSelect.fContent.Connect("Clicked()","feb::gui::Gains",this,
				       "HandleHigh()");
      }
      void HandleLow() {
	bool low = fLow.fSelect.fContent.IsDown();
	fHigh.fSelect.fContent.SetDown(!low,false);
	fLow.HandleChoice(low);
	fHigh.HandleChoice(!low);
      }
      void HandleHigh() {
	bool high = fHigh.fSelect.fContent.IsDown();
	fLow.fSelect.fContent.SetDown(!high,false);
	fLow.HandleChoice(!high);
	fHigh.HandleChoice(high);
      }
      void Take(const feb::slow_control::low_gain&  lg,
		const feb::slow_control::high_gain& hg) {
	fLow.fSelect.fContent.SetDown(lg.preamp_send[0]);
	// Must be called before Take - otherwise it may zero entries!
	HandleLow();
	fHigh.Take(hg);
	fLow .Take(lg);
      }
      void Give(feb::slow_control::low_gain&  lg,
		feb::slow_control::high_gain& hg) {
	fHigh.Give(hg);
	fLow .Give(lg);
      }
    };
  }
}
#endif

	
	
