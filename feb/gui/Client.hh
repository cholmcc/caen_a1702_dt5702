/** 
 * @file      feb/gui/Client.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Main-widget to control client
 */
#ifndef FEB_GUI_CLIENT_HH
#define FEB_GUI_CLIENT_HH
#include <feb/base_host.hh>
#include <feb/client.hh>
#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGButton.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TGSplitter.h>
#include <TGCanvas.h>
#include <feb/gui/Channel.hh>
#include <feb/gui/Board.hh>
#include <feb/gui/Thresholds.hh>
#include <feb/gui/Bias.hh>
#include <feb/gui/Gains.hh>
#include <feb/gui/Trigger.hh>
#include <feb/gui/Misc.hh>
#include <feb/gui/Channels.hh>

namespace feb
{
  namespace gui
  {

    //================================================================
    /** Top of interface */
    struct Top : public TGGroupFrame
    {
      Board<Client>     fSelect;
      TGTextButton      fPush;
      TGTextButton      fPull;
      TGTextButton      fLoad;
      TGTextButton      fSave;

      Top(Client& c, feb::base_host& h, TGCompositeFrame& parent)
	: TGGroupFrame(&parent, "Board", kHorizontalFrame),
	  fSelect     (c, h, *this),
	  fPush       (this,"Push"),
	  fPull       (this,"Pull"),
	  fLoad       (this,"Load"),
	  fSave       (this,"Save")
      {
	parent.AddFrame(this, new TGLayoutHints(kLHintsExpandX,2,2,2,2));

	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandX,3,3,3,3);
	AddFrame(&fSelect, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
	AddFrame(&fPush,   lh);
	AddFrame(&fPull,   lh);
	AddFrame(&fLoad,   lh);
	AddFrame(&fSave,   lh);

	fPush.SetToolTipText("Push settings to hardware");
	fPull.SetToolTipText("Pull settings from hardware");
	fLoad.SetToolTipText("Load settings from files");
	fSave.SetToolTipText("Save settings from files");
      }
    };
      
    //================================================================
    /** An element that represents a client */
    struct Client : public TGVerticalFrame
    {
      base_host&        fHost;
      std::string       fConfDir;

      Top               fTop;
      Bias              fBias;
      Channels          fChannels;
      TGHorizontalFrame fGroup1;
      Thresholds        fThresholds;
      Gains             fGains;
      TGHorizontalFrame fGroup2;
      Trigger           fTrigger;
      Misc              fMisc;
      //
      
      Client(base_host&         h,
	     const std::string& confdir,
	     TGCompositeFrame&  parent)
	: TGVerticalFrame(&parent),
	  fHost          (h),
	  fConfDir(confdir),
	  //
	  fTop           (*this,h,*this),
	  fBias          (fTop.fSelect, *this),
	  fChannels      (*this),
	  fGroup1        (this),
	  fThresholds    (fGroup1),
	  fGains         (fGroup1),
	  fGroup2        (this),
	  fTrigger       (fGroup2),
	  fMisc          (fGroup2)
	  // 
      {
	AddFrame(&fGroup1,new TGLayoutHints(kLHintsExpandX,2,2,2,2));
	AddFrame(&fGroup2,new TGLayoutHints(kLHintsExpandX,2,2,2,2));

	fTop.fSelect.Connect("Select(Int_t)","feb::gui::Client",this,
			     "HandleSelect(Int_t)");
	fTop.fLoad.Connect("Clicked()","feb::gui::Client",this,
			   "HandleLoad()");
	fTop.fSave.Connect("Clicked()","feb::gui::Client",this,
			   "HandleSave()");
	fTop.fPush.Connect("Clicked()","feb::gui::Client",this,
			   "HandlePush()");
	fTop.fPull.Connect("Clicked()","feb::gui::Client",this,
			   "HandlePull()");
      }
      /** Update available boards */
      void Update() {
	fTop.fSelect.Update();
	for (auto& c : fHost._clients)
	  c.second.load_settings(fConfDir);
	HandleBoard(fTop.fSelect.GetFEB());
      }
      void HandleSelect(Int_t) {
	HandleBoard(fTop.fSelect.GetFEB());
      }
      /** Handle selection of a board */
      void HandleBoard(feb::client& cl)
      {
	Take(cl._slow_control, cl._probe);
	fBias.fMac5 = cl._mac5;
	// fTop.fPull.SetEnabled(cl._mac5 != feb::broadcast);
      }
      /** Handle load from file request */
      void HandleLoad() {
	if (!fTop.fSelect.HasFEB()) {
	  std::clog << "E: Invalid board selected" << std::endl;
	  return;
	}
	
	auto& cl = fTop.fSelect.GetFEB();
	cl.load_settings(fConfDir);
	if (cl._mac5 == feb::broadcast) {
	  for (auto& c : fHost._clients)
	    c.second.load_settings(fConfDir);
	}
	
	HandleBoard(cl);
      }
      /** Handle save to file request */
      void HandleSave() {
	if (!fTop.fSelect.HasFEB()) {
	  std::clog << "E: Invalid board selected" << std::endl;
	  return;
	}

	auto& cl = fTop.fSelect.GetFEB();

	Give(cl._slow_control, cl._probe, cl._flexible_logic);
	cl.save_settings(fConfDir);
	if (cl._mac5 == feb::broadcast) {
	  for (auto& c : fHost._clients)
	    c.second.load_settings(fConfDir);
	}
      }
      /** Handle push to hardware request */
      void HandlePush() {
	if (!fTop.fSelect.HasFEB()) {
	  std::clog << "E: Invalid board selected" << std::endl;
	  return;
	}

	auto& cl = fTop.fSelect.GetFEB();
	
	Give(cl._slow_control, cl._probe, cl._flexible_logic);
	cl.configure();
      }
      /** Handle pull from hardware request */
      void HandlePull() {
	if (!fTop.fSelect.HasFEB()) {
	  std::clog << "E: Invalid board selected" << std::endl;
	  return;
	}
	
	auto& cl = fTop.fSelect.GetFEB();
	if (cl._mac5 == feb::broadcast) {
	  for (auto& c : fHost._clients)
	    c.second.sync();
	}
	
	cl.sync();
	HandleBoard(cl);
	// std::clog << "D: " << cl._slow_control << std::endl;
      }
      /** Take values from stored settings */
      void Take(const slow_control& sc, const probe& pb) {
	// std::clog << "D: Taking values from read" << std::endl;
	
	fBias      .Take(sc.bv);
	fThresholds.Take(sc.th);
	fGains     .Take(sc.lg,sc.hg);
	fTrigger   .Take(sc.tr);
      	fMisc      .Take(sc.mc);
	fChannels  .Take(sc.ch,pb);
      }
      /** Give values to stored settings */
      void Give(slow_control&   sc,
		probe&          pb,
		flexible_logic& fl) { 
	// std::clog << "D: Giving values to write" << std::endl;
	fBias      .Give(sc.bv);
	fThresholds.Give(sc.th);
	fGains     .Give(sc.lg,sc.hg);
	fTrigger   .Give(sc.tr);
	fMisc      .Give(sc.mc);
	fChannels  .Give(sc.ch,pb,fl);

      }
    };
  }
}
#endif

      
