/** 
 * @file      feb/gui/Channels.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to select control channel settings
 */
#ifndef FEB_GUI_CHANNELS_HH
#define FEB_GUI_CHANNELS_HH
#include <feb/gui/Channel.hh>
#include <feb/gui/Utils.hh>


namespace feb
{
  namespace gui
  {
    struct Channels : public TGGroupFrame, ProbeHandler
    {
      TGCanvas          fCanvas;
      GridFrame<>       fContainer;
      Channel           fAll;
      Channel*          fChannels[32];


      Channels(TGCompositeFrame& parent)
	: TGGroupFrame(&parent, "Channels"),
	  fCanvas        (this, 1000, 800),
	  fContainer     (*fCanvas.GetViewPort(),34,12),
	  fAll           (fContainer, *this, -1)
      {
	parent.AddFrame(this,new TGLayoutHints(kLHintsLeft|
					       kLHintsExpandX|
					       kLHintsExpandY,
					       3,3,3,3));

	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandX|
					      kLHintsExpandY,2,2,2,2);
	
	AddFrame(&fCanvas, lh);
	fCanvas.SetContainer(&fContainer);
	fContainer.fLayout.fSep = 0;
	const char* headers[] = {
	  "",
	  "Trigger",
	  "Pre-amp",
	  "Probe",
	  "Time\nthreshold",
	  "Charge\nthreshold",
	  "Bias\n",
	  "Bias value",
	  "High gain",
	  "Low gain",
	  "High gain\ntest",
	  "Low gain\ntest"
	};
	for (auto head : headers) {
	  TGLabel* l = new TGLabel(&fContainer, head);
	  l->SetTextFont(gEnv->GetValue("Gui.MenuHiFont","bold"));
	  fContainer.AddFrame(l, &fContainer.fXHint);
	}

	fAll.AddTo(fContainer);
	
	fAll.fTrigger        .fInner.Connect("Clicked()",
					     "feb::gui::Channels",this,
					     "HandleTriggers()");
	fAll.fAmp            .fInner.Connect("Clicked()",
					     "feb::gui::Channels",this,
					     "HandlePreamps()");
	// fAll.fProbe          .fInner.Connect("Selected(Int_t)",
	// 				     "feb::gui::Channels",this,
	// 				     "HandleProbes()");
	auto e = fContainer.FindFrameElement(&fAll.fProbe);
	if (e) {
	  e->fFrame = new TGLabel(&fContainer, "");
	  Pixel_t hi;
	  GetClient()->GetColorByName("lightyellow",hi);
	  e->fFrame->SetBackgroundColor(hi);
	}
	  
	// fContainer.HideFrame(&fAll.fProbe);
	fAll.fTimeThreshold  .fInner.Connect("ValueSet(Long_t)",
					     "feb::gui::Channels",this,
					     "HandleTimes()");
	fAll.fChargeThreshold.fInner.Connect("ValueSet(Long_t)",
					     "feb::gui::Channels",this,
					     "HandleCharges()");
	fAll.fBiasOn         .fInner.Connect("Clicked()",
					     "feb::gui::Channels",this,
					     "HandleBiasesOn()");
	fAll.fBias           .fInner.Connect("ValueSet(Long_t)",
					     "feb::gui::Channels",this,
					     "HandleBiases()");	
	fAll.fHighGain       .fInner.Connect("ValueSet(Long_t)",
					     "feb::gui::Channels",this,
					     "HandleHighs()");	
	fAll.fLowGain        .fInner.Connect("ValueSet(Long_t)",
					     "feb::gui::Channels",this,
					     "HandleLows()");	
	fAll.fTestHighGain   .fInner.Connect("Clicked()",
					     "feb::gui::Channels",this,
					     "HandleHighTests()");	
	fAll.fTestLowGain    .fInner.Connect("Clicked()",
					     "feb::gui::Channels",this,
					     "HandleLowTests()");	

	
	for (size_t i = 0; i < 32; i++) {
	  fChannels[i] = new Channel(fContainer, *this, i);
	  fChannels[i]->AddTo(fContainer);
	}

      }
      void HandleTriggers() {
	bool on = fAll.fTrigger.fInner.IsDown();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fTrigger.fInner.SetDown(on,false);
      }
      void HandlePreamps() {
	bool on = fAll.fAmp.fInner.IsDown();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fAmp.fInner.SetDown(on,false);
      }
      void HandleProbes() {
	int on = fAll.fProbe.fInner.GetSelected();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fProbe.fInner.Select(on,false);
      }
      void HandleTimes() {
	Int_t val = fAll.fTimeThreshold.fInner.GetIntNumber();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fTimeThreshold.fInner.SetIntNumber(val);
      }
      void HandleCharges() {
	Int_t val = fAll.fChargeThreshold.fInner.GetIntNumber();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fChargeThreshold.fInner.SetIntNumber(val);
      }
      void HandleBiasesOn() {
	bool on = fAll.fBiasOn.fInner.IsDown();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fBiasOn.fInner.SetDown(on,false);
      }
      void HandleBiases() {
	Int_t val = fAll.fBias.fInner.GetIntNumber();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fBias.fInner.SetIntNumber(val);
      }
      void HandleHighs() {
	Int_t val = fAll.fHighGain.fInner.GetIntNumber();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fHighGain.fInner.SetIntNumber(val);
      }
      void HandleLows() {
	Int_t val = fAll.fLowGain.fInner.GetIntNumber();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fLowGain.fInner.SetIntNumber(val);
      }
      void HandleHighTests() {
	bool on = fAll.fTestHighGain.fInner.IsDown();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fTestHighGain.fInner.SetDown(on,false);
      }
      void HandleLowTests() {
	bool on = fAll.fTestLowGain.fInner.IsDown();
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->fTestLowGain.fInner.SetDown(on,false);
      }
      /** 
       * Ensure only one probe is set 
       */
      void HandleProbe(Int_t sel, Int_t ch) override {
	for (Int_t i = 0; i < 32; i++) 
	  if (i != ch) 
	    fChannels[i]->fProbe.fInner.Select(0,false);
      }
      void Take(const feb::slow_control::channels& ch,
		const feb::probe& pb)
      {
	bool trg  = true;
	bool amp  = true;
	bool bias = true;
	for (size_t i = 0; i < 32; i++) {
	  fChannels[i]->Take(ch, pb);
	  if (!fChannels[i]->fTrigger.fInner.IsDown()) trg  = false;
	  if (!fChannels[i]->fAmp.fInner    .IsDown()) amp  = false;
	  if (!fChannels[i]->fBiasOn.fInner .IsDown()) bias = false;
	}
	fAll.fTrigger.fInner.SetDown(trg, false);
	fAll.fAmp.fInner    .SetDown(amp, false);
	fAll.fBiasOn.fInner .SetDown(bias,false);
      }
      void Give(feb::slow_control::channels& ch,
		feb::probe&                  pb,
		feb::flexible_logic&         fl)
      {
	for (size_t i = 0; i < 32; i++)
	  fChannels[i]->Give(ch, pb, fl); 
     }
    };
  }
}
#endif
//
// EOF
//
