/** 
 * @file      feb/gui/Channel.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Meta-widget to select control channel settings
 */
#ifndef FEB_GUI_CHANNEL_HH
#define FEB_GUI_CHANNEL_HH
#include <feb/slow_control.hh>
#include <feb/probe.hh>
#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGButton.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TEnv.h>
#include <feb/gui/Utils.hh>

namespace feb
{
  namespace gui
  {
    // Forward decl
    struct ProbeHandler {
      virtual void HandleProbe(Int_t sel, Int_t ch) = 0;
    };

    /** A single channel */
    struct Channel
    {
      template <typename Inner>
      struct Wrapped : public TGHorizontalFrame
      {
	Inner fInner;

	template <typename ... Args>
	Wrapped(TGCompositeFrame& parent, Args ... args)
	  : TGHorizontalFrame(&parent),
	    fInner(this, args...)
	{
	  AddFrame(&fInner, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY,
					      2,2,0,0));
	}
	void SetBackgroundColor(Pixel_t col) override {
	  TGHorizontalFrame::SetBackgroundColor(col);
	  fInner.SetBackgroundColor(col);
	}
      };
      
		   
      short                   fCh;
      ProbeHandler&           fParent;
      Wrapped<TGLabel>        fName;
      Wrapped<TGCheckButton>  fTrigger;
      Wrapped<TGCheckButton>  fAmp;
      Wrapped<TGComboBox>     fProbe;
      Wrapped<TGNumberEntry>  fTimeThreshold;
      Wrapped<TGNumberEntry>  fChargeThreshold;
      Wrapped<TGCheckButton>  fBiasOn;
      Wrapped<TGNumberEntry>  fBias;
      Wrapped<TGNumberEntry>  fHighGain;
      Wrapped<TGNumberEntry>  fLowGain;
      Wrapped<TGCheckButton>  fTestHighGain;
      Wrapped<TGCheckButton>  fTestLowGain;
      
      
      Channel(GridFrame<>& parent, ProbeHandler& chs, short ch)
	: fCh(ch),
	  fParent(chs),
	  fName           (parent,ch < 0 ? "All" : Form("ch %2d",ch)),
	  fTrigger        (parent,""),
	  fAmp            (parent,""),
	  fProbe          (parent,"None  "),
	  fTimeThreshold  (parent,0,2,ch,
			   TGNumberFormat::kNESInteger,
			   TGNumberFormat::kNEANonNegative,
			   TGNumberFormat::kNELLimitMinMax, 0, 0xF),
	  fChargeThreshold(parent,0,2,ch,
			   TGNumberFormat::kNESInteger,
			   TGNumberFormat::kNEANonNegative,
			   TGNumberFormat::kNELLimitMinMax, 0, 0xF),
	  fBiasOn         (parent,""),
	  fBias           (parent,0,3,ch,
			   TGNumberFormat::kNESInteger,
			   TGNumberFormat::kNEANonNegative,
			   TGNumberFormat::kNELLimitMinMax, 0, 0xFF),
	  fHighGain       (parent,51,2,ch,
			   TGNumberFormat::kNESInteger,
			   TGNumberFormat::kNEANonNegative,
			   TGNumberFormat::kNELLimitMinMax, 0, 0x3F),
	  fLowGain        (parent,47,2,ch,
			   TGNumberFormat::kNESInteger,
			   TGNumberFormat::kNEANonNegative,
			   TGNumberFormat::kNELLimitMinMax, 0, 0x3F),
	  fTestHighGain   (parent,""),
	  fTestLowGain    (parent,"")
      {
	fProbe.fInner.SetWidth(80);
	fProbe.fInner.SetHeight(20);
	fProbe.fInner.AddEntry("None",          0);
	fProbe.fInner.AddEntry("Fast shaper",   1);
	fProbe.fInner.AddEntry("Low gain",      2);
	fProbe.fInner.AddEntry("Low gain peak", 3);
	fProbe.fInner.AddEntry("High gain",     4);
	fProbe.fInner.AddEntry("High gain peak",5);
	fProbe.fInner.AddEntry("PHG",           6);
	fProbe.fInner.AddEntry("PLG",           7);
	fProbe.fInner.Connect("Selected(Int_t)","feb::gui::Channel",this,
			      "HandleProbe(Int_t)");

	fTrigger        .fInner.SetToolTipText("Enable in trigger mask");
	fAmp            .fInner.SetToolTipText("Enable pre-amplifier");
	fProbe          .fInner.GetTextEntry()
	  ->SetToolTipText("Set probe-point output");
	fTimeThreshold  .fInner.GetNumberEntry()
	  ->SetToolTipText("Set time threshold adjust");
	fChargeThreshold.fInner.GetNumberEntry()
	  ->SetToolTipText("Set charge threshold adjust");
	fBiasOn         .fInner.SetToolTipText("Enable bias voltage");
	fBias           .fInner.GetNumberEntry()
	  ->SetToolTipText("Bias voltage setting");
	fHighGain       .fInner.GetNumberEntry()
	  ->SetToolTipText("High gain setting");
	fLowGain        .fInner.GetNumberEntry()
	  ->SetToolTipText("Low gain setting");
	fTestHighGain   .fInner.SetToolTipText("High gain testing");
	fTestLowGain    .fInner.SetToolTipText("Low gain testing");
      }
      void AddTo(GridFrame<>& parent) {
	// TGLayoutHints* lh = &parent.fXHint;
	Pixel_t hi = parent.GetWhitePixel();
	if (fCh < 0) {
	  parent.GetClient()->GetColorByName("lightyellow",hi);
	  fName.fInner.SetTextFont(gEnv->GetValue("Gui.MenuHiFont","bold"));
	}
	else if (fCh % 2 == 1) {
	  if (!parent.GetClient()->GetColorByName("lightblue",hi))
	    parent.GetClient()->GetColorByName("lightgray",hi);
	}
	
	TGFrame* wrappers[] =
	  { &fName,
	    &fTrigger,
	    &fAmp,
	    &fProbe,            
	    &fTimeThreshold,    
	    &fChargeThreshold,  
	    &fBiasOn,           
	    &fBias,             
	    &fHighGain,         
	    &fLowGain,          
	    &fTestHighGain,     
	    &fTestLowGain };

	for (auto w : wrappers) {
	  w->SetBackgroundColor(hi);
	  parent.AddFrame(w,new TGLayoutHints(kLHintsFillX|kLHintsFillY));
	}
      }
      void HandleProbe(Int_t sel) {
	fParent.HandleProbe(sel, fCh);
      }
      
      void Take(const slow_control::channels& ch, const probe& pb) {
	fTrigger        .fInner.SetOn       (ch.mask[fCh]);
	fAmp            .fInner.SetOn       (not ch.disable_preamps[fCh]);
	fTimeThreshold  .fInner.SetIntNumber(ch.time_thresholds    [fCh]);
	fChargeThreshold.fInner.SetIntNumber(ch.charge_thresholds  [fCh]);
	fBiasOn         .fInner.SetOn       (ch.biases_enable      [fCh]);
	fBias           .fInner.SetIntNumber(ch.biases             [fCh]);
	fHighGain       .fInner.SetIntNumber(ch.high_gains         [fCh]);
	fLowGain        .fInner.SetIntNumber(ch.low_gains          [fCh]);
	fTestHighGain   .fInner.SetOn       (ch.high_tests         [fCh]);
	fTestLowGain    .fInner.SetOn       (ch.low_tests          [fCh]);

	fProbe.fInner.Select(0,false);
	if (pb.fast_shaper     [fCh]) fProbe.fInner.Select(1,false);
	if (pb.low_gain_shaper [fCh]) fProbe.fInner.Select(2,false);
	if (pb.low_gain_peak   [fCh]) fProbe.fInner.Select(3,false);
	if (pb.high_gain_shaper[fCh]) fProbe.fInner.Select(4,false);
	if (pb.high_gain_peak  [fCh]) fProbe.fInner.Select(5,false);
	if (pb.high_gains      [fCh]) fProbe.fInner.Select(6,false);
	if (pb.low_gains       [fCh]) fProbe.fInner.Select(7,false);
	
      }
      void Give(slow_control::channels& ch,
		probe&                  pb,
		flexible_logic&         fl) {
	ch.mask             [fCh] = fTrigger        .fInner.IsOn();
	fl.mask1            [fCh] = fTrigger        .fInner.IsOn();
	ch.disable_preamps  [fCh] = not fAmp        .fInner.IsOn();
	ch.time_thresholds  [fCh] = fTimeThreshold  .fInner.GetIntNumber();
	ch.charge_thresholds[fCh] = fChargeThreshold.fInner.GetIntNumber();
	ch.biases_enable    [fCh] = fBiasOn         .fInner.IsOn();
	ch.biases           [fCh] = fBias           .fInner.GetIntNumber();
	ch.high_gains       [fCh] = fHighGain       .fInner.GetIntNumber();
	ch.low_gains        [fCh] = fLowGain        .fInner.GetIntNumber();
	ch.high_tests       [fCh] = fTestHighGain   .fInner.IsOn();
	ch.low_tests        [fCh] = fTestLowGain    .fInner.IsOn();

	pb.fast_shaper      [fCh] = 0;
	pb.low_gain_shaper  [fCh] = 0;
	pb.low_gain_peak    [fCh] = 0;
	pb.high_gain_shaper [fCh] = 0;
	pb.high_gain_peak   [fCh] = 0;
	pb.high_gains       [fCh] = 0;
	pb.low_gains        [fCh] = 0;

	switch (fProbe.fInner.GetSelected()) {
	case 1: pb.fast_shaper     [fCh] = 1; break;
	case 2: pb.low_gain_shaper [fCh] = 1; break;
	case 3: pb.low_gain_peak   [fCh] = 1; break;
	case 4: pb.high_gain_shaper[fCh] = 1; break;
	case 5: pb.high_gain_peak  [fCh] = 1; break;
	case 6: pb.high_gains      [fCh] = 1; break;
	case 7: pb.low_gains       [fCh] = 1; break;
	}
      }
    };
  }
}
#endif

	
	
	
	  
