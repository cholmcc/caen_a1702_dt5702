/** 
 * @file      feb/gui/Utils.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Various utilities
 */
#ifndef FEB_GUI_UTILS_HH
#define FEB_GUI_UTILS_HH
#include <TGButton.h>
#include <TGFrame.h>
#include <TGTableLayout.h>

namespace feb
{
  namespace gui
  {
    //================================================================
    /** Grid layout */
    template <typename Frame=TGCompositeFrame>
    struct GridFrame : public Frame {
      TGTableLayout fLayout;
      TGLayoutHints  fXHint;
      TGLayoutHints  fXYHint;
      UInt_t         fCurRow;
      UInt_t         fCurCol;
      
      GridFrame(TGCompositeFrame& parent,
		Int_t nrow, Int_t ncol)
	: Frame(&parent),
	  fLayout(this, nrow, ncol, 2, 2),
	  fXHint(kLHintsNormal),
	  fXYHint(kLHintsNormal|kLHintsExpandY),
	  fCurRow(0),
	  fCurCol(0)
	  
      {
	this->SetLayoutManager(&fLayout);

	parent.AddFrame(this, new TGLayoutHints(kLHintsExpandX, 2,2,2,2));
      }
      void AddFrame(TGFrame* f, TGLayoutHints* h=0) {
	if (fCurCol >= fLayout.fNcols) {
	  fCurCol = 0;
	  fCurRow++;
	}
	ULong_t hints = kLHintsNormal;
	if (h) hints = h->GetLayoutHints();
	Frame::AddFrame(f, new TGTableLayoutHints(fCurCol,fCurCol+1,
						  fCurRow,fCurRow+1,
						  hints));
	fCurCol++;
      }
    };

    //================================================================
    /** Headers */
    struct Headers {
      TGLabel fFirst;
      TGLabel fSecond;

      Headers(TGCompositeFrame& parent,
	      const char*       first,
	      const char*       second,
	      TGLayoutHints&    hints)
	: fFirst(&parent, first),
	  fSecond(&parent, second)
      {
	parent.AddFrame(&fFirst,  &hints);
	parent.AddFrame(&fSecond, &hints);
      }
    };
    //----------------------------------------------------------------
    struct HeadersXtra : Headers {
      TGLabel fXtra;

      HeadersXtra(TGCompositeFrame& parent, 
		  const char*       first,
		  const char*       second,
		  const char*       third,
		  TGLayoutHints&    hints)
	: Headers(parent, first, second, hints),
	  fXtra(&parent, third)
      {
	parent.AddFrame(&fXtra, &hints);
      }
    };

    //================================================================
    template <typename Content>
    struct Labelled {
      TGLabel  fLabel;
      Content  fContent;

      template <typename ... Args>
      Labelled(TGCompositeFrame& parent,
	       const char*       name,
	       TGLayoutHints&    hints,
	       Args ...          args)
	: fLabel(&parent, name),
	  fContent(&parent,args...)
      {
	parent.AddFrame(&fLabel,   &hints);
	parent.AddFrame(&fContent, &hints);
      }
    };
    
    //================================================================
    /** Enable w/pulser optionx */
    struct EnablePulser {
      TGCheckButton fEnable;
      TGCheckButton fPulser;
      bool          fInverted;

      EnablePulser(TGCompositeFrame& parent,
		   const char*       name,
		   TGLayoutHints&    hints)
	: fEnable(&parent, name),
	  fPulser(&parent, ""),
	  fInverted(true)
      {
	// fEnable.Connect("Clicked()","feb::gui::EnablePulser",this,
	//                 "HandleEnable()");

	parent.AddFrame(&fEnable, &hints);
	parent.AddFrame(&fPulser, &hints);
	fEnable.SetToolTipText("Enable");
	fPulser.SetToolTipText("Enable pulser mode");
      }
      virtual void Enable(bool en)
      {
	fEnable.SetEnabled(en);
	fPulser.SetEnabled(en);
      }
      virtual void HandleEnable() {
	fPulser.SetEnabled(fEnable.IsDown());
      }
      virtual void Take(bool enable, bool not_pulser) {
	fEnable.SetDown(enable);
	fPulser.SetDown(fInverted ? !not_pulser : not_pulser);
      }
      template <typename T1, typename T2>
      void Give(T1 enable, T2 not_pulser) const {
	enable     = fEnable .IsDown();
	not_pulser = fInverted ? !fPulser.IsDown() : fPulser.IsDown();
      }
    };

    //----------------------------------------------------------------
    /** Enable w/pulser option and extra widget */
    template <typename Xtra>
    struct EnablePulserXtra  : EnablePulser
    {
      Xtra fXtra;

      template <typename ...Args>
      EnablePulserXtra(TGCompositeFrame& parent,
		       const char* name,
		       TGLayoutHints& hints,
		       Args...args)
	: EnablePulser(parent,name,hints),
	  fXtra(&parent,args...)
      {
	parent.AddFrame(&fXtra, &hints);
      }
    };
  }
}
#endif 
      
	
