/** 
 * @file      feb/gui/Main.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Main UI
 */
#ifndef FEB_GUI_MAIN_HH
#define FEB_GUI_MAIN_HH
#include <TGFrame.h>
#include <TGTab.h>
#include <TGMenu.h>
#include <TGFileDialog.h>
#include <feb/gui/Client.hh>
#include <feb/gui/DAQ.hh>
#include <feb/log.hh>

namespace feb
{
  namespace gui
  {
    struct Main : public TGMainFrame
    {
      TGMenuBar         fMenu;
      TGPopupMenu       fMain;
      TGPopupMenu       fVerbosity;
      TGPopupMenu       fDirectories;
      TGTab             fTabs;
      TGCompositeFrame* fClientTab;
      Client            fClient;
      TGCompositeFrame* fDAQTab;
      DAQ               fDAQ;

      Main(base_host& host,
	   const std::string& confdir,
	   const std::string& datadir)
	: TGMainFrame(gClient->GetRoot(), 10, 10,
		      kMainFrame|kVerticalFrame),
	  fMenu(this),
	  fMain(gClient->GetRoot()),
	  fVerbosity(gClient->GetRoot()),
	  fDirectories(gClient->GetRoot()),
	  fTabs(this),
	  fClientTab(fTabs.AddTab("Settings")),
	  fClient   (host, confdir, *fClientTab),
	  fDAQTab   (fTabs.AddTab("Data acquisition/Monitoring")),
	  fDAQ      (host, datadir, *fDAQTab)
      {
	// feb::gui::_setlog();
	
	TGLayoutHints* lhx  = new TGLayoutHints(kLHintsExpandX);
	TGLayoutHints* lhxy = new TGLayoutHints(kLHintsExpandX|kLHintsExpandY);
	AddFrame(&fMenu, lhx);
	AddFrame(&fTabs, lhxy);
	
	fClientTab->AddFrame(&fClient, lhxy);
	fDAQTab   ->AddFrame(&fDAQ,    lhxy);

	TGLayoutHints* mlh = new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
	fMenu.AddPopup("&File", &fMain, mlh);
	fMain.AddEntry("&Close", 0);
	fMain.Connect("Activated(Int_t)", "feb::gui::Main",this,
		      "HandleMain(Int_t)");
	
	fMenu.AddPopup("&Verbosity", &fVerbosity,mlh);
	fVerbosity.AddEntry("Errors",      0);
	fVerbosity.AddEntry("Warnings",    1);
	fVerbosity.AddEntry("Information", 2);
	fVerbosity.AddEntry("Debugging",   3);
	fVerbosity.Connect("Activated(Int_t)","feb::gui::Main",this,
			   "HandleVerbose(Int_t)");
	fVerbosity.CheckEntry(feb::logbuf::instance()->get_level());

	fMenu.AddPopup("&Directories", &fDirectories, mlh);
	fDirectories.AddEntry("Configuration ...", 0);
	fDirectories.AddEntry("Data output ...",   1);
	fDirectories.Connect("Activated(Int_t)","feb::gui::Main",this,
			     "HandleDirectory(Int_t)");
	
	fClient.Update();
	fDAQ.Update();
	
	MapSubwindows();
	Resize(GetDefaultSize());
	MapWindow();
      }
      void HandleMain(Int_t what) {
	std::clog << "I: Handling file menu: " << what << std::endl;
	if (what == 0) CloseWindow();
      }
      void HandleVerbose(Int_t lvl) {
	fVerbosity.UnCheckEntries();
	fVerbosity.CheckEntry(lvl);

	feb::logbuf::instance()->set_level(lvl);

	std::clog << "E: Error       enabled" << std::endl;
	std::clog << "W: Warning     enabled" << std::endl;
	std::clog << "I: Information enabled" << std::endl;
	std::clog << "D: Debug       enabled" << std::endl;
      }
      void HandleDirectory(Int_t which) {
	std::string& dir = which == 0 ? fClient.fConfDir : fDAQ.fDataDir;

	TGFileInfo info;
	info.SetIniDir(gSystem->DirName(dir.c_str()));
	info.SetFilename(gSystem->BaseName(dir.c_str()));
	new TGFileDialog(gClient->GetRoot(), this, kDOpen, &info);
	
	if (!info.fIniDir) return;

	dir = info.fFilename;
	std::clog << "I: Directories: \n"
		  << "   conf = " << fClient.fConfDir << "\n"
		  << "   data = " << fDAQ   .fDataDir << std::endl;
      }
    };
  }
}
#endif
