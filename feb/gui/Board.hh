/** 
 * @file      feb/gui/Board.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to select FEB
 */
#ifndef FEB_GUI_BOARD_HH
#define FEB_GUI_BOARD_HH
#include <feb/base_host.hh>
#include <feb/client.hh>
#include <TGComboBox.h>

namespace feb
{
  namespace gui
  {
    struct BaseBoard : public TGComboBox
    {
      feb::base_host& fHost;
      
      BaseBoard(feb::base_host& host, TGCompositeFrame& parent)
	: TGComboBox(&parent,"",0),
	  fHost(host)
      {
	// parent.AddFrame(this, new TGLayoutHints(kLHintsExpandX,2,2,1,1));
	Connect("Selected(Int_t)","feb::gui::BaseBoard",this,
		"HandleSelect(Int_t)");
	GetTextEntry()->SetToolTipText("Select front-end board");
      } 
      /** Check if we have this board */
      bool HasFEB(Int_t id=-1) const {
	if (id < 0) id = GetSelected();

	unsigned char sid = id;
	return !(fHost._clients.find(sid) == fHost._clients.end());
      }
      /** Get the board interface */
      feb::client& GetFEB(Int_t id=-1) const {
	if (id < 0) id = GetSelected();

	unsigned char sid = id;
	return fHost._clients.find(sid)->second;
      }
      /** On selection of a board */
      void HandleSelect(Int_t sel) {
	// std::clog << "D: Handle select of board" << std::endl;
	if (!HasFEB(sel)) return;

	auto& cl = GetFEB(sel);

	HandleBoard(cl);
      }
      /** Take the new board */
      virtual void HandleBoard(feb::client& cl) {}
      /** Update from the host */
      void Update()
      {
	RemoveAll();

	int sel = -1;
	for (auto& kv : fHost._clients) {
	  if (kv.first == 0xFF) 
	    AddEntry("Broadcast (0xFF)",0xFF);
	  else
	    AddEntry(Form("%3d (0x%02x)",
			  kv.first, kv.first), kv.first);
	  if (sel < 0) sel = kv.first;
	}
	if (sel < 0) return;

	Select(sel); // Will fire event
      }
    };
    //----------------------------------------------------------------
    template <typename Other>
    struct Board : public BaseBoard
    {
      Other& fOther;
      Board(Other& o, feb::base_host& host, TGCompositeFrame& parent)
	: BaseBoard(host, parent),
	  fOther(o)
      {}
      /** Delegates to other */
      void HandleBoard(feb::client& cl) { fOther.HandleBoard(cl); }
    };
  }
}
#endif
//
// EOF
//

	
						
