/** 
 * @file      feb/gui/Misc.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to control misc settings
 */
#ifndef FEB_GUI_FLAGS_HH
#define FEB_GUI_FLAGS_HH
#include <feb/slow_control.hh>
#include <feb/gui/Utils.hh>

namespace feb
{
  namespace gui
  {
    struct Misc : public TGGroupFrame
    {
      GridFrame<>             fGrid;
      Labelled<TGComboBox>    fScaBias;
      Labelled<TGCheckButton> fOtaqTest;
      Labelled<TGLabel>       fSpacer;
      Headers                 fHeaders;
      Labelled<TGCheckButton> fFastFollow;
      EnablePulser            fFastShaper;
      EnablePulser            fTemp;
      EnablePulser            fBandgap;
      EnablePulser            fProbeOta;
      EnablePulser            fRaz;

      Misc(TGCompositeFrame& parent)
	: TGGroupFrame(&parent, "Misc", kVerticalFrame),
	  fGrid       (*this, 10, 2),
          fScaBias    (fGrid, "SCA bias",       fGrid.fXYHint,"",0),
          fOtaqTest   (fGrid, "OTaq test",      fGrid.fXHint),
	  fSpacer     (fGrid, "",               fGrid.fXHint,""),
	  fHeaders    (fGrid, "Enable","Pulser",fGrid.fXHint),
          fFastFollow (fGrid, "Fast follow",    fGrid.fXHint),
	  fFastShaper (fGrid, "Fast shaper",    fGrid.fXHint),
          fTemp       (fGrid, "Temperature",    fGrid.fXHint),
          fBandgap    (fGrid, "Bandgap",        fGrid.fXHint),
	  fProbeOta   (fGrid, "OTA probe",      fGrid.fXHint),
          fRaz        (fGrid, "RAZ",            fGrid.fXHint)
      {
	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandY,2,2,2,2);
	parent.AddFrame(this,lh);
	
	fScaBias.fContent.RemoveAll();
	fScaBias.fContent.AddEntry("High (5MHz)",0);
	fScaBias.fContent.AddEntry("Weak",       1);
	fScaBias.fContent.SetWidth(100);
	fScaBias.fContent.SetHeight(20);
	Pixel_t red; gClient->GetColorByName("red",red);
	fScaBias.fContent.SetBackgroundColor(red);
      }
      void Take(const slow_control::misc& mc)
      {
        fScaBias   .fContent.Select(mc.sca_bias                    [0]);
        fOtaqTest  .fContent.SetOn (mc.otaq_test                   [0]);
        fFastFollow.fContent.SetOn (!mc.disable_fast_follow_pulser [0]);

	fFastShaper.Take(mc.enable_fast_shaper        [0],
			 mc.disable_fast_shaper_pulser[0]);
	fTemp      .Take(mc.enable_temp               [0],
			 mc.disable_temp_pulser       [0]);
	fBandgap   .Take(mc.enable_bandgap            [0],
			 mc.disable_bandgap_pulser    [0]);
	fProbeOta  .Take(mc.enable_probe_ota          [0],
			 mc.disable_probe_ota_pulser  [0]);
	fRaz       .Take(mc.enable_raz                [0],
			 mc.disable_raz_pulser        [0]);
      }
      void Give(slow_control::misc& mc)
      {
        mc.sca_bias		      [0] = fScaBias    .fContent.GetSelected();
        mc.disable_fast_follow_pulser [0] = !fFastFollow.fContent.IsOn();
        mc.otaq_test		      [0] = fOtaqTest   .fContent.IsOn();

	fFastShaper.Give(mc.enable_fast_shaper        [0],
			 mc.disable_fast_shaper_pulser[0]);
	fTemp      .Give(mc.enable_temp               [0],
			 mc.disable_temp_pulser       [0]);
	fBandgap   .Give(mc.enable_bandgap            [0],
			 mc.disable_bandgap_pulser    [0]);
	fProbeOta  .Give(mc.enable_probe_ota          [0],
			 mc.disable_probe_ota_pulser  [0]);
	fRaz       .Give(mc.enable_raz                [0],
			 mc.disable_raz_pulser        [0]);
      }
    };
  }
}
#endif
