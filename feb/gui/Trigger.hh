/** 
 * @file      feb/gui/Trigger.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to control trigger settings
 */
#ifndef FEB_GUI_TRIGGER_HH
#define FEB_GUI_TRIGGER_HH
#include <feb/gui/Utils.hh>

namespace feb
{
  namespace gui
  {
    struct Trigger : public TGGroupFrame
    {
      TGGroupFrame  fGroup1;
      GridFrame<>   fDiscs;
      Headers       fHeaders;
      EnablePulser  fChargeDisc;
      EnablePulser  fTimeDisc;
      EnablePulser  fValidate;

      TGGroupFrame   fGroup2;
      GridFrame<>    fOther;
      TGComboBox     fLatched;
      TGComboBox     fExternal;
      TGComboBox     fPolarity;
      
      TGCheckButton  fOrChannels;
      TGCheckButton  fBypassPeak;
      TGCheckButton  fMultiplex;

      TGCheckButton  fOrOut;
      TGCheckButton  fOrCollect;
      TGCheckButton  fOrTime;
      
      Trigger(TGCompositeFrame& parent)
	: TGGroupFrame(&parent, "Trigger", kVerticalFrame),
	  fGroup1(this, "Inputs"),
	  fDiscs(fGroup1,4,2),
	  fHeaders       (fDiscs, "Enable", "Pulser",    fDiscs.fXHint),
	  fChargeDisc    (fDiscs, "Charge Discriminator",fDiscs.fXHint),
	  fTimeDisc      (fDiscs, "Time Discriminator",  fDiscs.fXHint),
	  fValidate      (fDiscs, "Validate",            fDiscs.fXHint),
	  
	  fGroup2(this, "Handling"),
	  fOther(fGroup2, 3,3),
	  fLatched   (&fOther,"",0),
	  fExternal  (&fOther,"",1),
	  fPolarity  (&fOther,"",2),

	  fOrChannels(&fOther,"Enable channel triggers"),
	  fBypassPeak(&fOther,"Bypass peak"),
	  fMultiplex (&fOther,"Multiplex"),
	  
	  fOrOut     (&fOther,"OR-32 output"),
	  fOrCollect (&fOther,"OR-32 open collector"),
	  fOrTime    (&fOther,"OR-32 time open collector")
      {
	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,
					      2,2,2,2);
	parent.AddFrame(this,lh);
	AddFrame(&fGroup1);
	AddFrame(&fGroup2);
	
	fLatched.RemoveAll();
	fLatched.AddEntry("Trigger",0);
	fLatched.AddEntry("RS",     1);
	fLatched.Select(0);
	fLatched.SetWidth(120);
	fLatched.SetHeight(20);

	fExternal.RemoveAll();
	fExternal.AddEntry("Internal trigger", 0);
	fExternal.AddEntry("External trigger", 1);
	fExternal.Select(0);
	fExternal.SetWidth(120);
	fExternal.SetHeight(20);

	fPolarity.RemoveAll();
	fPolarity.AddEntry("Active low",  0);
	fPolarity.AddEntry("Active high", 1);
	fPolarity.Select(0);
	fPolarity.SetWidth(120);
	fPolarity.SetHeight(20);

	fOther.AddFrame(&fLatched,    &fOther.fXHint);
	fOther.AddFrame(&fExternal,   &fOther.fXHint);
	fOther.AddFrame(&fPolarity,   &fOther.fXHint);
	fOther.AddFrame(&fOrChannels, &fOther.fXHint);	
	fOther.AddFrame(&fBypassPeak, &fOther.fXHint);
	fOther.AddFrame(&fMultiplex,  &fOther.fXHint);
	fOther.AddFrame(&fOrOut,      &fOther.fXHint);
	fOther.AddFrame(&fOrCollect,  &fOther.fXHint);
	fOther.AddFrame(&fOrTime,     &fOther.fXHint);
      }

      void Take(const feb::slow_control::trigger& tr) {
	fChargeDisc    .Take(tr.enable_discriminator         [0],
			     tr.disable_discriminator_pulser [0]);
	fTimeDisc      .Take(tr.enable_discriminator2        [0],
			     tr.disable_discriminator2_pulser[0]);
	fValidate      .Take(tr.enable_validate              [0],
			     tr.disable_validate_pulser      [0]);

	fLatched       .Select(tr.latched_output               [0]);
	fExternal      .Select(tr.external                     [0]);
	fPolarity      .Select(tr.polarity                     [0]);
	fOrChannels    .SetDown(tr.enable_channel_out          [0]);
	fMultiplex     .SetDown(tr.enable_multiplex            [0]);
	fBypassPeak    .SetDown(tr.bypass_peak_sense           [0]);
	fOrOut         .SetDown(tr.enable_or_out               [0]);
	fOrCollect     .SetDown(tr.enable_or_collect_out       [0]);
	fOrTime        .SetDown(tr.enable_or_time_out          [0]);
      }
      void Give(feb::slow_control::trigger& tr) {
	fChargeDisc.Give(tr.enable_discriminator         [0],
		       tr.disable_discriminator_pulser [0]);
	fTimeDisc  .Give(tr.enable_discriminator2        [0],
		       tr.disable_discriminator2_pulser[0]);
	fValidate  .Give(tr.enable_validate              [0],
			 tr.disable_validate_pulser      [0]);
	
	tr.latched_output               [0] = fLatched       .GetSelected();
	tr.external                     [0] = fExternal      .GetSelected();
	tr.polarity                     [0] = fPolarity      .GetSelected();
	tr.enable_channel_out           [0] = fOrChannels    .IsDown();
	tr.enable_multiplex             [0] = fMultiplex     .IsDown();
	tr.bypass_peak_sense            [0] = fBypassPeak    .IsDown();
	tr.enable_or_out                [0] = fOrOut         .IsDown();
	tr.enable_or_collect_out        [0] = fOrCollect     .IsDown();
	tr.enable_or_time_out           [0] = fOrTime        .IsDown();
      }
    };
  }
}
#endif
//
// EOF
//
