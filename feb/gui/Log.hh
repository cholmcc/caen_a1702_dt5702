/** 
 * @file      feb/gui/log.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Logging system 
 */
#ifndef FEB_GUI_LOGGER_HH
#define FEB_GUI_LOGGER_HH
#include <feb/log.hh>
#include <TGMsgBox.h>
#include <TGClient.h>

namespace feb
{
  namespace gui
  {
    struct LogBuf : feb::color_logbuf
    {
      LogBuf() : feb::color_logbuf() {}
      
      void log_error(const std::string& s) {
	feb::logbuf::log_error(s);

	new TGMsgBox(gClient->GetRoot(),
		     gClient->GetRoot(),
		     "FEB ERROR",
		     s.c_str(),
		     kMBIconExclamation,
		     kMBDismiss);
	// delete msg;
      }
      void log_fatal(const std::string& s) {
	new TGMsgBox(gClient->GetRoot(),
		     gClient->GetRoot(),
		     "FEB FATAL",
		     s.c_str(),
		     kMBIconStop,
		     kMBDismiss);
	// delete msg;
	feb::logbuf::log_fatal(s);
      }
    };
    //------------------------------------------------------------------
    inline feb::logbuf* setlog() {
      feb::logbuf::_instance = new LogBuf();
      std::clog.rdbuf(feb::logbuf::_instance);
      std::clog.exceptions(std::ios_base::badbit);
      return feb::logbuf::_instance;
    }
  }
}
	
	
#endif
//
// EOF
//

