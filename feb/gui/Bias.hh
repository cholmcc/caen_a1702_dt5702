/** 
 * @file      feb/gui/Bias.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Widget to control biases
 */
#ifndef FEB_GUI_BIAS_HH
#define FEB_GUI_BIAS_HH
#include <feb/slow_control.hh>
#include <TGFrame.h>
#include <TGNumberEntry.h>
#include <TGButton.h>
#include <feb/gui/Board.hh>

namespace feb
{
  namespace gui
  {
    // Forward declaration
    struct Client;
    
    struct Bias : public TGGroupFrame
    {
      BaseBoard&      fBoard;
      TGCheckButton   fEnable;
      TGComboBox      fReference;
      TGTextButton    fOnOff;

      Pixel_t fRed;
      Pixel_t fGreen;

      unsigned char fMac5;

      Bias(BaseBoard& board, TGCompositeFrame& parent)
	: TGGroupFrame(&parent, "Bias voltage", kHorizontalFrame),
	  fBoard(board),
	  fEnable(this, ""),
	  fReference(this, "None    "),
	  fOnOff(this,"Turn on"),
	  fMac5(0xFF)
      {
	TGLayoutHints* lh = new TGLayoutHints(kLHintsExpandX,2,2,2,2);

	gClient->GetColorByName("pink",fRed);
	gClient->GetColorByName("lightgreen",fGreen);
	
	parent.AddFrame(this, lh);

	fReference.RemoveAll();
	fReference.AddEntry("Internal (2.5V)", 0);
	fReference.AddEntry("External (4.5V)", 1);
	fReference.Select(1);
	// fReference.GetTextEntry()->SetEnabled(false);
	
	fEnable.Connect("Clicked()","feb::gui::Bias",this,"HandleEnable()");
	fOnOff.Connect("Clicked()","feb::gui::Bias",this,"HandleOnOff()");
	fOnOff.SetBackgroundColor(fRed);
	fOnOff.AllowStayDown(true);
	
	AddFrame(&fEnable,    lh);
	AddFrame(&fReference, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,
						2,2,2,2));
	AddFrame(&fOnOff,     lh);

	fEnable.SetToolTipText("Enable bias voltage");
	fReference.GetTextEntry()->SetToolTipText("Bias voltage reference");
	fOnOff.SetToolTipText("Turn on/off bias voltage");
      }
      void HandleEnable() {
	fOnOff.SetEnabled(fEnable.IsDown());
	fReference.GetTextEntry()->SetEnabled(fEnable.IsDown());
      }
      void HandleOnOff() {
	bool on = !fOnOff.IsDown(); // We get target state 

	auto& cl = fBoard.GetFEB();

	if (on) cl.turn_off_bias();
	else    cl.turn_on_bias();
	
	fOnOff.SetText(!on ? "Turn off" : "Turn on");
	fOnOff.SetBackgroundColor(on ? fRed : fGreen);
      }
      void Take(const slow_control::bias& bv) {
	fEnable.SetDown(bv.enable[0]);
	fReference.Select(bv.reference[0]);
	HandleEnable();
      }
      void Give(slow_control::bias& bv) {
	bv.enable   [0] = fEnable.IsDown();
	bv.reference[0] = fReference.GetSelected();
      }
    };
  }
}
#endif
      
