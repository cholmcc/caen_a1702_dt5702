/** 
 * @file      feb/probe.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Probe point configuration
 */
#ifndef FEB_PROBE_HH
#define FEB_PROBE_HH
#include <feb/memory.hh>

namespace feb {

  /** 
   * Probe point outputs 
   *
   * For example, to configure analogue output of fast shaper from
   * channel 3 (starting at 0), do 
   *
   *     probe prb;
   *     prb.fast_shaper[3].on = true;
   
   * Signals are multiplixed on probe
   * point
   */
  struct probe : memory<224>
  {
    /** Probe faster shaper analogue output */
    view<  0,1> fast_shaper;
    /** Probe slow, low gain shaper analogue output */
    view< 32,1> low_gain_shaper;
    /** Probe low gain peak sensing digital output */
    view< 64,1> low_gain_peak;
    /** Probe slow, low gain shaper analogue output */
    view< 96,1> high_gain_shaper;
    /** High gain peak sensing digital output */
    view<128,1>  high_gain_peak;
    /** Pre-amp analogue output */
    view<160,1,unsigned short,2> high_gains;
    /** Pre-amp analogue output */
    view<161,1,unsigned short,2> low_gains;

    probe()
      : fast_shaper     (_data),
	low_gain_shaper (_data),
	low_gain_peak   (_data),
	high_gain_shaper(_data),
	high_gain_peak  (_data),
	high_gains      (_data),
	low_gains       (_data)
    {
      for (size_t i = 0; i < 32; i++) {
	fast_shaper     [i] = 0;
	low_gain_shaper [i] = 0;
	low_gain_peak   [i] = 0;
	high_gain_shaper[i] = 0;
	high_gain_peak  [i] = 0;
	high_gains      [i] = 0;
	low_gains       [i] = 0;
      }
    }
    /** Encode memory into bytes */
    void to_bytes(unsigned char* o) const override {
      if (_data.count() > 1) {
	std::clog << "E: More than one bit set in probe memory - "
		  << "Refuse to write as it may short ASIC. "
		  << std::endl;
	return;
      }
      memory::to_bytes(o);
    }
    void save(std::ostream& o) const override
    {
      if (_data.count() > 1) {
	std::clog << "E: More than one bit set in probe memory - "
		  << "Will not write this configuation as it may short ASIC. "
		  << std::endl;
	return;
      }
      o << " Probe memory\n";
      for (size_t j = 0; j < 32; j++)
	o << fast_shaper[j]      <<"\t# Fast shaper ch "      << j << "\n";
      for (size_t j = 0; j < 32; j++)
	o << low_gain_shaper[j]  <<"\t# Low gain shaper ch "  << j << "\n";
      for (size_t j = 0; j < 32; j++)
	o << low_gain_peak[j]    <<"\t# Low gain peak ch "    << j << "\n";
      for (size_t j = 0; j < 32; j++)
	o << high_gain_shaper[j] <<"\t# High gain shaper ch " << j << "\n";
      for (size_t j = 0; j < 32; j++)
	o << high_gain_peak[j]   <<"\t# High gain peak ch "   << j << "\n";
      for (size_t j = 0; j < 32; j++) 
	o << high_gains[j] << "\t"
	  << low_gains[j] << "\t"
	  << "# High and low gain pre-amp ch " << j << "\n";
    }
    void load(std::istream& i) override
    {
      swallow(i);
      for (size_t j = 0; j < 32; j++)  fast_shaper     [j] = get_one_eat(i);
      for (size_t j = 0; j < 32; j++)  low_gain_shaper [j] = get_one_eat(i);
      for (size_t j = 0; j < 32; j++)  low_gain_peak   [j] = get_one_eat(i);
      for (size_t j = 0; j < 32; j++)  high_gain_shaper[j] = get_one_eat(i);
      for (size_t j = 0; j < 32; j++)  high_gain_peak  [j] = get_one_eat(i);
      for (size_t j = 0; j < 32; j++) {
	high_gains[j] = get_one(i);
	low_gains [j] = get_one_eat(i);
      }
      if (_data.count() > 1) {
	std::clog << "E: More than one bit set in probe memory - "
		  << "Will zero all bits as setting may short ASIC. "
		  << std::endl;
	_data.reset();
      }
    }
  };
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
//
// EOF
//
