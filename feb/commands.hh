/** 
 * @file      feb/commands.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Commands 
 */
#ifndef FEB_COMMANDS_HH
#define FEB_COMMANDS_HH
#include <feb/command.hh>
#include <feb/slow_control.hh>
#include <feb/event.hh>
#include <cstring>

namespace feb
{
  namespace commands
  {
    // ---------------------------------------------------------------
    /** Read switch register `FEB-RD-SR` */
    struct read_switch : command<0x00,0x01,2> {
      /** Read a switch register */
      read_switch(unsigned char addr) : command(addr) {}

      unsigned short value() const {
	return *(unsigned short*)(data);
      }
    };
    
    // ---------------------------------------------------------------
    /** Write Switch registers `FEB-WR-SR` */
    struct write_switch : command<0x00,0x02,2> {
      write_switch(unsigned char addr, unsigned short value)
	: command(addr)
      {
	memcpy(data,&value,2);
      }
    };
    
    // ---------------------------------------------------------------
    /** Read all switch registers `FEB-RD-SRFF` */
    struct read_switch_bulk : command<0x00,0x03,256> {
      using block_type = std::array<unsigned short,128>;

      /** Read all */
      read_switch_bulk() : command() {}

      /** Decodes into data member */
      block_type values() const {
	block_type values;;
	for (size_t i = 0; i < values.size(); i++)
	  values[i] = *(unsigned short*)(&data[2*i]);
	return values;
      }
    };
    
    // ---------------------------------------------------------------
    /** Write all switch registers `FEB-WD-SRFF` */
    struct write_switch_bulk : command<0x00,0x04,256> {
      using block_type = std::array<unsigned short,128>;

      write_switch_bulk(const block_type& values)
	: command() {
	for (size_t i = 0; i < values.size(); i++)
	  memcpy(&data[2*i],&(values[i]),2);
      }
    };

    // ---------------------------------------------------------------
    /** Set host MAC on clients and recieve client MACs `FEB-SET-RECV` */
    struct set_host : command<0x01,0x01,6,500> {
      set_host(const mac_address& host, unsigned short vcxo_corr=0)
	: command(vcxo_corr)
      {
	for (size_t i = 0; i < host.size(); i++) 
	  data[i] = host[i];
      }
      mac_address fw() const {
	return {data[0],data[1],data[2],data[3],data[4],data[5]}; }
    };

    // ---------------------------------------------------------------
    /** Start, stop, reset acquisition `FEB-GEN-INIT` */
    struct acquire : command<0x01,0x02,0,1000> {
      enum mode {
	STOP = 0x0000,
	RESET = 0x1010,
	START = 0x2020,
	CPU_RESET = 0xFFFF };
      acquire(unsigned short mode) : command(mode) {}
    };
    
    // ---------------------------------------------------------------
    /** Turn bias voltage on `FEBGEN-HVON` */
    struct turn_on_bias : command<0x01,0x03,0,1000> {
      turn_on_bias() : command(0) {};
    };

    // ---------------------------------------------------------------
    /** Turn bias voltage off `FEBGEN-HVOF` */
    struct turn_off_bias : command<0x01,0x04,0,1000> {
      turn_off_bias() : command(0) {};
    };

    // ---------------------------------------------------------------
    /** Get the event rate `FEB-GET-RATE` */
    struct get_event_rate : command<0x01,0x05,4,1000> {
      get_event_rate() : command() {}

      float rate() const { return *((float*)data); } // Uh!? 
    };

    // ---------------------------------------------------------------
    /** Read a memory */
    template <typename Memory,
	      unsigned char FAMILY,
	      unsigned char ID=0x1,
	      unsigned short TIMEOUT=50000>
    struct read_memory : command<FAMILY,ID,Memory::nbytes,TIMEOUT> {
      using base=command<FAMILY,ID,Memory::nbytes,TIMEOUT>;
      read_memory(unsigned short addr=0) : base(addr) {}
      void get(Memory& m) { m.from_bytes(base::data); }
    };
    // ---------------------------------------------------------------
    /** Write a memory */
    template <typename Memory,
	      unsigned char FAMILY,
	      unsigned char ID=0x2,
	      unsigned short TIMEOUT=50000>
    struct write_memory : command<FAMILY,ID,Memory::nbytes,TIMEOUT> {
      using base=command<FAMILY,ID,Memory::nbytes,TIMEOUT>;
      write_memory(const Memory& m, unsigned short addr=0) : base(addr) {
	m.to_bytes(base::data);
      }
    };
    
    // ---------------------------------------------------------------
    /** Read slow control register - `FEB-RD-SCR` */
    using read_slow_control=read_memory<slow_control,0x02>;
    /** Write slow control register `FEB-WR-SCR` */
    using write_slow_control=write_memory<slow_control,0x02>;

    // ---------------------------------------------------------------
    /** Read event - `FEB-RD-CDR` */
    struct read_events : command<0x03,0x01,1464,5000> {
      using base=command<0x03,0x01,1464,5000>;

      /** How much data do we have */
      size_t remain = 0;
      /** Current offset */
      size_t off    = 0;

      /** Read events from a card */
      read_events(unsigned int addr=0) : command(addr) {}

      /** Verify read */
      bool verify() const override {
	if (id == 0x03) return true; // No more data
	return command_base::verify();
      }
	
      /** Take length of reply */
      bool length(int len) override {
	remain = 0;
	off    = 0;
	
	if (len - 18 < 0) return false;
	
	remain = len-18;
	off = 0;
	return len > 0;
      }
      
      /** 
       * Get the next channel event.  When no more events are
       * available, return false.
       *
       * @param ev   Structure to read data into 
       * @param toff Correction for drift relative to GPS
       *
       * @return true when event filled, false when no more data 
       */
      bool get(event& ev, unsigned int toff) {
	if (off >= remain) return false; // No more events to decoe
	
	if (remain - off < 76) {
	  std::clog << "W: Not enough data for event: "
		    << remain - off << " < " << 76 << std::endl;
	  return false; // Not enough data
	}

	ev.overwritten  = *(unsigned short*)(&data[off]); off += 2;
	ev.lost         = *(unsigned short*)(&data[off]); off += 2;
	unsigned int t0 = *(unsigned int*  )(&data[off]); off += 4;
	unsigned int t1 = *(unsigned int*  )(&data[off]); off += 4;
	for (size_t i = 0; i < 32; i++) {
	  ev.adc[i] = *(unsigned short*)(&data[off]);
	  off += 2;
	}

	// Take the status bits 
	ev.t0_sync      = (t0 & 0x80000000);
	ev.t1_sync      = (t1 & 0x80000000);
	ev.t0_overflow  = (t0 & 0x40000000);
	ev.t1_overflow  = (t1 & 0x40000000);
	t0              = (t0 & 0x3FFFFFFF);
	t1              = (t1 & 0x3FFFFFFF);
	// Decode the gray encoding
	ev.t0           = ((gray_to_bin(t0 >> 2) << 2) | (t0 & 0x3)) + toff;
	ev.t1           = ((gray_to_bin(t1 >> 2) << 2) | (t1 & 0x3)) + toff;
	
	return true;
      }

    };
    
    // ---------------------------------------------------------------
    /** Read probe register - `FEB-RD-PMR` */
    using read_probe=read_memory<probe,0x04>;
    /** Write probe register - `FEB-WR-PMR` */
    using write_probe=write_memory<probe,0x04>;
    
    // ---------------------------------------------------------------
    /** Read trigger mask - `FEB-RD-FIL` */
    using read_flexible_logic=read_memory<flexible_logic,0x06>;

    /** Write trigger mask - `FEB-WR-FIL` */
    using write_flexible_logic=write_memory<flexible_logic,0x06>;
	
#if 0
    // *** IMPORTANT IMPORTANT IMPORTANT ***
    //
    // The code below needs to be checked before deploying
    // 
    // ---------------------------------------------------------------
    /** Read firmware - `FEB-RD-FW` 
     * 
     * To read the formware directly from running FPGA (addr 0) 
     * or memory, do 
     *
     * @code 
     unsigned int   addr    = write_firmware_address::fpga;
     unsigned char* fw[1024*1024];
     unsigned char* ptr     = fw;
     size_t         nreads  = 16;
     size_t         nblocks = 64;
     for (size_t i = 0; i < nreads; i++) { 
       read_firmware rd(addr, nblocks * 1024);
       memcpy(ptr, rd.data, nblocks * 1024);
       addr += 64*1024;
       ptr  += 64*1024;
     }
     @endcode 
     *
     */
    struct read_firmware : command<0x05,0x01,1024> {
      struct req {
	unsigned start : 24;
	unsigned len : 16;
      };
      unsigned short blocks;
      
      read_firmware(unsigned int addr, unsigned short blocks) :
	command(), blocks(blocks)
      {
	req r{addr & 0x00FFFFFF, blocks};
	memcpy(data,&req,5);
      }
      /** Take the data from the reply */
      bool take(unsigned short len, const packet& pkt) override
      {
	// Check for end of file 
	if (pkt.family == family() && pkt.id == 0x03) return false;

	// Check the check-sum 
	unsigned char crc7 = crc(data, blocks*1024);
	return crc7 == pkt.value;
      }
    };
    /** 
     * Write firwmare data - FEB-DATA-FW 
     * 
     * Flashing the firmware consist of 
     *
     * - First, write the address space to write to and the number of
     *   1024 byte blocks that will be written
     *   (write_firmware_address)
     * - Then write that many blocks of data 
     */
    struct write_firmware_block : command<0x05,0x04,1024,5000> {
      write_firmware(const unsigned char* fw) : command(crc(fw,1024))
      {
	memcpy(data,fw,1024);
      }
    };

    /** 
     * Write firmware address to write to - `FEB-WR-FW` 
     *
     * The address 0x20000 is PROM (safe)
     * The address 0x0     is FPGA 
     *
     * To program the PROM and FPGA (supposing we have the binary data in fw) 
     *
     * @code 
     size_t         nblocks = 64;
     unsigned char* ptr     = fw;
     write_firmware_address(write_firmware_address::to_flash, 
                            write_firmware_address::prom, 
			    nblocks);
     for (size_t i = 0; i < nblocks; i++)  {
        write_firmware_data(ptr);
        ptr += 1024;
     }
     
     write_firmware_address(write_firmware_address::copy_to_fpga,
                            write_firmware_address::prom,
			    nblocks);
     @endcode 
     *
     * To write to the FPGA directly do 
     *
     * @code 
     size_t         nblocks = 8;
     size_t         npages  = 64;
     unsigned char* ptr     = fw;
     unsigned int   addr    = write_firmware_address::fpga;
     for (size_t i = 0; i < nblocks; i++) { 
        write_firmware_address(write_firmware_address::to_flash,
	                       addr,npages);
        
        for (size_t j = 0; j < npages; j++) {
          write_firmware_data(ptr);
          ptr = 1024;
        }
	addr += npages * 1024;
     }
     @endcode   
     *
     */
    struct write_firmware_address : command<0x05,0x02,5> {
      /** Base addresses */
      enum {
	fpga = 0x80000,
	prom = 0x20000
      };
      /** What to do */
      enum {
	to_flash = 0x0,
	copy_to_fpga = 0x0101,
      };
      write_firmware_address(unsigned short mode,
			     unsigned int   addr,
			     unsigned short blocks)
	: command(mode)
      {
	unsigned int faddr = addr & 0xFFFFFF;
	memcpy(data,&faddr, 3);
	memcpy(data,&blocks, 2);
      }
    };
#endif
  }
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
