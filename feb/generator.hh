/** 
 * @file      feb/generator.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Event generator for emulator
 */
#ifndef FEB_GENERATOR_HH
#define FEB_GENERATOR_HH
#include <feb/slow_control.hh>
#include <feb/flexible_logic.hh>
#include <random>
#include <chrono>

namespace feb
{
  //==================================================================
  /** 
   * Generates funny events 
   */
  struct generator
  {
    using normal = std::normal_distribution<float>;
    using poisson = std::poisson_distribution<int>;
    using p_normal = typename normal::param_type;
    using p_poisson = typename poisson::param_type;
    using clock = std::chrono::steady_clock;
    using time_point = typename clock::time_point;
    
    std::default_random_engine        _engine;
    normal  _normal;
    poisson _poisson;
    time_point _start;
    size_t     _nev;
    
    generator(unsigned int seed=123456)
      : _engine(),
	_normal(0,1),
	_poisson(3)
    {
      std::clog << "I: seed = " << seed << std::endl;
      _engine.seed(seed == 0 ? std::random_device()() : seed);
    }
    void clear() {
      _start = clock::now();
      _nev   = 0;
    }
    
    /** Generate events */
    int generate(unsigned char* data,
		 const slow_control& sc,
		 const flexible_logic& fl)
    {
      // std::clog << "D: Generating events " << std::flush;
      // Check if we use low_gain shaper
      bool           low = sc.lg.preamp_send[0];
      unsigned short thr = sc.th.value_1[0]; // 0-1023
      
      // Determine number of events, capped at 8, at least 1
      size_t nev = std::min(_poisson(_engine),8);
      std::clog << "D: " << nev << " " << std::flush;

      if (nev <= 0) {
	// std::clog << "D: " << std::endl;
	return 0; // EOF
      }

      // Write pointer
      unsigned char* ptr = data;

      // Start time - Poisson distributed 
      unsigned int t0  = _poisson(_engine, p_poisson{100.});
      unsigned int t1  = _poisson(_engine, p_poisson{100.});
      int          len = 0;
      for (size_t iev = 0; iev < nev; iev++) {
	// Triggered ?
	unsigned trg = 0;;

	// First, increment time 
	t0 += _normal(_engine,p_normal{100,10}); 
	t1 += _normal(_engine,p_normal{100,10}); 
	
	// Then, we generate our ADC values
	std::array<unsigned short,32> adcs;
	for (size_t ich = 0; ich < 32; ich++) {
	  // std::clog << "D: Channel # " << std::setw(2) << ich 
	  // 	    << std::setw(9)
	  // 	    << (sc.ch.mask[ich] ? "enabled" : "disabled")
	  // 	    << " preamp" 
	  // 	    << std::setw(9)
	  // 	    << (sc.ch.disable_preamps[ich] ? "disabled" : "enabled")
	  // 	    << std::flush;
	  if (not sc.ch.mask[ich] or // Not in mask
	      sc.ch.disable_preamps[ich]) {// Pre-amp off
	    std::clog << " D: nothing" << std::endl;
	    continue; // This channel is zero
	  }

	  // Generate unscaled signal 
	  float          sig     = _normal(_engine);

	  // Get bias (0-255) and gain (0-63)
	  unsigned short bias    = sc.ch.biases[ich];
	  bool           bias_en = sc.ch.biases_enable[ich];
	  unsigned short gain    = (low ?
				    sc.ch.low_gains[ich] : 
				    sc.ch.high_gains[ich]);

	  // Modify signal
	  // - Large gain, large signal
	  // - Large bias, wide signal (no bias, low signal)
	  adcs[ich] = int((bias_en ? gain : 32) * 16
			  + sig * (bias_en ? bias : 200));

	  // Check if this channel is in trigger mask and wether signal
	  // is above threshold
	  // std::clog << "D: trig " << fl.mask1[ich]
	  // 	    << " adc " << std::setw(6) << adcs[ich]
	  // 	    << " thr " << thr << std::flush;
	  if (fl.mask1[ich] and adcs[ich] > thr) trg++;
	  // std::clog << "D: " << (trg>0 ? "triggered" : "") << std::endl;
	}

	// std::clog << "D: " << (trg>0 ? "+" : "-") << std::flush;
	
	// Then check if we got triggered 
	if (trg == 0) continue;

	len += 2+2+4+4+32*2; // 76
	// Overwritten and lost 2 bytes each 
	*(unsigned short*)ptr = _poisson(_engine,p_poisson{2}); ptr += 2;
	*(unsigned short*)ptr = _poisson(_engine,p_poisson{1}); ptr += 2;
	// Times 
	*(unsigned int*  )ptr = (t0 & 0x3FFFFFFF); ptr += 4;
	*(unsigned int*  )ptr = (t1 & 0x3FFFFFFF); ptr += 4;
	// ADCs
	for (size_t ich = 0; ich < 32; ich++) {
	  *(unsigned short*)ptr = adcs[ich] & 0xFFFF;
	  ptr += 2;
	}

	_nev++;
      }
      // std::clog << "D: \tdone - " << len << " bytes " << std::endl;
      return len;
    }
    float rate() const {
      auto                          now = clock::now();
      std::chrono::duration<double> elapsed = now-_start;
      return 1e3 * float(_nev) / elapsed.count();
    }
  };
}
#endif
