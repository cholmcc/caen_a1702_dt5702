/** 
 * @file      feb/flexible_logic.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Flexible (trigger) logic
 */
#ifndef FEB_FLEXIBLE_LOGIC_HH
#define FEB_FLEXIBLE_LOGIC_HH
#include <feb/memory.hh>

namespace feb
{
  /** 
   * Flexible logic 
   * 
   */
  struct flexible_logic : memory<72,true>
  {
    view< 0,1> mask1;
    view<32,1> mask2;
    view<64,8> majority;

    flexible_logic(const flexible_logic&) = delete;

    flexible_logic& operator=(const flexible_logic&) = delete;

    flexible_logic()
      : mask1(_data),
	mask2(_data),
	majority(_data)
    {
      for (size_t i = 0; i < 32; i++) {
	mask1[i] = 0;
	mask2[i] = 0;
      }
      majority[0] = 0;
    }

    void save(std::ostream& o) const override {
      o << "# Flexible logic\n";
      for (size_t i = 0; i < 32; i++)
	o << mask1[i] << "\t# Mask 1 channel " << i << "\n";
      for (size_t i = 0; i < 32; i++)
	o << mask2[i] << "\t# Mask 2 channel " << i << "\n";
      o << majority[0] << "\t# Majority\n";
    }
    
    void load(std::istream& i) override {
      swallow(i);
      for (size_t j = 0; j < 32; j++)  mask1[j] = get_one_eat(i);
      for (size_t j = 0; j < 32; j++)  mask2[j] = get_one_eat(i);
      majority[0] = get_one_eat(i);
    }
  };
}

    
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
//
// EOF
//
