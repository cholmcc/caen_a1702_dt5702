/** 
 * @file      feb/slow_control.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Slow control registers 
 */
#ifndef FEB_SLOW_CONTROL_HH
#define FEB_SLOW_CONTROL_HH
#include <feb/utils.hh>
#include <feb/memory.hh>
#include <iomanip>
#include <vector>

namespace feb
{
  struct slow_control : memory<8 * 143>
  {
    //----------------------------------------------------------------
    struct channels {
      view<   0,4>                   time_thresholds  ;
      view< 128,4>                   charge_thresholds;
      view< 265,1>                   mask             ;
      view< 331,8,unsigned int,9>    biases           ;
      view< 339,1,unsigned int,9>    biases_enable    ;
      view< 619,6,unsigned int,15>   high_gains       ;
      view< 625,6,unsigned int,15>   low_gains        ;
      view< 631,1,unsigned int,15>   high_tests       ;
      view< 632,1,unsigned int,15>   low_tests        ;
      view< 633,1,unsigned int,15>   disable_preamps  ;
      channels(data_type_ref data)
	: time_thresholds  (data),
	  charge_thresholds(data),
	  mask             (data),
	  biases           (data),
	  biases_enable    (data),
	  high_gains       (data),
	  low_gains        (data),
	  high_tests       (data),
	  low_tests        (data),
	  disable_preamps  (data)
      {
	std::vector<std::pair<unsigned,bool>>
	  inp_val{{198,1},{203,1},{188,1},{202,1},
		  {192,1},{203,1},{191,1},{202,1},
		  {203,1},{203,1},{193,1},{197,1},
		  {191,1},{201,1},{193,1},{205,1},
		  {204,1},{190,1},{198,1},{195,1},
		  {191,1},{193,1},{197,1},{192,1},

		  {194,1},{185,1},{187,1},{192,1},
		  {191,1},{195,1},{188,1},{198,1}};
	for (size_t i = 0; i <32; i++) {
	  time_thresholds    [i] = 0;
	  charge_thresholds  [i] = 0;
	  mask               [i] = 0;
	  biases             [i] = inp_val[i].first;
	  biases_enable      [i] = inp_val[i].second;
	  high_gains         [i] = 51;
	  low_gains          [i] = 51;
	  high_tests         [i] = false;
	  low_tests          [i] = false;
	  disable_preamps    [i] = true;
	}
      }
      void save(std::ostream& o) const {
	o << "# Time\t"
	  << "Charge\t"
	  << "On\t"
	  << "Bias\t"
	  << "Enable\t"
	  << "High\t"
	  << "Low\t"
	  << "Test\t"
	  << "Test\t"
	  << "!Preamp\n";
	for (size_t j = 0; j < 32; j++) {
	  o << time_thresholds  [j] << "\t"
	    << charge_thresholds[j] << "\t"
	    << mask             [j] << "\t"
	    << biases           [j] << "\t"
	    << biases_enable    [j] << "\t"
	    << high_gains       [j] << "\t"
	    << low_gains        [j] << "\t"
	    << high_tests       [j] << "\t"
	    << low_tests        [j] << "\t"
	    << disable_preamps  [j]
	    << "\t# Channel " << j << std::endl;
	}
      }
      void load(std::istream& i) {
	swallow(i);
	for (size_t j = 0; j < 32; j++) {
	  time_thresholds  [j] = get_one(i);
	  charge_thresholds[j] = get_one(i);
	  mask             [j] = get_one(i);
	  biases           [j] = get_one(i);
	  biases_enable    [j] = get_one(i);
	  high_gains       [j] = get_one(i);
	  low_gains        [j] = get_one(i);
	  high_tests       [j] = get_one(i);
	  low_tests        [j] = get_one(i);
	  disable_preamps  [j] = get_one_eat(i);
	}
      }
    } ch;

    //----------------------------------------------------------------
    struct high_gain {
      view< 297,1> disable_track_pulser ;
      view< 298,1> enable_track         ;
      view< 302,1> pp_pdet              ;
      view< 303,1> enable_pdet          ;
      view< 306,1> sca_or_peak          ;
      view< 318,1> disable_shaper_pulser;
      view< 319,1> enable_shaper        ;
      view< 320,3> shaper_time          ;
      view< 324,1> disable_preamp_pulser;
      view< 325,1> enable_preamp        ;
      view<1127,1> enable_ota           ;
      view<1128,1> disable_ota_pulser        ;

      high_gain(data_type_ref data)
	: disable_track_pulser (data),
          enable_track         (data),
	  pp_pdet              (data),
	  enable_pdet          (data),
	  sca_or_peak          (data),
          disable_shaper_pulser(data),
          enable_shaper        (data),
          shaper_time          (data),
          disable_preamp_pulser(data),
          enable_preamp        (data),
          enable_ota           (data),
          disable_ota_pulser   (data)
      {
	disable_track_pulser  [0] = true;
	enable_track          [0] = true;
	pp_pdet               [0] = false;
	enable_pdet           [0] = false;
	sca_or_peak           [0] = true;
	disable_shaper_pulser [0] = true;
	enable_shaper         [0] = true;
	shaper_time           [0] = 6;
	disable_preamp_pulser [0] = true;
	enable_preamp         [0] = true;
	enable_ota            [0] = true;
	disable_ota_pulser    [0] = true;
      }
      void save(std::ostream& o) const {
	o << "# High gain\n"
	  << disable_track_pulser [0] << "\t# Disable track'n'hold pulser\n"
	  << enable_track         [0] << "\t# Enable track'n'hold\n"
	  << pp_pdet              [0] << "\t# PP peak detect\n"
	  << enable_pdet          [0] << "\t# Enable peak detect\n"
	  << sca_or_peak          [0] << "\t# Sample or peak\n"
	  << disable_shaper_pulser[0] << "\t# Disable shaper pulser\n"
	  << enable_shaper        [0] << "\t# Enable shaper\n"
	  << shaper_time          [0] << "\t# Shaper time\n"
	  << disable_preamp_pulser[0] << "\t# Disable preamp pulser\n"
	  << enable_preamp        [0] << "\t# Enable preamp\n"
	  << enable_ota           [0] << "\t# Enable OTA\n"
	  << disable_ota_pulser   [0] << "\t# Disable OTA pulser\n";
      }
      void load(std::istream& i) {
	swallow(i);
	disable_track_pulser [0] = get_one_eat(i);
	enable_track         [0] = get_one_eat(i);
	pp_pdet              [0] = get_one_eat(i);
	enable_pdet          [0] = get_one_eat(i);
	sca_or_peak          [0] = get_one_eat(i);
	disable_shaper_pulser[0] = get_one_eat(i);
	enable_shaper        [0] = get_one_eat(i);
	shaper_time          [0] = get_one_eat(i);
	disable_preamp_pulser[0] = get_one_eat(i);
	enable_preamp        [0] = get_one_eat(i);
	enable_ota           [0] = get_one_eat(i);
	disable_ota_pulser   [0] = get_one_eat(i);
      }	
    } hg;
    
    //----------------------------------------------------------------
    struct low_gain {
      view< 299,1> disable_track_pulser ; // Common
      view< 300,1> enable_track         ; // Common
      view< 304,1> pp_pdet              ; // Common
      view< 305,1> enable_pdet          ; // Common
      view< 307,1> sca_or_peak          ; // Common
      view< 313,1> disable_shaper_pulser; // Common
      view< 314,1> enable_shaper        ; // Common
      view< 315,3> shaper_time          ; // Common
      view< 323,1> preamp_bias          ; // Specific
      view< 326,1> disable_preamp_pulser; // Specific
      view< 327,1> enable_preamp        ; // Common
      view< 328,1> preamp_send          ; // Specific
      view<1129,1> enable_ota           ; // Common
      view<1130,1> disable_ota_pulser   ; // Common

      low_gain(data_type_ref data)
	: disable_track_pulser  (data),
	  enable_track          (data),
	  pp_pdet               (data),
	  enable_pdet           (data),
	  sca_or_peak           (data),
	  disable_shaper_pulser (data),
	  enable_shaper         (data),
	  shaper_time           (data),
	  preamp_bias           (data),
	  disable_preamp_pulser (data),
	  enable_preamp         (data),
	  preamp_send           (data),
	  enable_ota            (data),
	  disable_ota_pulser    (data)
      {
	disable_track_pulser  [0] = true;
	enable_track          [0] = true;
	pp_pdet               [0] = false;
	enable_pdet           [0] = false;
	sca_or_peak           [0] = true;
	disable_shaper_pulser [0] = true;
	enable_shaper         [0] = true;
	shaper_time           [0] = 6;
	preamp_bias           [0] = false;
	disable_preamp_pulser [0] = true;
	enable_preamp         [0] = true;
	preamp_send           [0] = false;
	enable_ota            [0] = true;
	disable_ota_pulser    [0] = true;
      }
      void save(std::ostream& o) const {
	o << "# Low gain\n"
	  << disable_track_pulser [0] << "\t# Disable track'n'hold pulser\n"
	  << enable_track         [0] << "\t# Enable track'n'hold\n"
	  << pp_pdet              [0] << "\t# PP peak detect\n"
	  << enable_pdet          [0] << "\t# Enable peak detect\n"
	  << sca_or_peak          [0] << "\t# Sample(1) or peak(0)\n"
	  << disable_shaper_pulser[0] << "\t# Disable shaper pulser\n"
	  << enable_shaper        [0] << "\t# Enable shaper\n"
	  << shaper_time          [0] << "\t# Shaper time\n"
	  << preamp_bias          [0] << "\t# Preamp bias mode\n"
	  << disable_preamp_pulser[0] << "\t# Disable preamp pulser\n"
	  << enable_preamp        [0] << "\t# Enable preamp\n"
	  << preamp_send          [0] << "\t# Send output\n"
	  << enable_ota           [0] << "\t# Enable OTA\n"
	  << disable_ota_pulser   [0] << "\t# Disable OTA pulser\n";
      }
      void load(std::istream& i) {
	swallow(i);
	disable_track_pulser [0] = get_one_eat(i);
	enable_track         [0] = get_one_eat(i);
	pp_pdet              [0] = get_one_eat(i);
	enable_pdet          [0] = get_one_eat(i);
	sca_or_peak          [0] = get_one_eat(i);
	disable_shaper_pulser[0] = get_one_eat(i);
	enable_shaper        [0] = get_one_eat(i);
	shaper_time          [0] = get_one_eat(i);
	preamp_bias          [0] = get_one_eat(i);
	disable_preamp_pulser[0] = get_one_eat(i);
	enable_preamp        [0] = get_one_eat(i);
	preamp_send          [0] = get_one_eat(i);
	enable_ota           [0] = get_one_eat(i);
	disable_ota_pulser   [0] = get_one_eat(i);
      }	
    } lg;

    //----------------------------------------------------------------
    struct thresholds {
      view< 261,1>  enable_charge   ;
      view< 262,1>  pp_charge       ;
      view< 263,1>  enable_time     ;
      view< 264,1>  pp_time         ;
      view<1103,1>  enable_1        ;
      view<1104,1>  disable_1_pulser;
      view<1105,1>  enable_2        ;
      view<1106,1>  disable_2_pulser;
      view<1107,10> value_1         ;
      view<1117,10> value_2         ;

      thresholds(data_type_ref data)
	: enable_charge   (data),
	  pp_charge       (data),
	  enable_time     (data),
	  pp_time         (data),
	  enable_1        (data),
	  disable_1_pulser(data),
	  enable_2        (data),
	  disable_2_pulser(data),
	  value_1         (data),
      	  value_2         (data)
      {
	enable_charge   [0] = true;
	pp_charge       [0] = true;
	enable_time     [0] = true;
	pp_time         [0] = true;
	enable_1        [0] = true;
	disable_1_pulser[0] = true;
	enable_2        [0] = true;
	disable_2_pulser[0] = true;
	value_1         [0] = 377;
	value_2         [0] = 250;
      }
      void save(std::ostream& o) const {
	o << "# Thresholds\n"
	  << enable_charge    [0] << "\t# Enable charge threshold/channel\n"
	  << pp_charge        [0] << "\t# PP charge threshold/channel\n"
	  << enable_time      [0] << "\t# Enable time threshold/channel\n"
	  << pp_time          [0] << "\t# PP time threshold/channel\n"
	  << enable_1         [0] << "\t# Enable threshold 1\n"
	  << disable_1_pulser [0] << "\t# Disable threshold 1 pulser\n"
	  << enable_2         [0] << "\t# Enable threshold 2\n"
	  << disable_2_pulser [0] << "\t# Disable threshold 2 pulser\n"
	  << value_1          [0] << "\t# Threshold 1 value\n"
	  << value_2          [0] << "\t# Threshold 2 value\n";
      }
      void load(std::istream& i) {
	swallow(i);
	enable_charge    [0] = get_one_eat(i);
	pp_charge        [0] = get_one_eat(i);
	enable_time      [0] = get_one_eat(i);
	pp_time          [0] = get_one_eat(i);
	enable_1         [0] = get_one_eat(i);
	disable_1_pulser [0] = get_one_eat(i);
	enable_2         [0] = get_one_eat(i);
	disable_2_pulser [0] = get_one_eat(i);
	value_1          [0] = get_one_eat(i);
	value_2          [0] = get_one_eat(i);
      }
    } th;

    //----------------------------------------------------------------
    struct bias {
      view< 329,1> enable	 ; 
      view< 330,1> reference; 

      bias(data_type_ref data)
	: enable    (data),
	  reference (data)
      {
	enable   [0] = true;
	reference[0] = true;
      }
      void save(std::ostream& o) const {
	o << "# Bias\n"
	  << enable[0]    << "\t# Enable bias voltage\n"
	  << reference[0] << "\t# Bias refernce (0: internal, 1: external)\n";
      }
      void load(std::istream& i) {
	swallow(i);
	enable   [0] = get_one_eat(i);
	reference[0] = get_one_eat(i);
      }
    } bv;

    //----------------------------------------------------------------
    struct trigger {
      view< 256,1> enable_discriminator         ;
      view< 257,1> disable_discriminator_pulser ;
      view< 258,1> latched_output               ;
      view< 259,1> enable_discriminator2        ;
      view< 260,1> disable_discriminator2_pulser;
      view< 308,1> bypass_peak_sense            ;
      view< 309,1> external                     ;
      view<1134,1> enable_validate              ;
      view<1135,1> disable_validate_pulser      ;
      view<1138,1> enable_multiplex             ;
      view<1139,1> enable_or_out                ;
      view<1140,1> enable_or_collect_out        ;
      view<1141,1> polarity             ;
      view<1142,1> enable_or_time_out           ;
      view<1143,1> enable_channel_out           ;

      trigger(data_type_ref data)
	: enable_discriminator          (data),
	  disable_discriminator_pulser  (data),
	  latched_output                (data),
	  enable_discriminator2         (data),
	  disable_discriminator2_pulser (data),
	  bypass_peak_sense             (data),
	  external                      (data),
	  enable_validate               (data),
	  disable_validate_pulser       (data),
	  enable_multiplex              (data),
	  enable_or_out                 (data),
	  enable_or_collect_out         (data),
	  polarity                      (data),
	  enable_or_time_out            (data),
	  enable_channel_out            (data)
      {
	enable_discriminator         [0] = true;
	disable_discriminator_pulser [0] = true;
	latched_output               [0] = false;
	enable_discriminator2        [0] = true;
	disable_discriminator2_pulser[0] = true;
	bypass_peak_sense            [0] = true;
	external                     [0] = false;
	enable_validate              [0] = false;
	disable_validate_pulser      [0] = false;
	enable_multiplex             [0] = false;
	enable_or_out                [0] = true;
	enable_or_collect_out        [0] = false;
	polarity                     [0] = false;
	enable_or_time_out           [0] = false;
	enable_channel_out           [0] = true;
      }
      void save(std::ostream& o) const {
	o << "# Trigger\n"
	  << enable_discriminator         [0] << "\t# Enable charge disc.\n"
	  << disable_discriminator_pulser [0] << "\t# Disable disc. pulser\n"
	  << latched_output               [0] << "\t# Latched (1: RS, 0: trig)\n"
	  << enable_discriminator2        [0] << "\t# Enable time disc.\n"
	  << disable_discriminator2_pulser[0] << "\t# Disable time disc. pulser\n"
	  << bypass_peak_sense            [0] << "\t# By-pass peak sense cell\n"
	  << external                     [0] << "\t# External trigger\n"
	  << enable_validate              [0] << "\t# Enable validate recv.\n"
	  << disable_validate_pulser      [0] << "\t# Disable validate pulser\n"
	  << enable_multiplex             [0] << "\t# Digital multiplex output\n"
	  << enable_or_out                [0] << "\t# Enable OR-32 output\n"
	  << enable_or_collect_out        [0] << "\t# Enable OR-32 open coll.\n"
	  << polarity                     [0] << "\t# Polarity of trigger\n"
	  << enable_or_time_out           [0] << "\t# Enable OR-32 time\n"
	  << enable_channel_out           [0] << "\t# Enable trigger out\n";
      }
      void load(std::istream& i) {
	swallow(i);
	enable_discriminator         [0] = get_one_eat(i);
	disable_discriminator_pulser [0] = get_one_eat(i);
	latched_output               [0] = get_one_eat(i);
	enable_discriminator2        [0] = get_one_eat(i);
	disable_discriminator2_pulser[0] = get_one_eat(i);
	bypass_peak_sense            [0] = get_one_eat(i);
	external                     [0] = get_one_eat(i);
	enable_validate              [0] = get_one_eat(i);
	disable_validate_pulser      [0] = get_one_eat(i);
	enable_multiplex             [0] = get_one_eat(i);
	enable_or_out                [0] = get_one_eat(i);
	enable_or_collect_out        [0] = get_one_eat(i);
	polarity                     [0] = get_one_eat(i);
	enable_or_time_out           [0] = get_one_eat(i);
	enable_channel_out           [0] = get_one_eat(i);
      }
    } tr;

    //----------------------------------------------------------------
    struct misc {
      view< 301,1> sca_bias                  ;
      view< 310,1> disable_fast_follow_pulser;
      view< 311,1> enable_fast_shaper        ;
      view< 312,1> disable_fast_shaper_pulser;
      view<1099,1> disable_temp_pulser       ;
      view<1100,1> enable_temp               ;
      view<1101,1> disable_bandgap_pulser    ;
      view<1102,1> enable_bandgap            ;
      view<1131,1> enable_probe_ota          ;
      view<1132,1> disable_probe_ota_pulser  ;
      view<1133,1> otaq_test                 ;
      view<1136,1> enable_raz                ;
      view<1137,1> disable_raz_pulser        ;

      misc(data_type_ref data)
	: sca_bias                  (data),
	  disable_fast_follow_pulser(data),
	  enable_fast_shaper        (data),
	  disable_fast_shaper_pulser(data),
	  disable_temp_pulser       (data),
	  enable_temp               (data),
	  disable_bandgap_pulser    (data),
	  enable_bandgap            (data),
	  enable_probe_ota          (data),
	  disable_probe_ota_pulser  (data),
	  otaq_test                 (data),
	  enable_raz                (data),
	  disable_raz_pulser        (data)
      {
	sca_bias                  [0] = true;
	disable_fast_follow_pulser[0] = true;
	enable_fast_shaper        [0] = true;
	disable_fast_shaper_pulser[0] = true;
	disable_temp_pulser       [0] = true;
	enable_temp               [0] = true;
	disable_bandgap_pulser    [0] = true;
	enable_bandgap            [0] = true;
	enable_probe_ota          [0] = true;
	disable_probe_ota_pulser  [0] = true;
	otaq_test                 [0] = false;
	enable_raz                [0] = false;
	disable_raz_pulser        [0] = false;
      }
      void save(std::ostream& o) const {
	o << "# Misc\n"
	  << sca_bias                  [0] << "\t# 1: weak, 0: high 5MHz R/O\n"
	  << disable_fast_follow_pulser[0] << "\t# Disable f/s follow pulser\n"
	  << enable_fast_shaper        [0] << "\t# Enable f/s\n"
	  << disable_fast_shaper_pulser[0] << "\t# Disable f/s pulser\n"
	  << disable_temp_pulser       [0] << "\t# Disable temperature pulser\n"
	  << enable_temp               [0] << "\t# Enable temperature sensor\n"
	  << disable_bandgap_pulser    [0] << "\t# Disable band-gap pulser\n"
	  << enable_bandgap            [0] << "\t# Enable band-gap\n"
	  << enable_probe_ota          [0] << "\t# Enable probe OTA\n"
	  << disable_probe_ota_pulser  [0] << "\t# Disable probe OTA pulser\n"
	  << otaq_test                 [0] << "\t# OTAQ test \n"
	  << enable_raz                [0] << "\t# Enable reset latch trigger recv.\n"
	  << disable_raz_pulser        [0] << "\t# Disable reset latch trigger rec. pulser\n";
      }
      void load(std::istream& i) {
	swallow(i);
	sca_bias                  [0] = get_one_eat(i);
	disable_fast_follow_pulser[0] = get_one_eat(i);
	enable_fast_shaper        [0] = get_one_eat(i);
	disable_fast_shaper_pulser[0] = get_one_eat(i);
	disable_temp_pulser       [0] = get_one_eat(i);
	enable_temp               [0] = get_one_eat(i);
	disable_bandgap_pulser    [0] = get_one_eat(i);
	enable_bandgap            [0] = get_one_eat(i);
	enable_probe_ota          [0] = get_one_eat(i);
	disable_probe_ota_pulser  [0] = get_one_eat(i);
	otaq_test                 [0] = get_one_eat(i);
	enable_raz                [0] = get_one_eat(i);
	disable_raz_pulser        [0] = get_one_eat(i);
      }
    } mc;

    //----------------------------------------------------------------
    slow_control()
      : ch(_data),
	hg(_data),
	lg(_data),
	th(_data),
	bv(_data),
	tr(_data),
	mc(_data)
    {
    }
    void save(std::ostream& o) const override {
      ch.save(o);
      hg.save(o);
      lg.save(o);
      th.save(o);
      bv.save(o);
      tr.save(o);
      mc.save(o);
      o << "# EOF" << std::endl;
    }
    void load(std::istream& i) override {
      ch.load(i);
      hg.load(i);
      lg.load(i);
      th.load(i);
      bv.load(i);
      tr.load(i);
      mc.load(i);
    }
  };
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
    
    
    
    
    
    
