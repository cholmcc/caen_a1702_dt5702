/** 
 * @file      feb/utils.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Various utilites
 */
#ifndef FEB_UTILS_HH
#define FEB_UTILS_HH
#include <iostream>
#include <iomanip>
#include <string>
#include <type_traits>
#include <bitset>

namespace feb
{
  //==================================================================
  /** 
   * Calculate check-sum 
   */
  inline unsigned char crc(unsigned char* data, unsigned int len)
  {
    unsigned char ret = 0;

    for (unsigned int i = 0; i < len; i++) {
      ret ^= data[i];

      for (unsigned int j = 0; j < 8; j++) {
	if (ret & 0x1)
	  ret ^= 0x91;

	ret >>= 1;
      }
    }
    
    return ret;
  }
  //==================================================================
  /** 
   * Gray encoding to binary 
   */
  inline unsigned int gray_to_bin(unsigned int n)
  {
    int a[32];
    for (int i = 0; i < 32; i++)  {
      a[i] = ((n & 0x80000000) > 0);
      n = n << 1; // Multiply by 2 
    }

    int b[32];
    b[0] = a[0];
    for (int i = 1; i < 32; i++) {
      if (a[i] > 0)
	b[i] = (b[i-1] == 0);
      else
	b[i] = b[i-1];
    }

    unsigned int ret = 0;
    for (int i = 0; i < 32; i++) {
      ret = (ret << 1); // Multiply by 2
      ret = (ret | b[i]);
    }

    return ret;
  }
  //==================================================================
  template <size_t WIDTH>
  struct hex_out
  {
    unsigned int _x;
    const char*  _p;
    hex_out(unsigned int x) : _x(x), _p("") {}
    hex_out(const char* p, unsigned int x) : _x(x), _p(p) {}
  };
  //------------------------------------------------------------------
  template <size_t WIDTH>
  inline std::ostream& operator<<(std::ostream& o, const hex_out<WIDTH>& v)
  {
    auto fill = o.fill('0');
    auto flag = o.flags(std::ios_base::hex);

    o << v._p << std::setw(WIDTH) << v._x;

    o.fill(fill);
    o.flags(flag);
    return o;
  }
  //==================================================================
  template <size_t N>
  inline void dump_bits(const std::bitset<N>& b,std::ostream& o=std::cout)
  {
    o << "         " << std::left;
    for (size_t i = 0; i < 10; i++) o << " " << std::setw(4) << i*4;
    o << std::right;

    for (size_t i = 0; i < N; i++) {
      if (i % 40 == 0)
	o << "\n"
	  << std::setw(4) << i/8 << " "
	  << std::setw(4) << i;
      if (i % 4 == 0) o << ' ';
      o << b[i];
    }
    o << std::endl;
  }
  //------------------------------------------------------------------
  /** Print out memory with the most significant byte first */
  inline void dump_bytes(const unsigned char* ptr,
			 size_t n, std::ostream& o=std::cout)
  {
    for (size_t i = 0; i < n; i++) {
      if (i % 8 == 0) {
	if (i != 0) o << "\n";
	o << std::setw(4) << i << ' ';
      }
      o << hex_out<2>(ptr[i]) << ' ' << std::flush;
    }
    o << std::endl;
  }
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
//
// EOF
//

