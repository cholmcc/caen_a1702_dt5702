/** 
 * @file      feb/packet.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     A packet in the communication protocol
 */
#ifndef FEB_PACKET_HH
#define FEB_PACKET_HH
#include <array>
#include <iostream>
#include <iomanip>
#include <cstring>
namespace feb
{
  /** Type type of a MAC address */
  using mac_address = std::array<unsigned char,6>;

  inline std::ostream& operator<<(std::ostream& o, const mac_address& m)
  {
    o << std::setfill('0') << std::hex;
    for (auto& h : m) o << ' ' << std::setw(2) << unsigned(h);
    o << std::setfill(' ') << std::dec;
    return o;
  }
      
  /** Maximum packet length */
  constexpr const unsigned short max_len = 1500;
  /** Maximum payload length */
  constexpr const unsigned short max_payload = max_len-18;
  /** The protocol type */
  constexpr const unsigned short protocol = 0x0108;
  /** Broadcast MAC address */
  constexpr const unsigned char broadcast = 0xFF;

  //==================================================================
  /** 
   * The packet structure for communicating with front-end 
   */
  struct packet
  {
    /** Destination address 6 bytes */
    mac_address    destination = {0x00,0x60,0x37,0x12,0x34,0x00};
    /** Source address 6 bytes */
    mac_address    source = {0x00,0x00,0x00,0x00,0x00,0x00};
    /** Protocol type 2 bytes */
    unsigned short ip_type  = protocol;
    /** Command id */
    unsigned char id = 0;
    /** Command family */
    unsigned char family = 0;
    /** Value (or address) 2 bytes */
    unsigned short value = 0;
    /** Payload - maximum (1500-6-6-2-1-1-2=1500-18) */
    unsigned char  data[max_payload];

    void print(std::ostream& o, int trunc=18) const
    {
      o << source << " => " << destination << " "
	<< std::hex << std::setfill('0')
	<< "IP:  0x" << std::setw(4) << ip_type
	<< " CMD: 0x" << std::setw(2) << unsigned(family)
	<< "/0x"      << std::setw(2) << unsigned(id)
	<< " REG: 0x" << std::setw(4) << value;
      size_t n = trunc >= 18 ? trunc - 18 : 0;
      if (n > 0) o << " " << std::dec << n << " bytes" << std::hex;
      for (size_t i = 0; i < n; i++) {
	if (i % 10 == 0) o << "\n";
	o << std::setw(2) << unsigned(data[i]) << ' ';
      }
      o <<  std::setfill(' ') << std::dec;
    }
  };
  /** Write a packet to stream */
  inline std::ostream& operator<<(std::ostream& o, const packet& p) {
    p.print(o);
    return o;
  }
    
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
//
// EOF
//
