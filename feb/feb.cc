/** 
 * @file      feb/feb.cc
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     
 */
#include <feb/client.hh>
#include <feb/host.hh>
#include <feb/dummy.hh>
#include <feb/log.hh>

namespace feb {
  base_host* open(const std::string& inter,
		  const std::string& confdir,
		  int                loglvl,
		  int                maxcl)
  {
    _logbuf->set_level(loglvl);
    base_host* h = 0;
    if (inter.empty()) h = new feb::dummy(inter);
    else               h = new feb::host(inter);

    h->scan(maxcl);

    return h;
  }
}

