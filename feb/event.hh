/** 
 * @file      feb/event.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Event structure
 */
#ifndef FEB_EVENT_HH
#define FEB_EVENT_HH

namespace feb
{
  /** 
   * A raw event of a single channel as given by the FEB 
   *
   * Size is 
   *
   *   2 + 2 + 4 + 4 + 32 * 2 = 4 + 8 + 64 = 76 bytes
   */
  struct event
  {
    unsigned short overwritten;
    unsigned short lost;
    unsigned int   t0;
    unsigned int   t1;
    bool           t0_sync;
    bool           t1_sync;
    bool           t0_overflow;
    bool           t1_overflow;
    unsigned short adc[32];
  };
}

#endif
    
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:
