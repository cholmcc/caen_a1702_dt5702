/** 
 * @file      feb/log.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Logging system 
 */
#ifndef FEB_LOGGER_HH
#define FEB_LOGGER_HH
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>

namespace feb
{
  //==================================================================
  /** 
   * A stream buffer for logging.  Set the `rdbuf` on a stream to use
   * that as a logger.  For example
   *
   * @code 
   feb::logbuf lbuf;
   lbuf.level = logbuf::INFO;

   std::clog.rdbuf(&lbuf);
   std::clog.exceptions(std::ios_base::badbit); // fatal throws
   
   std::clog << "I This is an info message" << std::endl;
   std::clog << "W This is an warning message" << std::endl;
   std::clog << "E This is an error message" << std::endl;
   std::clog << "This is an uncategorized message (treated as info)" 
             << std::endl;
   try { 
     std::clog << "F This is fatal message which throws" << std::endl;
   }
   catch (std::exception& e) {
     std::cerr << e.what() << std::endl;
   }
   @endcode 
  */
  struct logbuf : public std::stringbuf
  {
    enum class levels {
      DEBUG,
      INFO,
      WARNING,
      ERROR,
      FATAL
    } level = levels::WARNING;
    
    constexpr static char debug   = 'D';
    constexpr static char info    = 'I';
    constexpr static char warning = 'W';
    constexpr static char error   = 'E';
    constexpr static char fatal   = 'F';
    static logbuf* _instance;

    static logbuf* instance();
    
    /** Constructor */
    logbuf() : std::stringbuf(std::ios_base::out)
    {
#ifndef FEB_NO_LOG
      std::clog.rdbuf(this);
      std::clog.exceptions(std::ios_base::badbit);
#endif 
    }

    /** 
     * Deduce the level and massage message 
     */
    levels code(std::string& s) {
      levels ret  = levels::INFO;
      bool   take = false;
      switch (s[0]) {
      case debug:    ret = levels::DEBUG;   take = true; break;
      case info:     ret = levels::INFO;    take = true; break;
      case warning:  ret = levels::WARNING; take = true; break;
      case error:    ret = levels::ERROR;   take = true; break;
      case fatal:    ret = levels::FATAL;   take = true; break;
      }
      if (take) {
	s = s.substr(1);
	while (s[0] == ' ' or s[0] == ':') 
	  s = s.substr(1);
      }
      return ret;
    }

    /** 
     * Called when syncronising buffer for output 
     *
     * Based on the first chararacter of the message, we delegate to
     * an appropriate member function.  These member functions are
     *
     * - 'I': `log_info`
     * - 'W': `log_warning`
     * - 'E': `log_error`
     * - 'F': `log_fatal` 
     *
     * If non of the chararacters are present in the beginning, then
     * we delegate to `log_info`.
     *
     * @returns 0 on success, 1 otherwise 
     */
    int sync() override {
      int ret = std::stringbuf::sync();
      std::string s = str();

      switch (code(s)) {
      case levels::DEBUG:    if (is_debug())   log_debug  (s); break;
      case levels::INFO:     if (is_info())    log_info   (s); break;
      case levels::WARNING:  if (is_warning()) log_warning(s); break;
      case levels::ERROR:    if (is_error())   log_error  (s); break;
      case levels::FATAL:                      log_fatal  (s); break;
      }

      // Reset buffer
      std::stringbuf::operator=(std::stringbuf());
      
      return ret;
    }
    void set_level(unsigned short lvl) {
      switch (lvl) {
      case 0:  level = levels::ERROR;    break;
      case 1:  level = levels::WARNING;  break;
      case 2:  level = levels::INFO;     break;
      default: level = levels::DEBUG;    break;
      }
    }
    void set_level(const std::string& lvl) {
      std::string l = lvl;
      std::transform(l.begin(), l.end(), l.begin(),
		     [](char c){return std::tolower(c);});
      level = levels::WARNING;  
      if      (lvl == "error")   level = levels::ERROR;  
      else if (lvl == "warn")    level = levels::WARNING;
      else if (lvl == "info")    level = levels::INFO;   
      else if (lvl == "debug")   level = levels::DEBUG;
    }      
    unsigned short get_level() const {
      switch (level) {
      case levels::FATAL:    
      case levels::ERROR:     return 0;
      case levels::WARNING:   return 1;
      case levels::INFO:      return 2;
      case levels::DEBUG:     return 3;
      }
      return 1;
    }
    bool is_debug()   const { return level <= levels::DEBUG; }
    bool is_info()    const { return level <= levels::INFO; }
    bool is_warning() const { return level <= levels::WARNING; }
    bool is_error()   const { return level <= levels::ERROR; }
    /** 
     * Log information message 
     *
     * @param s Message
     */
    virtual void log_debug(const std::string& s) { std::cerr << "D: " << s; }
    /** 
     * Log information message 
     *
     * @param s Message
     */
    virtual void log_info(const std::string& s) { std::cerr << "I: " << s; }
    /** 
     * Log information message 
     *
     * @param s Message
     */
    virtual void log_warning(const std::string& s) { std::cerr << "W: " << s; }
    /** 
     * Log information message 
     *
     * @param s Message
     */
    virtual void log_error(const std::string& s) { std::cerr << "E: " << s; }
    /** 
     * Log fatal message.  By default, this throws an exception.
     *
     * @param s Message
     */
    virtual void log_fatal(const std::string& s)
    {
      throw std::runtime_error(s);
    }
  };

  //------------------------------------------------------------------
  struct color_logbuf : logbuf
  {
    constexpr static const char* debug_color   = "\033[0;36m";
    constexpr static const char* info_color    = "\033[0;34m";
    constexpr static const char* warning_color = "\033[01;0;33m";
    constexpr static const char* error_color   = "\033[0;31m";
    constexpr static const char* fatal_color   = "\033[01;31m";
    constexpr static const char* end_color     = "\033[0m";
    
    color_logbuf() : logbuf() {}
    void log_debug(const std::string& s) override {
      std::cerr << debug_color << s << end_color;
    }
    void log_info(const std::string& s) override {
      std::cerr << info_color << s << end_color;
    }
    void log_warning(const std::string& s) override {
      std::cerr << warning_color << s << end_color;
    }
    void log_error(const std::string& s) override {
      std::cerr << error_color << s << end_color;
    }
    void log_fatal(const std::string& s) override {
      throw std::runtime_error(fatal_color + s + end_color);
    }
  };
  //------------------------------------------------------------------
  inline logbuf* logbuf::instance() {
    // Create instance 
    if (!_instance)
      _instance = new color_logbuf();

    return _instance;
  }
  
#ifndef LOG_INSTANCE
#define LOG_INSTANCE
  //------------------------------------------------------------------
  logbuf* logbuf::_instance = new color_logbuf();
#endif
}

#endif
// Local Variables:
//   compile-command: "(cd ../ && g++ -I. test_log.cc -o test_log)"
// End:

    
