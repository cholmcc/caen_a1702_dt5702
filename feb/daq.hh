/** 
 * @file      feb/daq.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Base clas for DAQs
 */
#ifndef FEB_DAQ_HH
#define FEB_DAQ_HH
#include <feb/base_host.hh>
#include <feb/client.hh>
#include <feb/commands.hh>
#include <feb/event.hh>
#include <chrono>
#include <signal.h>

namespace feb
{
  /** Base class for data acquisition */
  struct daq
  {
    base_host& _host;
    size_t     _nev     = 0;
    bool       _running = false;
    event      _event;

    /** 
     * @{ 
     * @name Handling Ctrl-C interrupt 
     */
    /** Singleton instance */
    static daq* _instance;
    /** Action structure */
    static struct sigaction _int_action;
    /** Handler */
    static void int_handle(int) {
      if (!_instance) return;
      std::clog << "W: Caught Ctrl-C - stopping" << std::endl;
      _instance->stop();
    }
    /** @} */

    /** Constructor */
    daq(base_host& host) : _host(host) {}
    /** Destructor */
    virtual ~daq() {
      std::clog << "W: DAQ destructed, stopping run" << std::endl;
      stop();
    }
	

    /** Install Ctrl-C signal handler */
    void install_handler() {
      _instance              = this;
      _int_action.sa_handler = daq::int_handle;
      _int_action.sa_flags   = 0;
      sigemptyset(&_int_action.sa_mask);

      sigaction(SIGINT, &_int_action, NULL);
      std::clog << "I: Installed Ctrl-C handler" << std::endl;
    }
	
    /** Get broadcast "client" */
    client& broadcast() {
      return _host._clients.find(feb::broadcast)->second;
    }

    /** 
     * Initialize clients. 
     *
     * - Write current configuration 
     * - Turn on bias voltage 
     *
     * @return true on success 
     */
    virtual bool init() {
      for (auto& kv : _host._clients) {
	if (kv.first == feb::broadcast) continue; // Do not broadcast
	if (!kv.second.configure()) {
	  std::clog << "E: Failed to configure card "
		    << kv.first << std::endl;
	  return false;
	}
      }

      auto& bcast = broadcast();

      if (!bcast.turn_on_bias()) {
	std::clog << "E: Failed to turn on bias voltage" << std::endl;
	return false;
      }

      return true;
    }
    /** 
     * Start a run 
     */
    virtual bool start() {
      _nev = 0;
      _running = false;
      
      if (!init()) {
	std::clog << "E: Failed to initialize boards" << std::endl;
	return _running;
      }

      auto& bcast = broadcast();

      commands::acquire rst(commands::acquire::RESET);
      if (!bcast.send_command(rst)) {
	std::clog << "E: Failed to reset buffers" << std::endl;
	return _running;
      }

      commands::acquire str(commands::acquire::START);
      if (!bcast.send_command(str)) {
	std::clog << "E: Failed to start acquistition" << std::endl;
	return _running;
      }

      _running = true;
      return _running;
    }
    /** 
     * Stop a run 
     */
    virtual bool stop()
    {
      auto& bcast = broadcast();

      commands::acquire stp(commands::acquire::STOP);
      if (!bcast.send_command(stp)) {
	std::clog << "E: Failed to stop acquistiion" << std::endl;
	return false;
      }
      _running = false;

      return true;
    }
    /** 
     * Loop for some specified time, or for some number of events,
     * or forever.
     * 
     * @param maxev   Max number of events  (0 unlimited)
     * @param maxsec  Max number of seconds (0 unlimited)
     *
     * @return trus;
     */
    bool loop(size_t maxev, size_t maxsec)
    {
      auto st = std::chrono::steady_clock::now();
	
      if (!start()) return false;
      const char throb[] = { '|', '/', '-', '\\' };
      size_t i = 0;
      while (_running) {
	if (i % 100 == 0) 
	  std::clog << "I: " << "\r" << throb[(i/100)%4] << std::flush;
	i++;
	
	// Execute pre-hook
	if (!pre()) break;

	if (maxsec > 0) {
	  // Check time
	  auto now = std::chrono::steady_clock::now();
	  std::chrono::duration<double> elapsed = now-st;
	  if (elapsed.count() >= maxsec) break;
	}
	// Check number of events 
	if (maxev > 0 and _nev >= maxev) break;

	// Loop over all clients, and get data
	for (auto& kv : _host._clients) {
	  if (kv.first == feb::broadcast) continue;

	  if (!_running) break;
	  if (!take(kv.second)) {
	    std::clog << "E: Failed to get events from FEB"
		      << std::endl;
	    break;
	  }
	}

	if (!post()) break;
      }

      if (!stop()) return false;

      auto now = std::chrono::steady_clock::now();
      std::chrono::duration<double> elapsed = now-st;
      std::clog << "I: Took " << _nev << " events in "
		<< elapsed.count() << " seconds" << std::endl;
	
      return true;
    }
    /** 
     * Take data from a single client 
     *
     * @param cl       Client 
     * @param adjvcxo  Frequency in adjust of VCXO 
     *
     * @return true
     */
    virtual bool take(client& cl, unsigned short adjvcxo=20) {
      if (!_running) return false;
      
      if (!cl.read_events()) {
	std::clog << "E: Failed to read event from client "
		  << hex_out<2>("0x", cl._mac5)
		  << std::endl;
	return false;
      }

      while (cl.get_event(_event)) {
	if (!process(cl)) continue;
	_nev++;
      }

      if (adjvcxo > 0) cl.adjust_vcxo(adjvcxo);
	
      return true;
    }
    /** 
     * Called one each event has been read in.  Access the event
     * through the member _event
     *
     * @param cl  Client 
     *
     * @return true
     */
    virtual bool process(client& cl)
    {
      // Access event information from member _event
      std::clog << "I: Got event at t0=" << _event.t0 << std::endl;
      return true;
    }
    /** pre-event hook */
    virtual bool pre() { return true; }
    /** post-event hook */
    virtual bool post() { return true; }
  };
#ifndef DAQ_INSTANCE
#define DAQ_INSTANCE
  daq* daq::_instance = 0;
  struct sigaction daq::_int_action;
#endif
}
#endif
