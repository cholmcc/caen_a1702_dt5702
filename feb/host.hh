/** 
 * @file      feb/host.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Host system interface 
 */
#ifndef FEB_HOST_HH
#define FEB_HOST_HH
#include <feb/base_host.hh>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <cerrno>
#include <cstring>
#include <stdexcept>
#include <iostream>

namespace feb
{
  //==================================================================
  struct host : base_host
  {
    /** 
     * Get last system error string 
     *
     * @return system error string 
     */
    static std::string err_string()
    {
      return strerror(errno);
    }

    /** Index of used interface */
    int _index;
    /** Write socket file descriptor */
    int _write_socket;
    /** Read socket file descriptor */
    int _read_socket;

    /** 
     * Open host.  We open two sockets for write and read 
     *
     * @param interface Name of the interface (e.g., "eth1")
     */
    host(const std::string& interface)
      : base_host(interface),
	_index(-1),
	_write_socket(-1),
	_read_socket(-1)
    {
      // std::clog << "D: Opening sockets on " << interface << std::endl;

      if ((_write_socket = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1)
	std::clog << "F: Failed to open write socket: "
		  << err_string() << std::endl;
      std::clog << "D: Got write socket " << _write_socket << std::endl;

      if ((_read_socket = socket(PF_PACKET, SOCK_RAW, protocol)) == -1)
	std::clog << "F: Failed to open read socket: "
		  << err_string() << std::endl;
      std::clog << "D: Got read socket " << _read_socket << std::endl;

      // Set socket options
      set_timeout(500000);

      // Bind listener to device
      if (setsockopt(_read_socket, SOL_SOCKET, SO_BINDTODEVICE,
		     interface.c_str(), interface.size()) == -1)
	std::clog << "F: Failed to bind to device: "
		  << err_string() << std::endl;
      //std::clog << "D: Bound read socket " << _read_socket << " to interface "
      //  	  << interface << std::endl;
	
      // Get index of interface - maybe we need only do this once (store index)
      _index = get_index();

      // Get source MAC address - maybe it is only needed once 
      _source = get_mac();
    }
    // ---------------------------------------------------------------
    /** 
     * @{ 
     * @name Query and setting utilities 
     */
    /** 
     * Get index of interface to write to 
     *
     * @return Index 
     */
    int get_index() const
    {
      // Get index of interface 
      // std::clog << "D: Get index of " << _interface << std::endl;

      ifreq request;
      memset(&request, 0, sizeof(ifreq));
      strncpy(request.ifr_name, _interface.c_str(), IFNAMSIZ-1);

      if (ioctl(_write_socket, SIOCGIFINDEX, &request) < 0)
	std::clog << "F: Failed to get index of interface: "
		  << err_string() << std::endl;

      // std::clog << "D: Got index " << request.ifr_ifindex << std::endl;
      return request.ifr_ifindex;
    }
    
    // _______________________________________________________________
    /** 
     * Get MAC address of interface to write to 
     *
     */
    mac_address get_mac() const
    {
      // std::clog << "D: Get mac of " << _interface << std::endl;
      // Get request of interface 
      ifreq request;
      memset(&request, 0, sizeof(ifreq));
      strncpy(request.ifr_name, _interface.c_str(), IFNAMSIZ-1);

      if (ioctl(_write_socket, SIOCGIFHWADDR, &request) < 0)
	std::clog << "F: Failed to get MAC of interface: "
		  << err_string() << std::endl;

      mac_address mac;
      memcpy(&mac, (uint8_t*)&request.ifr_hwaddr.sa_data, 6);

      // std::clog << "D: Got MAC " << mac << std::endl;
      return mac;
    }

    // _______________________________________________________________
    /** 
     * Set timeout (in microseconds) of the reply from the request to
     * be send.
     *
     * @param microseconds timeout in microseconds 
     */
    void set_timeout(unsigned int microseconds=500000) const
    {
      //std::clog << "D: Set timeout to " << microseconds << " us" << std::endl;
      timeval tv = { 0, microseconds }; // 500ms

      // Set timeout on reply 
      if (setsockopt(_read_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv,
		     sizeof(timeval))== -1)
	std::clog << "F: Failed to set read socket options: "
		  << err_string() << std::endl;
    }
    /** @} */
    
    // ---------------------------------------------------------------
    /** 
     * @{ 
     * @name Sending commands 
     */
    /** 
     * Send a packet with a specific length and timeout 
     *
     * Note, packet is overwritten by the reply 
     *
     * @param pkt  Packet to send.  Will contain reply 
     * @param len  Length of packet data 
     * @param timeout Timeout in microseconds 
     *
     * @return number of recieved bytes, or negative in case of error 
     */
    int send(packet& pkt, int len, unsigned int timeout) const
    {
      auto family = pkt.family;
      // auto id     = pkt.id;
      auto dst    = pkt.destination[5];
      // int  idx    = get_index(); // Do we need to do this every time?
      // auto mac    = get_mac();   // Is this needed?
      int  idx    = _index; 
      set_timeout(timeout);

      struct sockaddr_ll address;
      address.sll_ifindex = idx;
      address.sll_halen   = ETH_ALEN;
      for (size_t i = 0; i < 6; i++)
	address.sll_addr[i] = pkt.destination[i];

      //       std::clog << "D: >> (" << std::setw(4)<< len << ") ";
      pkt.print(std::clog, len);
      std::clog << std::endl;

      if (sendto(_write_socket, (unsigned char*)&pkt, len, 0,
		 (struct sockaddr*)&address,
		 sizeof(struct sockaddr_ll)) < 0) {
	std::clog << "E: Failed to send to destination "
		  << unsigned(pkt.destination[5]) << std::endl;
	return 0;
      }

      int numbytes = 0;
      int retval   = -1;
      int numpkts  = 0;
      while (numbytes >= 0) {	// 
	numbytes = recvfrom(_read_socket, &pkt,
			    max_len, 0, NULL, NULL);
	// std::clog << "D: << (" << std::setw(4) << numbytes << ") ";
	pkt.print(std::clog,numbytes);
	std::clog << std::endl;
	
	if (numbytes < 0) {
	  if (numpkts == 0)
	    std::clog << "W: Failed to " << len << " read from "
	       	      << unsigned(pkt.destination[5])
		      << " within " << timeout << " us: "
		      << err_string() << std::endl;
	  break;
	}

	// Check if this was not for us and not broadcast 
	if (pkt.source[5] != dst && dst != broadcast) {
	  // std::clog << "D: Reply is not for us ("
	  // 	    << unsigned(dst) << ") but "
	  //  	    << pkt.source[5] << std::endl;
	  continue;
	}
	retval += numbytes;
	numpkts++;
      }
      if (retval < 0) return 0;
      
      if (family != pkt.family) {
	std::clog << "W: Reply is of the wrong family: "
		  << unsigned(pkt.family) << " expected "
		  << unsigned(family) << std::endl;
	return 0;
      }
      if (pkt.id == 0xFF) {
	std::clog << "W: Reply contained error: "
		  << unsigned(pkt.id) << " for family "
		  << unsigned(pkt.family) << std::endl;
	return 0;
      }
      
      return retval;
    }
    
    /** 
     * Send a command.  In case the command succeeds, the command will
     * decode the reply and store
     *
     * Note, the command settings are overwritten by the reply (i.e.,
     * commands and one-shot objects).
     * 
     * @param mac5  Last byte of destination mac address 
     * @param cmd   Command to send 
     */
    bool send_command(const char     mac5,
		      command_base&  cmd) override
    {
      // Very short timeout 
      set_timeout(10);

      // Flush input buffer 
      int numbytes = 1;
      while (numbytes > 0) {
	// std::clog << "D: Flushing input buffer" << std::endl;
	packet pkt;
	numbytes = recvfrom(_read_socket, &pkt, max_len, 0, NULL, NULL);
      }

      cmd.source         = _source;
      cmd.destination[5] = mac5;

      // std::clog << "D: Command has length " << cmd.size() << std::endl;
      int pktlen      = std::max(18+cmd.size(),64);
      int rcvlen      = send(cmd, pktlen, cmd.timeout());

      if (rcvlen <= 0)         return false;
      if (!cmd.verify())       return false;
      if (!cmd.length(rcvlen)) return false;
      
      return true;
    }
    /** @} */

    /** 
     * @{ 
     * @name Client discovery 
     */
    /** 
     * Scan for clients on the network interface 
     */
    bool scan(unsigned short max=254) override;
    /** @} */
  };
}
#include <feb/client.hh>
namespace feb {
  inline bool host::scan(unsigned short max)
  {
    _clients.clear();

    std::clog << "I: Scanning" << std::flush;
    for (unsigned char mac5 = 0; mac5 <= max; mac5++) {
      if (mac5 % 8 == 0) std::clog << "I: ." << std::flush;
      commands::set_host sh(_source,500);
      if (!send_command(mac5, sh)) continue;

      // std::cout << " found" << std::endl;
      auto i = _clients.emplace(mac5,*this);
      i.first->second._mac5 = mac5;
    }
    auto i = _clients.emplace(broadcast,*this);
    i.first->second._mac5 = broadcast;

    std::clog << "I: " << _clients.size() << " clients" << std::endl;
    return _clients.size() > 1;
  }
}

#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:

