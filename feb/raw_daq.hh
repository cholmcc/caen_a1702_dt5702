#ifndef FEB_RAW_DAQ_HH
#define FEB_RAW_DAQ_HH
#include <feb/daq.hh>

namespace feb
{
  struct raw_daq : public feb::daq
  {
    std::ostream& _o;
    /** 
     * Constructor 
     *
     * @param h Host
     * @param out Output stream 
     */
    raw_daq(feb::base_host& h, std::ostream& out)
      : feb::daq(h), _o(out)
    {
      _o << "# Cl "
	 << std::left 
	 << std::setw(6)  << "T0 sy"
	 << std::setw(6)  << "T1 sy"
	 << std::setw(6)  << "T0 of"
	 << std::setw(6)  << "T1 of"
	 << std::setw(6)  << "Overw"
	 << std::setw(6)  << "Lost"
	 << std::setw(10) << "T0"
	 << std::setw(10) << "T1"
	 << "ADCs"
	 << std::right 
	 << std::endl;
    }
    /** 
     * @param cl Client 
     */
    bool process(feb::client& cl) {
      _o << std::boolalpha << std::left 
	 << feb::hex_out<2>("0x",cl._mac5) << " "
	 << std::setw(6)  << _event.t0_sync    
	 << std::setw(6)  << _event.t1_sync    
	 << std::setw(6)  << _event.t0_overflow
	 << std::setw(6)  << _event.t1_overflow
	 << std::setw(6)  << _event.overwritten 
	 << std::setw(6)  << _event.lost        
	 << std::setw(10) << _event.t0          
	 << std::setw(10) << _event.t1;
      for (size_t i = 0; i < 32; i++)
	_o << std::setw(6) << _event.adc[i];
      _o << std::right << std::endl;

      return true;
    }
  };
}
#endif
