/** 
 * @file      feb/functions.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Functions to fit to data
 */
#ifndef FEB_FUNCTIONS_HH
#define FEB_FUNCTIONS_HH
#include <cmath>
#include <numeric>
#include <valarray>

namespace feb
{
  /** 
   * Various functions to use with the data 
   */
  struct functions
  {
    /** Type of array of real values */
    using real_array = std::valarray<double>;
    
    /** 
     * Helper function to promote std::valarray from one type to another 
     */
    template <typename R, typename T>
    static std::valarray<R>
    conv(const std::valarray<T>& v)
    {
      std::valarray<R> r(v.size());

      for (size_t i = 0; i < v.size(); i++) r[i] = v[i];

      return r;
    }
    /** 
     * Calculate Poisson probability at each integer value 
     *
     * @f[f(n;\lambda) = \frac{e^{-\lambda}\lambda^n}{n!}@f]
     *
     * @param n where to evaluate the probability 
     * @param m The mean @f$\lambda@f$ 
     *
     * @return Probability at each @f$ n@f$ 
     */
    static real_array
    poisson(const real_array& n, double m)
    {
      return std::exp(n * std::log(m) - m - (n+1).apply(lgamma));
    }
    /** 
     * Calculate response function 
     *
     * @f[ 
     f(x) = A\sum_{i=0}^N \left[P(i;\lambda)+XP(i-1;\lambda)\right]
     N(x;p + i\cdot g,\sqrt{n^2+i\cdot\delta})\quad,
     @f]
     *
     * where 
     *
     * - @f$A@f$ is the normalisation 
     * - @f$P(i;\lambda)@f$ is the Poisson probability of @f$i@f$ with 
     *   mean @f\lambda@f$
     * - @f$X@f$ is the cross-talk factor 
     * - @f$N(x;\mu,\sigma)@f$ is the Normal probability of @f$x@f$ 
     *   for mean and standard deviation, @f$\mu=p+i\cdot g@f$ and 
     *   @f$\sigma=\sqrt{n+i\cdot\delta}@f$,
     *   respectively 
     * - @f$ g@f$ is the gain factor 
     * - @f$ p@f$ is the pedestal location 
     * - @f$ n@f$ is the noise (or pedestal width)
     * - @f$ \delta@f$ is the extra width 
     * - @f$ N@f$ is maximum number of photons to fold in
     * 
     * 
     * @param x             Where to evaluate function (ADC) 
     * @param normalisation @f$A@f$ Overall normalisation 
     * @param gain          @f$g@f$ ADC to photons 
     * @param pedestal      @f$p@f$ Pedestal mean 
     * @param noise         @f$n@f$ Pedestal width in ADC
     * @param avg_photons   @f$\lambda@f$ Average number of photons 
     * @parma xtra_width    @f$\delta@f$ Extra width factor 
     * @param x_talk        @f$X@f$ Cross-talk factor 
     * @param max_photons   Maximum number of photons to fold 
     *
     * @return function value 
     */
    static real_array
    response(const real_array& x,
	     double 	       normalisation,
	     double            gain,
	     double            pedestal,
	     double            noise,
	     double            avg_photons,
	     double            xtra_width,
	     double            x_talk,
	     size_t            max_photons=6)
    {
      real_array i_photon(max_photons);
      std::iota(std::begin(i_photon), std::end(i_photon), 0);
      real_array i_photon1 = i_photon + 1;

      // Calculate peak locations
      real_array peaks = pedestal + gain * i_photon;
      
      // Calculate photon probability (Poisson)
      real_array p_photon = std::exp(i_photon * std::log(avg_photons)
				     - avg_photons
				     - (i_photon1).apply(lgamma)); 
      real_array p2 = p_photon[std::slice(1,p_photon.size()-2,1)];
      
      // Add cross-talk term 
      p_photon[std::slice(2,p_photon.size(),1)] +=x_talk * p2;

      // Fold with normal distribution
      auto s = noise * noise + i_photon * xtra_width;
      auto f = p_photon / std::sqrt(2 * M_PI * s);
      // Cannot pass capuring lambda to std::valarray::apply - sigh!
      // auto g = [f,s,peaks](double x) -> double {
      //   return (f * std::exp(-pow(x-peaks,2)/(2*s))).sum();
      // };

      // Calculate the folding 
      real_array r(x.size());
      for (size_t i = 0; i < x.size(); i++) 
	r[i] = (f * std::exp(-std::pow(x[i] - peaks,2) / (2 * s))).sum();
      
      return normalisation * r;
    }
    /** 
     * Calculates the outer product of two vectors (assumed to be the
     * same size).  
     *
     * @param u  First vector 
     * @param v  Second vector 
     * 
     * @returns the outer product @f$ uv^T@f$ 
     */
    static real_array outer_mul(const real_array& u, const real_array& v)
    {
      auto   bu = std::begin(u);
      auto   bv = std::begin(v);
      size_t m  = std::distance(bu, std::end(u));
      size_t n  = std::distance(bv, std::end(v));

      real_array uv(m*n);
      auto       iuv = std::begin(uv);

      for (auto iu = bu; iu != std::end(u); ++iu)
        for (auto iv = bv; iv != std::end(v); ++iv, ++iuv)
          *iuv = *iu * *iv;

      return uv;
    }
    /** 
     * Calculates the outer sum of two vectors (assumed to be the
     * same size).  
     *
     * @param u  First vector 
     * @param v  Second vector 
     * 
     * @returns the outer product @f$ uv^T@f$ 
     */
    static real_array outer_add(const real_array& u, const real_array& v)
    {
      auto   bu = std::begin(u);
      auto   bv = std::begin(v);
      size_t m  = std::distance(bu, std::end(u));
      size_t n  = std::distance(bv, std::end(v));

      real_array uv(m*n);
      auto       iuv = std::begin(uv);
      for (auto iu = bu; iu != std::end(u); ++iu)
        for (auto iv = bv; iv != std::end(v); ++iv, ++iuv)
          *iuv = *iu + *iv;

      return uv;
    }
    /** 
     * Calculate response function 
     *
     * @f[ 
     f(x) = A\sum_{i=0}^N\sum_{j=0}^N P(i;\lambda)P(j;X\lambda)\right]
     N(q;i + j,\sqrt{n^2+\delta^2(i+j)})\quad,
     @f]
     *
     * where 
     *
     * - @f$A@f$ is the normalisation 
     * - @f$q=(x-p)/g@f$ 
     * - @f$ g@f$ is the gain factor 
     * - @f$ p@f$ is the pedestal location 
     * - @f$ n@f$ is the noise (or pedestal width)
     * - @f$P(i;\lambda)@f$ is the Poisson probability of @f$i@f$ with 
     *   mean @f\lambda@f$
     * - @f$X@f$ is the cross-talk factor 
     * - @f$N(x;\mu,\sigma)@f$ is the Normal probability of @f$x@f$ 
     *   for mean and standard deviation, @f$\mu=i+j@f$ and 
     *   @f$\sigma=\sqrt{n^2+\delta^(i+q)}@f$,
     *   respectively 
     * - @f$ \delta@f$ is the extra width 
     * - @f$ N@f$ is maximum number of photons to fold in
     * 
     * 
     * @param x             Where to evaluate function (ADC) 
     * @param normalisation @f$A@f$ Overall normalisation 
     * @param gain          @f$g@f$ ADC to photons 
     * @param pedestal      @f$p@f$ Pedestal mean 
     * @param noise         @f$n@f$ Pedestal width in ADC
     * @param avg_photons   @f$\lambda@f$ Average number of photons 
     * @parma xtra_width    @f$\delta@f$ Extra width factor 
     * @param x_talk        @f$X@f$ Cross-talk factor 
     * @param max_photons   Maximum number of photons to fold 
     *
     * @return function value 
     */
    static real_array
    response2(const real_array& x,
	      double            normalisation,
	      double            gain,
	      double            pedestal,
	      double            noise,
	      double            avg_photons,
	      double            xtra_width,
	      double            x_talk,
	      size_t            max_photons=6)
    {
      real_array i_photon(max_photons);
      std::iota(std::begin(i_photon), std::end(i_photon), 0);
      real_array i_photon1 = i_photon + 1;

      // Calculate peak locations
      real_array peaks = pedestal + gain * i_photon;
      
      // Calculate photon probability
      double avg    = avg_photons;
      auto lg       = (i_photon1).apply(lgamma);
      auto p_photon = std::exp(i_photon * std::log(avg)- avg - lg); 
      // Calculate photon probability w/cross-talk
      avg           = x_talk * avg_photons;
      auto xp_photon = std::exp(i_photon * std::log(avg)- avg - lg);
      
      // Calculate outer product and sum, and variance (matrixes)
      auto pxp = outer_mul(p_photon, xp_photon);
      auto ij  = outer_add(i_photon,i_photon);
      auto var = (noise * noise + xtra_width * xtra_width * ij) / (gain * gain);

      // This does the sum over the matrix 
      real_array q = (x - pedestal) / gain;
      real_array r(q.size());
      for (size_t i = 0; i < r.size(); i++) 
	r[i] = (pxp / std::sqrt(2 * M_PI * var)
		* std::exp(-std::pow(q[i] - ij, 2) / (2 * var))).sum();
      
      return normalisation * r;
    }
			 
  };
}
#endif
//
// EOF
//
