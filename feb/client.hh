/** 
 * @file      feb/client.hh
 * @date      Sun 26 Sep 2021
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @copyright 2021 Christian Holm Christensen
 * @license   GPL-v3
 * @brief     Client class 
 */
#ifndef FEB_CLIENT_HH
#define FEB_CLIENT_HH
#include <feb/slow_control.hh>
#include <feb/probe.hh>
#include <feb/flexible_logic.hh>
#include <feb/commands.hh>
#include <fstream>
#include <sstream>
#include <cstring>

namespace feb
{
  /** Forward declaration */
  struct base_host;
  
  /** A single client (board) */
  struct client
  {
    /** The host interface */
    base_host& _host;
    /** The last byte of the mac address */
    unsigned char _mac5;
    /** The slow control settings */
    slow_control _slow_control;
    /** The probe settings */
    probe _probe;
    /** The flexible logic configuration */
    flexible_logic _flexible_logic;
    /** Last read events command */
    commands::read_events _events;

    /** Measure drift of sync signals */
    struct timing {
      unsigned int _last_sync = 0;
      unsigned int _n_sync    = 0;
      double       _avg_drift = 0;

      /** Update mean drift */
      void update(unsigned int t) {
	_last_sync = t;
	// Welfords algorithm for on-line mean
	if (_n_sync <= 0) {
	  _n_sync    = 1;
	  _avg_drift = t;
	  return;
	}
	_n_sync++;
	_avg_drift += 1. / _n_sync * (t - _avg_drift);
      }
      /** Reset the sync analysis */
      void reset() {
	_last_sync = 0;
	_n_sync    = 0;
	_avg_drift = 0;
      }
    };
    /** Sync on t0 */
    timing _t0;
    /** Sync on t1 */
    timing _t1;
    /** The timing adjustment */
    unsigned short _vcxo = 500;
    

    client(base_host& h, unsigned char m5=0)
      : _host(h),
	_mac5(m5)
    {}
#if 0
    client(client&& o)
      : _host(o._host),
	_mac5(o._mac5),
	_slow_control(std::move(o._slow_control)),
	_probe(std::move(o._probe)),
	_flexible_logic(std::move(o._flexible_logic)),
	_events()
    {}
#endif	
    client(const client&) = delete;
    client& operator=(const client&) = delete;
    
    /** 
     * @{ 
     * @name Default filenames 
     */
    /** Client specific name */
    std::string base_filename(const std::string& post) const {
      std::stringstream s;
      s << "client_0x" << std::setfill('0') << std::hex
	<< std::setw(2) << unsigned(_mac5) << post;
      return s.str();
    }
    /** Broadcase file name */
    std::string bcast_filename(const std::string& post) const {
      return "client_0xff" + post;
    }
    /** Get filename with path */
    std::string full_filename(const std::string& dir,
			      const std::string& filename) const {
      if (dir.empty()) return filename;
      return dir + "/" + filename;
    }
    /** @} */
    
    /** 
     * @{ 
     * @name Load settings for this client from files
     */
    /** Read in slow control settings */
    void load_slow_control(std::istream& in) { in >> _slow_control; };
    /** Read in probe settings */
    void load_probe(std::istream& in) { in >> _probe; };
    /** Read in flexible logic settings */
    void load_flexible_logic(std::istream& in) { in >> _flexible_logic; };
    /** Read in slow control settings from names file */
    bool load_slow_control(const std::string& dir,
			   const std::string& filename) {
      std::ifstream in(full_filename(dir,filename).c_str());
      if (!in) {
	std::clog << "W: Failed to open "
		  << full_filename(dir,filename)
		  << " for reading" << std::endl;
	return false;
      }

      std::clog << "I: Loading slow control from "
		<< full_filename(dir,filename) << std::endl;

      load_slow_control(in);

      in.close();

      return true;
    }
    /** Read in probe settings from names file */
    bool load_probe(const std::string& dir,
		    const std::string& filename) {
      std::ifstream in(full_filename(dir,filename).c_str());
      if (!in) {
	std::clog << "W: Failed to open "
		  << full_filename(dir,filename)
		  << " for reading" << std::endl;
	return false;
      }

      std::clog << "I: Loading probe from "
		<< full_filename(dir,filename)	<< std::endl;

      load_probe(in);

      in.close();

      return true;
    }
    /** Read in flexible_logic settings from names file */
    bool load_flexible_logic(const std::string& dir,
			     const std::string& filename) {
      std::ifstream in(full_filename(dir,filename).c_str());
      if (!in) {
	std::clog << "W: Failed to open "
		  << full_filename(dir,filename)
		  << " for reading" << std::endl;
	return false;
      }

      std::clog << "I: Loading flexible logic from "
		<< full_filename(dir,filename)	<< std::endl;

      load_flexible_logic(in);

      in.close();

      return true;
    }
    /** 
     * Read slow control setting from default file name.  This falls
     * back to broadcast filename if specific file isn't found for
     * this client 
     */
    void load_slow_control(const std::string& dir="") {
      if (!load_slow_control(dir,base_filename(".sc")))
	load_slow_control(dir,bcast_filename(".sc"));
    }
    /** 
     * Read probe setting from default file name.  This falls
     * back to broadcast filename if specific file isn't found for
     * this client 
     */
    void load_probe(const std::string& dir="") {
      if (!load_probe(dir,base_filename(".pb"))) 
	load_probe(dir,bcast_filename(".pb"));
    }
    /** 
     * Read flexible_logic setting from default file name.  This falls
     * back to broadcast filename if specific file isn't found for
     * this client 
     */
    void load_flexible_logic(const std::string& dir="") {
      if (!load_flexible_logic(dir,base_filename(".fl")))
	load_flexible_logic(dir,bcast_filename(".fl"));
    }
    /** 
     * Read in all settings from default file names 
     */
    void load_settings(const std::string& dir="")
    {
      load_slow_control  (dir);
      load_probe         (dir);
      load_flexible_logic(dir);
    }
    /** @} */

    /** 
     * @{ 
     * @name Save settings to files 
     */
    /** Write slow control to stream */
    void save_slow_control(std::ostream& o) const { o << _slow_control; }
    /** Write probe to stream */
    void save_probe(std::ostream& o) const { o << _probe; }
    /** Write flexible_logic to stream */
    void save_flexible_logic(std::ostream& o) const { o << _flexible_logic; }
    /** Write slow control to named file */
    void save_slow_control(const std::string& dir,
			   const std::string& filename) const
    {
      std::ofstream o(full_filename(dir,filename).c_str());
      if (!o) {
	std::clog << "E: Failed to open "
		  << full_filename(dir,filename)
		  << " for writing" << std::endl;
	return;
      }
      std::clog << "I: Saving slow control to "
		<< full_filename(dir,filename) << std::endl;
      save_slow_control(o);
    }
    /** Write probe to named file */
    void save_probe(const std::string& dir,
		    const std::string& filename) const
    {
      std::ofstream o(full_filename(dir,filename).c_str());
      if (!o) {
	std::clog << "E: Failed to open "
		  << full_filename(dir,filename)
		  << " for writing" << std::endl;
	return;
      }

      std::clog << "I: Saving slow probe to "
		<< full_filename(dir,filename) << std::endl;
      save_probe(o);
    }
    /** Write flexible_logic to named file */
    void save_flexible_logic(const std::string& dir,
			     const std::string& filename) const
    {
      std::ofstream o(full_filename(dir,filename).c_str());
      if (!o) {
	std::clog << "E: Failed to open "
		  << full_filename(dir,filename)
		  << " for writing" << std::endl;
	return;
      }

      std::clog << "I: Saving flexible logic to "
		<< full_filename(dir,filename) << std::endl;
      save_flexible_logic(o);
    }
    /** Write slow control to default file */
    void save_slow_control(const std::string& dir="") {
      save_slow_control(dir,base_filename(".sc")); }
    /** Write probe to default file */
    void save_probe(const std::string& dir="") {
      save_probe(dir,base_filename(".pb")); }
    /** Write flexible_logic to default file */
    void save_flexible_logic(const std::string& dir="") {
      save_flexible_logic(dir,base_filename(".fl")); }
    /** Write out all settings to default files */
    void save_settings(const std::string& dir="") {
      save_slow_control  (dir);
      save_probe         (dir);
      save_flexible_logic(dir);
    }
    /** @} */

    /** 
     * @{ 
     * @name Write settings to card and sync back into memory 
     */
    /** 
     * Writes 
     *
     * - Slow control 
     * - Probe
     * - Flexible logic 
     *
     * settings to the hardware 
     *
     * @return true on success 
     */
    bool configure() const {
      commands::write_slow_control sc(_slow_control);
      if (!send_command(sc)) {
	std::clog << "E: Failed to send slow control to "
		  << hex_out<2>("0x",_mac5) << std::endl;
	return false;
      }

      commands::write_probe pb(_probe);
      if (!send_command(pb))  {
      	std::clog << "E: Failed to send probe to "
		  << hex_out<2>("0x", _mac5) << std::endl;
      	return false;
      }
      
      commands::write_flexible_logic fl(_flexible_logic);
      if (!send_command(fl))  {
	std::clog << "E: Failed to send flexible logic to "
		  << hex_out<2>("0x",_mac5) << std::endl;
	return false;
      }
      return true;
    }
    /** 
     * Read 
     *
     * - Slow control 
     * - Probe
     * - Flexible logic 
     *
     * from the hardware 
     *
     * @return true on success 
     */
    bool sync() {
      commands::read_slow_control sc;
      if (!send_command(sc)) {
	std::clog << "E: Failed to read slow control from 0x"
		  << std::setfill('0') << std::hex
		  << std::setw(2) << _mac5
		  << std::setfill(' ') << std::dec
		  << std::endl;
	return false;
      }
      sc.get(_slow_control);

      commands::read_probe pb;
      if (!send_command(pb)) {
	std::clog << "E: Failed to read probe from 0x"
		  << std::setfill('0') << std::hex
		  << std::setw(2) << _mac5
		  << std::setfill(' ') << std::dec
		  << std::endl;
	return false;
      }
      pb.get(_probe);

      commands::read_flexible_logic fl;
      if (!send_command(fl)) {
	std::clog << "E: Failed to read flexible logic from 0x"
		  << std::setfill('0') << std::hex
		  << std::setw(2) << _mac5
		  << std::setfill(' ') << std::dec
		  << std::endl;
	return false;
      }
      fl.get(_flexible_logic);

      return true;
    }
    bool turn_on_bias() {
      commands::turn_on_bias tob;
      if (!send_command(tob)) {
	std::clog << "E: Failed to turn on bias voltage" << std::endl;
	return false;
      }
      return true;
    }
    bool turn_off_bias() {
      commands::turn_off_bias tob;
      if (!send_command(tob)) {
	std::clog << "E: Failed to turn off bias voltage" << std::endl;
	return false;
      }
      return true;
    }    
    /** @} */
    
    /** 
     * @{ 
     * @name Some command handling 
     */
    /** 
     * Send a command. This forwards to the host interface with the
     * proper 6th byte of the mac address.
     */
    bool send_command(command_base& cmd) const;
    /** 
     * Read in the next events. 
     */
    bool read_events();
    /** @} */

    /** 
     * @{ 
     * @name Event processing 
     */
    /** 
     * Reset the timing trackers */
    void reset_timing() {
      _t0.reset();
      _t1.reset();
    }
    
    /** 
     * Get next available event.  This will also track the drift of
     * the syn signals.  This can be used with the member function -
     * adjust_vcxo - to sync input
     *
     * @param ev   Event structure to fill 
     * @param toff Correction for drift relative to GPS
     */
    bool get_event(event& ev, unsigned int toff=5)
    {
      if (!_events.get(ev,toff)) return false;

      if (ev.t0_sync) _t0.update(ev.t0);
      if (ev.t1_sync) _t1.update(ev.t1);

      return true;
    }
    /** 
     * Changing the timing based on mesaurement of the reference
     * offset 
     *
     * @param min_n Least number of syncs before adjusting 
     *
     */
    void adjust_vcxo(size_t min_n=20);
    /** 
     * Get current trigger rate 
     */
    float trigger_rate() const;
  };
}
#include <feb/host.hh>

namespace feb
{
  inline bool client::send_command(command_base& cmd) const
  {
    return _host.send_command(_mac5, cmd);
  }

  inline bool client::read_events()
  {
    if (_mac5 == broadcast)
      std::clog << "F: Cannot read events in broadcast" << std::endl;

    // Must make a new object every time - response overwrite 
    _events = commands::read_events();
    
    bool ret = _host.send_command(_mac5, _events);
    return ret;
  }
  inline void client::adjust_vcxo(size_t min_n)
  {
    if (_t0._n_sync < min_n) return;
    
    _vcxo -= int((_t0._avg_drift - 1e9) / 5.2);
    commands::set_host sh(_host._source, _vcxo);
    send_command(sh);
    _t0.reset();
  }
  inline float client::trigger_rate() const {
    commands::get_event_rate gr;
    if (!send_command(gr)) return 0;
    return gr.rate() / 1.e3;
  }
}
#endif
// Local Variables:
//   compile-command: "(cd .. && g++ -g -I.  first.cc -o first)"
// End:

    
	
      
    
    
